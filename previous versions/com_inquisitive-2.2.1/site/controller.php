<?php
/**
 * Inquisitive default controller
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$user = JFactory::getUser();
if($user->guest) {
//	JError::raiseError( 403, JText::_("ALERTNOTAUTH") );
}


jimport('joomla.application.component.controller');

/**
 * Inquisitive Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveController extends JControllerLegacy
{
	/**
	 * Method to display the view
	 *
	 * @access	public
	 */
	public function display($cachable = true, $urlparams = true)
	{		
		//XXX should permission be moved here?
		return parent::display();
	}

}

?>
