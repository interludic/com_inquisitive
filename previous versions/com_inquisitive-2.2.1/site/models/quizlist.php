<?php


/**
 * Inquisitive Model for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport('joomla.application.component.helper');
jimport( 'joomla.application.component.model' );
jimport( 'joomla.database.database' );


/**
 * Inquisitive Model
 *
 * @package    Inquisitive
 * @subpackage Components
 */
 
 //Models contain Model classes. 
 //One Model class is equal to one DB table.
 
class InquisitiveModelQuizList extends JModelLegacy
{			

	function trashThis($ID = NULL){		
		$db = JFactory::getDBO();	

		$del__quizid_questionid = 'DELETE FROM #__quizid_questionid WHERE ID='.$ID.'';		
		$db->setQuery( $del__quizid_questionid );
		$db->query();
		if($db->getErrorMsg()) return "error del__quizid_questionid: ".$db->getErrorMsg();	
	
		/* Preserve reporting ??? */
		$del_answers = 'DELETE FROM #__quiz_answer WHERE timestamp='.$ID.'';
		$db->setQuery( $del_answers );
		$db->query();		
		if($db->getErrorMsg()) return "error del_answers: ".$db->getErrorMsg();	
		
		$del_questions = 'DELETE FROM #__quiz_question WHERE quizID='.$ID.'';
		$db->setQuery( $del_questions );
		$db->query();		
		if($db->getErrorMsg()) return "error del_questions: ".$db->getErrorMsg();				
		
		$del_quiz_attempt = 'DELETE FROM #__quiz_attempt WHERE quizID='.$ID.'';
		$db->setQuery( $del_quiz_attempt);		
		$db->query();		
		if($db->getErrorMsg()) return "error del_quiz_attempt: ".$db->getErrorMsg();			

		$del_quiz_report_question = 'DELETE FROM #__quiz_report_question WHERE quiz_id='.$ID.'';
		$db->setQuery( $del_quiz_report_question);		
		$db->query();		
		if($db->getErrorMsg()) return "error del_quiz_report_question: ".$db->getErrorMsg();			
		
		$del_quiz_report_quiz = 'DELETE FROM #__quiz_report_quiz WHERE quiz_id='.$ID.'';
		$db->setQuery( $del_quiz_report_quiz);		
		$db->query();		
		if($db->getErrorMsg()) return "error del_quiz_report_quiz: ".$db->getErrorMsg();	

		$del_quiz_info = 'DELETE FROM #__quiz_info WHERE timestamp='.$ID.'';
		$db->setQuery( $del_quiz_info );
		$db->query();
		if($db->getErrorMsg()) return "error del_quiz_info: ".$db->getErrorMsg();			
		
		//echo "trash ? = ".$ID."<br/>".$del__quizid_questionid."<br/>".$del_answers."<br/>".$del_questions."<br/>".$del_quiz_info."<br/>".$del_quiz_attempt;
		return "trashed".$ID;
	}
			
		  
	/**************************************************************
	 * Return amount of times a student has accessed a quiz,
	 * if new user, create row in quiz_attempt
	 * return null for none left. 
	 * return 0 = first attempt yet to be used.
	 **************************************************************/
		 
	function quizAttemptNum($quizID){
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		$query = 'SELECT attemptNum FROM `#__quiz_attempt` WHERE quizID = "'.$quizID.'" and userID = "'.$user->username.'"';		
		$db->setQuery( $query );
		$db->query();
		$attemptNum = $db->loadRow();
		
		/*-echo "attempt - <pre>";
		print_r($attemptNum);
		echo "</pre>";//.$attemptNum;*/
		
		//row does not exist. Create (assumed new user!!)
		if($attemptNum['0'] == NULL) {			
			$queryAttempt = "INSERT INTO #__quiz_attempt (quizID, userID, attemptNum) VALUES ('".$quizID."', '".$user->username."', '0');";
			$db->setQuery( $queryAttempt );
			$db->query();
			
			return 1;
		}
		
		if($attemptNum['0'] == 0) return NULL;
			else return $attemptNum['0'];
	}
	
	function getQuizInfo($quizID){
		$db = JFactory::getDBO();
		$query = 'SELECT * FROM `#__quiz_info` WHERE timestamp = '.$quizID;			
		$db->setQuery( $query );
		$quizArray = $db->loadObject();

		return $quizArray;
	}
		
	function quizList($quizID = NULL, $filterCategory = NULL){
		if(isset($quizID)) $query = 'SELECT * FROM `#__quiz_info` WHERE timestamp = '.$quizID;
			elseif(isset($filterCategory)) $query = 'SELECT * FROM `#__quiz_info` WHERE catid = '.$filterCategory.' ORDER BY timestamp DESC';
				else $query = 'SELECT * FROM `#__quiz_info` ORDER BY timestamp DESC';
	
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$db->setQuery( $query );
		return $db->loadObjectList();		

	}

	//get state from db return true if published
	function toggle_quiz_info(){		
			$id = JRequest::getVar('ID');
			$toggle = JRequest::getVar('toggle');

			$user = JFactory::getUser();			
			$check = NULL;
									
			if($toggle == '10') $check = 10;
			if(($toggle == '1')|| ($toggle == '11')) $check = 11;
		
			if(($check == 11) || ($check == 10)){//update publish state into db 
				$db = JFactory::getDBO();
				$user = JFactory::getUser();
				$infoQuery =  "UPDATE #__quiz_info SET hidden = '".$check."' WHERE timestamp = '".$id."'";
				//echo "updating..".$infoQuery;
				$db->setQuery( $infoQuery );
				$db->query();		
			}else{
				return false;
			}
			return true;
	}
	
	function updateQuizInfo($postData){
		
		//TODO AJAXIFY quizid should be passed in as a variable
		$quizID = JRequest::getVar('quizID');
		$mode = JRequest::getVar('mode'.$quizID);
		$category = JRequest::getVar('category'.$quizID);
		
		if(isset($mode)){
			if($mode == "survey") $survey = 1;
				else $survey = 0;
		}
		
		//Get post and update per quiz
		echo $quizID."<pre>";
		print_r($postData);
		echo "</pre>mode= ".$mode." Survey=".$survey;

		
		
		$infoQuery =  "UPDATE #__quiz_info SET survey = '".$survey."', catid = '".$category."' WHERE timestamp = '".$quizID."'";
		echo "updating..".$infoQuery;

		$db = JFactory::getDBO();
		$db->setQuery( $infoQuery );
		$db->query();		
		return true;
	}

	//input select box with categories
	function getCategories() 
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$db->setQuery("SELECT asset_id AS catid, title FROM #__categories WHERE extension = 'com_inquisitive'");
		$categories = $db->loadObjectList();
		$options = array();
		if($categories){
			$options[0]['catid'] = 0;
			$options[0]['title'] = 'Uncategorised';
			foreach($categories as $category){
				$options[$category->catid]['catid'] = $category->catid;
				$options[$category->catid]['title'] = $category->title;
			}
		}
		return $options;
	}
}
