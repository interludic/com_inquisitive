<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

jimport( 'joomla.application.component.view');
defined('_JEXEC') or die('Restricted access'); 

class InquisitiveViewNewQuiz extends JViewLegacy
{
		
	function display($tpl = null)
	{

		/*	find out permissions status */  
		jimport('joomla.access.access');
		$user   = JFactory::getUser();
		$result = new JObject;
		if (empty($messageId)) {
			$assetName = 'com_inquisitive';
		}else {
			$assetName = 'com_inquisitive.message.'.(int) $messageId;
		}
		
		$actions = JAccess::getActions('com_inquisitive', 'component');
		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName)); 
		}
			
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}

		$quiz_name = JRequest::getVar('quizname');
		$componentName = JRequest::getVar('option');
		$edit_check = JRequest::getVar('editcheck');				
		$ID = JRequest::getVar('ID', time());
		$question_num = JRequest::getVar('question_num', '0');
		if((JRequest::getVar('question') != NULL) or ($question_num == 0)) $question_num++;      		
	
		$this->assignRef('quizname', $quiz_name);		
		$this->assignRef('comName', $componentName);
		$this->assignRef('ID', $ID);
		$this->assignRef('editcheck', $edit_check);		
		$this->assignRef('question_num', $question_num);
		
		parent::display($tpl);
	}
}
?>