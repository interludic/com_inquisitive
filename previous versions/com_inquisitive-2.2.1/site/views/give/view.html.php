<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');

class InquisitiveViewGive extends JViewLegacy
{
	
	function display($tpl = null)
	{			
		$id = JRequest::getVar('qid');
		$uid = JRequest::getVar('uid');
		$giveQuizS = JRequest::getVar('giveQuizString');	
		$componentName = JRequest::getVar('option');

		/*echo "<pre>";
		print_r($giveQuizS);
		echo "</pre>";*/
		
		//TODO detect survey mode, route to survey view or display an article

		$this->assignRef('comName', $componentName);
		$this->assignRef('giveQuizStrings', $giveQuizS);
			
		parent::display($tpl);
	}
}
?>
