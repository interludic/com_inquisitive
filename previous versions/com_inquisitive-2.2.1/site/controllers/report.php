<?php
/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

/**
 * Report Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerReport extends JControllerLegacy
{
	/**
	 * Method to display report to the creator
	 *
	 * @access
	 */
	 
	function display($cachable = false, $urlparams = false){
		//FIXME does the user own the quiz?		
		
		if ((JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) || (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive'))){				
			//$user = JFactory::getUser();
			
			$quizID = JRequest::getVar('quizID');
			//$uid = JRequest::getVar('uid');
			$view = JRequest::getVar('view', 'give');
			$layout = JRequest::getVar('layout', 'default');
			$format = JRequest::getVar('format', 'html');

			$take =$this->getModel('take');
			
				
			JRequest::setVar ( 'view', 'report' );
			//check for survey or quiz mode!

			$quizlist = $this->getModel('quizlist');
			
			$getQuizInfo = $quizlist->getQuizInfo($quizID);
			/*if($getQuizInfo->survey == 1) JRequest::setVar ( 'layout', 'survey' );
				else JRequest::setVar ( 'layout', 'default' );*/
			
			JRequest::setVar ( 'getQuizInfo', $getQuizInfo );				
			JRequest::setVar ( 'layout', 'default' );				
								
			parent::display();

		}else{		
			//FIXME redirector ?			
			$link = "index.php?option=com_users";
			$this->setRedirect($link);
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
	}
}

?>
