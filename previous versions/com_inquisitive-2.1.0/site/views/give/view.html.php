<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Inquisitive Component
 *
 * @package		Joomla.Tutorials
 * @subpackage	Components
 */
 
 //View folder contains view classes and templates. Every view class may have few templates that are stored in tmpl folder. Every view class has the same name view.html.php. And tmpl folder contains html template files. 
 
class InquisitiveViewGive extends JViewLegacy
{
	
	function display($tpl = null)
	{			
		$id = JRequest::getVar('qid');
		$uid = JRequest::getVar('uid');
		$giveQuizS = JRequest::getVar('giveQuizString');	
		$componentName = JRequest::getVar('option');
		$anchorTag = JRequest::getVar('anchorTag');
/*		echo "<pre>";
		print_r($giveQuizS);
		echo "</pre>";*/
				
		$this->assignRef('anchorTag', $anchorTag);
		$this->assignRef('comName', $componentName);
		$this->assignRef('giveQuizStrings', $giveQuizS);
			
		parent::display($tpl);
	}
}
?>
