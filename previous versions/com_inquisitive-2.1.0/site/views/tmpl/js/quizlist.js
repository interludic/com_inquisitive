 	function toggleConfirm(currentStatus, url){
		var msg;
		if((currentStatus == 'publish_g.png') || (currentStatus == 'publish_ox.png')){
			 location.href = url;
			 //alert("ok!");
			 return;
		}else if(currentStatus == 'publish_x.png'){
			msg = "Once you publish this quiz you will not be able to edit it again, you may however make a copy for a new quiz";
		}

		if(confirm(msg)) location.href = url;

	}
	 
	 function trash(url){
		var msg = "Are you sure you want to delete this quiz? \n\nWarning: Users will no longer be able to access their results \nClick ok to delete quiz. ";

		if(confirm(msg)) location.href = url;

	}


function submitbutton(pressbutton){
		switch (pressbutton) {
				
			case 'cancel':				
				document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist";
				document.newQuestion.submit();
				
				return;
				break;


			default:
				document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist&task=updateQuizInfo&quizID="+pressbutton;
				document.newQuestion.submit();
				
				return;
				break;
		}
}
				
var $ij = jQuery.noConflict();

var q=0;

$ij(".cp-img").click(function (e){	
	$ij("div#cp-"+e.currentTarget.id).toggle();
	e.preventDefault();
	if(q==0){	
		e.currentTarget.src = e.currentTarget.src.replace("-down","-up");
		q++;
	}
	else{
		e.currentTarget.src = e.currentTarget.src.replace("-up","-down");
		q--;
	}
});

//Filtering
$ij(".filtershowall").click(function (e){	
	document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist";
	document.newQuestion.submit();
});

$ij(".filterCategorySelect").change(function (e){	
	//alert("show all!");
	//uncheck showall
	//$ij(".filtershowall").attr('checked', false);

	document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist&filtercategory="+$ij(".filterCategorySelect").attr('value');
	document.newQuestion.submit();
});



/* if($.browser.mozilla) $("newQuestion").attr("autocomplete", "off"); */
