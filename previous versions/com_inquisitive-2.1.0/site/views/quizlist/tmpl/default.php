<?php // no direct access
	//TODO refactor http://www.gnu.org/licenses/gpl-2.0.html into headers
	
	defined('_JEXEC') or die('Restricted access'); 
	jimport( 'joomla.html.html.select' );	
	//Check for jquery
	require_once JPATH_COMPONENT . '/views/tmpl/jQuery.php';
	
	$user = JFactory::getUser();
	$rowCount = 0;
	$j = 1;	
	
	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);


	//Display filtering if category list exists.
	$categoryList = $this->categoriesList;
	if(!empty($categoryList)){				
		$filterCategorySelect = "<select class='filterCategorySelect' name='filterCategorySelect'  default='filterClearCategory'>";

		if(!isset($this->filterCategory)) {
			$filtershowall = 'checked="checked"';
			$filterCategorySelected = 'selected=selected';		
			$filterCategorySelect .= "<option value='showall' ".$filterCategorySelected."> </option>";
		}else{ 
			$filtershowall = NULL;
		}					

		foreach($categoryList as $filteredCategory){
			$filterCategorySelected = NULL;				
			/*if($this->filterCategory == $filteredCategory['catid']) $filterCategorySelected = 'selected=selected';				
				else $filterCategorySelected = NULL;*/
				
			$filterCategorySelect .= "<option value='".$filteredCategory['catid']."' ".$filterCategorySelected.">".$filteredCategory['title']."</option>";
			
		}
		$filterCategorySelect .= "</select>";
		
		$filterQuizlist = 'Filter: Show all <input class="filtershowall" type="radio" '.$filtershowall.' name="filtershowall">';
		$filterQuizlist .= 'Category '.$filterCategorySelect.'<br/>';
	} 



	echo '<h2>Quiz List</h2><form id="newQuestion" name="newQuestion" method="post">';
	if(empty($this->quizArray)){
		echo $filterQuizlist;
		echo "<br/>Nothing found";
	}else{
		echo $filterQuizlist;
		foreach($this->quizArray as $row){
			//Check is owner
			if($user->username == $row->teacher_name) $quizOwner = TRUE;
				else $quizOwner = FALSE;
			
			//Check user has write access and is quiz owner
			if(($this->usertype == "Publisher") && ($quizOwner)) $quizOwnerWrite = TRUE;
				else $quizOwnerWrite = FALSE; //User is a guest
			
			//Super Admin true?	
			if(isset($this->superUser)) {
				$quizOwnerWrite = TRUE;
				$quizOwner = TRUE;
			}
			
			//check first row
			if($rowCount == 0){	?>
				
				<div class="base-layer%20list-fixed">
				<div class="table-row-top">
					<div class="left-cell-big">Quiz Name</div>					
				<?php 
				if($quizOwnerWrite){?>
					<div class="left-cell-small">&nbsp;<!--Report & hits --></div>
					<!-- <div class="left-cell-small">Edit</div> -->
					<div class="left-cell-small">Status</div>				        						
				<?php } ?>
				</div></div><!-- end table-row-top -->
			<?php
			}//End first row
			$rowCount++;			
			
        	
				switch($row->hidden){	//get toggle state
					case "11":  //published (read only)
						$img = "publish_g.png";
						$alt = 'Click here to unpublish this.';										
						$toggleStatus = '10';
						$edittag = 'Preview answers';
						$editimg = 'preview.png';
					break;
					case "10":	//Unpublished, previously published (read only)
						$img = "publish_ox.png";
						$alt = 'Click here to publish this.';		
						$toggleStatus = '11';
						$edittag = 'Preview answers';
						$editimg = 'preview.png';						
					break;
					case ("0" || NULL):	//Unpublished, newquiz (writeable)						
						$img = "publish_x.png";
						$alt = 'Click here to publish this. Once published you will no longer be able to edit.';		
						$toggleStatus = '11';
						$edittag = 'Edit';
						$editimg = 'edit.png';
					break;
				}
				
				//Skip displaying quiz if user is not the owner and the quiz is unpublished				
				if((!$quizOwner) && ($toggleStatus != '10')) continue;
				
			if($quizOwnerWrite){ ?>        

		<div class="table-row-list">
			<div class="left-cell-big">
            
            <!-- Quiz Name -->       	
			<?php 
           	$quizNameLink = 'index.php?option='.JRequest::getVar('option').'&amp;controller=take&amp;task=sitquiz&amp;ID='.$row->timestamp;
			
			if(isset($row->quiz_name)){  ?>
				<a href="<?php echo JRoute::_($quizNameLink); ?>"><?php echo $row->quiz_name;?></a>
			<?php }else{ ?>
				<a href="<?php echo JRoute::_($quizNameLink); ?>">Unlabeled name</a>
			<?php } ?>
             </div>

            <div class="left-cell-small">&nbsp;
            <!-- Results -->
                <?php 		
                //TODO Add percentage		
                if($this->usertype != "Guest"){                 
					if($row->result != NULL){
						$reportAddress = "index.php?option=".JRequest::getVar('option')."&amp;controller=report&amp;quizID=".$row->timestamp;
						$reportIcon = '<img src="components/com_inquisitive/views/tmpl/images/cp_reports32.png" width="32" height="32" alt="Report" />';
	                    $reportLink = "<a href='".JRoute::_($reportAddress)."' title='Hits: ".$row->result."'>".$reportIcon."</a>";
						echo $reportLink;
					}
                }else{
                    echo "" ;
                }
                ?>
            </div>
    
            <div class="left-cell-small">
            <!-- Publisher Quiz -->    
                <span class="editlinktip hasTip" title="<?php echo JText::_( $alt );?>">
                    <a href="<?php echo $_SERVER["REQUEST_URI"] ?>#">
                        <?php 
                        $publish_link = "index.php?option=".JRequest::getVar('option')."&amp;controller=quizlist&amp;ID=".$row->timestamp."&amp;toggle=".$toggleStatus;
                        /* echo "publish link ".$publish_link."<br/><br/><br/>";
                         echo JRoute::_($publish_link);*/
                         ?>
                        <img src="components/com_inquisitive/views/tmpl/images/<?php echo $img;?>" width="32" height="32" alt="<?php echo $alt; ?>" onclick="toggleConfirm('<?php echo $img; ?>', '<?php echo $publish_link; ?>')" />
                     </a>
                </span>
            </div>
            <!-- Trash Quiz -->                                  
            <div class="left-cell-small">
                <span class="editlinktip hasTip" title="<?php echo JText::_( "Trash" );?>">
                <a href="<?php echo $_SERVER["REQUEST_URI"] ?>#">
                    <img src="components/com_inquisitive/views/tmpl/images/trash.png" width="32" height="32" alt="Trash" onclick="trash('index.php?option=<?php echo JRequest::getVar('option'); ?>&amp;controller=quizlist&amp;task=getbin&amp;ID=<?php echo $row->timestamp; ?>')" />
                 </a>
                </span>
            </div>
            <!-- Edit Quiz &controller-->
            <div class="left-cell-small">
                <span class="editlinktip hasTip" title="<?php echo JText::_( $edittag );?>">
                    <?php $link = "index.php?option=".JRequest::getVar('option')."=newquiz&amp;task=edit&amp;ID=".$row->timestamp;  ?>
                    <a href="<?php echo $link; ?>">
                        <img src="components/com_inquisitive/views/tmpl/images/<?php echo $editimg;?>" width="32" height="32" alt="<?php echo $edittag; ?>" />
                    </a>			
                </span>
            </div>
            <!-- Control panel (more/down button)-->
            <div class="left-cell-small cp-img" id="<?php echo $row->timestamp; ?>">
                <a href="<?php echo $_SERVER["REQUEST_URI"] ?>#" class="link">
                <img src="components/<?php echo $this->comName; ?>/views/tmpl/images/cp_morearrow-down.png" alt="more"/></a>
            </div>
            <div class='clearfix'>&nbsp;</div>
            <div id="cp-<?php echo $row->timestamp;?>" class="table-row-cp" style="display: none">
                <div class="left-cell-small"><a class="tooltip-inquisitive" href="#">Mode<span class="custom info"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/help.png" alt="Info" height="30" width="30" /><em>Mode</em> When quiz mode is selected answers will be displayed at the end of the quiz to help users learn from any errors made. Selecting survey mode will thank the users but suppress answers.</span></a></div><div class="left-cell-big">
                <input type="radio" value="survey" name="mode<?php echo $row->timestamp;?>" <?php if($row->survey == 1) echo 'checked="checked"'; ?> />Survey &nbsp;&nbsp;&nbsp;
                <input type="radio" value="quiz" name="mode<?php echo $row->timestamp;?>" <?php if($row->survey == 0) echo 'checked="checked"'; ?> />Quiz &nbsp;&nbsp;&nbsp;</div><br /><br />
               
            	<?php
                if(!empty($categoryList)){ ?>				                
	                <!-- Categories List -->
	                <div class="left-cell-small"><a class="tooltip-inquisitive" href="#">Category<span class="custom info"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/help.png" alt="Info" height="30" width="30" /><em>Categories</em> Select a category for this quiz.</span></a></div>
	                	<div class="left-cell-big">
	                	<?php
													
							$categorySelectQuiz = "<select name='category".$row->timestamp."'>";
													
	                		foreach($categoryList as $category){                			
	                			if($row->catid == $category['catid']) $selected = 'selected=selected';
	                				else $selected = NULL;
	                			$categorySelectQuiz .= "<option value='".$category['catid']."' ".$selected.">".$category['title']."</option>";	
	                			$selected = NULL;
	                		}
	                		$categorySelectQuiz .= "</select>";
							echo $categorySelectQuiz;						
	                	?>
	                	</div>
	                	<div class='clearfix'>&nbsp;</div>
	            <?php } ?>
                
                <br/><br/>
                <div class="left-cell-big">
                    <div class="buttons-footer text-btn">
                        <input type="button" onclick="submitbutton(this.name);" value="Save" name="<?php echo $row->timestamp;?>"> <!-- id="submitsave" -->
                    </div>
                    <div class="buttons-footer text-btn">
                        <button onclick="submitbutton('cancel')" type="button">Cancel</button>
                    </div>
                </div>
                <div class='clearfix'>&nbsp;</div>
                <div class="left-cell-small">Created on </div><div class="left-cell-big"><?php echo date("d m Y",$row->timestamp); ?></div>
            </div>
					
        
<?php }//end publisher check 

if(!$quizOwnerWrite){ ?>

		<div class="table-row-list">
			<div class="left-cell-big">
            
            <!-- Quiz Name -->
            	<?php $link = "index.php?option=".JRequest::getVar('option')."&amp;controller=take&amp;task=sitquiz&amp;ID=".$row->timestamp.$this->anchorTag;  ?>

				<a href="<?php echo $link; ?>"><?php echo $row->quiz_name;?></a>
             </div>

		<div class="left-cell-small">&nbsp;
        <!-- Results -->
	<?php // Show results for student or teacher
	if($this->usertype = "Guest") echo ""; //Add percentage
		elseif($row->result != NULL) echo $row->result;?> </div>
		
	<!--	<div class="left-cell-small">edit</div> -->
			<div class="left-cell-small">
            </div>
                    
		<div class="left-cell-small">
		</div>        
<?php } ?>
  <div class="space-line"></div>
</div>
<?php		

		}
	 }
	echo "</form>";
 ?>
 
<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>

<script type="text/javascript" src="components/<?php echo $this->comName; ?>/views/tmpl/js/quizlist.js"></script> 