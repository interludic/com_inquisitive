<?php
/**
 * @package    Joomla.Inquisitive
 * @subpackage Components
 * @link http://inquisitive.net.au
 * @license		GNU/GPL
 */

defined('_JEXEC') or die;

// Register dependent classes.

/**
 * Method to build a SEF route.
 *
 * @param   array  &$query  An array of route variables.
 *
 * @return  array  An array of route segments.
 *
 * @since   2.5
 */
function InquisitiveBuildRoute(&$query)
{
	static $menu;
	$segments = array();

	// Load the menu if necessary.
	if (!$menu)
	{
		$menu = JFactory::getApplication('site')->getMenu();
	}

	/*
	 * First, handle menu item routes first. When the menu system builds a
	 * route, it only provides the option and the menu item id. We don't have
	 * to do anything to these routes.
	 */
	if (count($query) === 2 && isset($query['Itemid']) && isset($query['option']))
	{
		return $segments;
	}

	/*
	 * Next, handle a route with a supplied menu item id. All system generated
	 * routes should fall into this group. We can assume that the menu item id
	 * is the best possible match for the query but we need to go through and
	 * see which variables we can eliminate from the route query string because
	 * they are present in the menu item route already.
	 */
	if (!empty($query['Itemid']))
	{
		// Get the menu item.
		$item = $menu->getItem($query['Itemid']);
		
		// Check if the view matches.
		if ($item && @$item->query['view'] === @$query['view'])
		{
			unset($query['view']);
		}

/*		// Check if the search query filter matches.
		if ($item && @$item->query['f'] === @$query['f'])
		{
			unset($query['f']);
		}

		// Check if the search query string matches.
		if ($item && @$item->query['q'] === @$query['q'])
		{
			unset($query['q']);
		}*/

/*		echo "<pre>";		
		var_dump($item->query['view']);
		var_dump($query);
		var_dump($query['controller']);		
		var_dump($query['task']);
		var_dump($query['ID']);
		var_dump($query['Itemid']);
		echo "</pre>";*/

//QuizList 		
		if(isset($query['controller'])){
			 array_push($segments, $query['controller']);
			 unset($query['controller']);
		}		
		if(isset($query['task'])){
			 array_push($segments, $query['task']);
			 unset($query['task']);
		}				
		if(isset($query['quizID'])){
			 array_push($segments, $query['quizID']);
			 unset($query['quizID']);
		}		
		if(isset($query['ID'])){
			 array_push($segments, $query['ID']);
			 unset($query['ID']);
		}		
		if(isset($query['Itemid'])){
			 array_push($segments, $query['Itemid']);
			 unset($query['Itemid']);
		}
		if(isset($query['toggle'])){
			 array_push($segments, $query['toggle']);
			 unset($query['toggle']);
		}
				
		return $segments;
	}

	/*
	 * Lastly, handle a route with no menu item id. Fortunately, we only need
	 * to deal with the view as the other route variables are supposed to stay
	 * in the query string.
	 */
/*	if (isset($query['view']))
	{
		// Add the view to the segments.
		$segments[] = $query['view'];
		unset($query['view']);
	}*/

	return $segments;
}

/**
 * Method to parse a SEF route.
 *
 * @param   array  $segments  An array of route segments.
 *
 * @return  array  An array of route variables.
 *
 * @since   2.5
 */
function InquisitiveParseRoute($segments)
{

	$vars = array();

	// Check if the view segment is set and it equals search or advanced.
	if (@$segments[0] === 'take'){
		$vars['controller'] = $segments[0];
		$vars['task'] = $segments[1];
		if(isset($segments[2]))$vars['ID'] = $segments[2];	
		if(isset($segments[3]))$vars['Itemid'] = $segments[3];	
	//		$vars['Itemid'] = $segments[0];		
	}
	if (@$segments[0] === 'report'){
		$vars['controller'] = $segments[0];
		if(isset($segments[1])) $vars['quizID'] = $segments[1];	
		if(isset($segments[2]))$vars['Itemid'] = $segments[2];	
	}
	
	/*	echo "<pre>";		
	var_dump($vars);
	echo "</pre>";*/		


	return $vars;
}

