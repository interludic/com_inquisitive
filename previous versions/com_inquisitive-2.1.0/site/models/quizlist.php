<?php


/**
 * Inquisitive Model for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport('joomla.application.component.helper');

jimport( 'joomla.application.component.model' );
jimport( 'joomla.database.database' );


/**
 * Inquisitive Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
 
 //Models contain Model classes. 
 //One Model class is equal to one DB table.
 
class InquisitiveModelQuizList extends JModelLegacy
{			

	function trashThis($ID = NULL){
		$db = JFactory::getDBO();	
			
		$del__quizid_questionid = 'DELETE FROM #__quizid_questionid WHERE ID='.$ID.'';		
		$db->setQuery( $del__quizid_questionid );
		$db->query();
		if($db->getErrorMsg()) echo $db->getErrorMsg();	
		
		/* Preserve reporting ??? */
		$del_answers = 'DELETE FROM #__quiz_answer WHERE timestamp='.$ID.'';
		$db->setQuery( $del_answers );
		$db->query();		
		if($db->getErrorMsg()) echo $db->getErrorMsg();			
		
		$del_questions = 'DELETE FROM #__quiz_question WHERE quizID='.$ID.'';
		$db->setQuery( $del_questions );
		$db->query();		
		if($db->getErrorMsg()) echo $db->getErrorMsg();			
		
		$del_quiz_info = 'DELETE FROM #__quiz_info WHERE timestamp='.$ID.'';
		$db->setQuery( $del_quiz_info );
		$db->query();
		if($db->getErrorMsg()) echo $db->getErrorMsg();			
		
		$del_quiz_attempt = 'DELETE FROM #__quiz_attempt WHERE quizID='.$ID.'';
		$db->setQuery( $del_quiz_attempt);		
		$db->query();		
		if($db->getErrorMsg()) echo $db->getErrorMsg();	
		
		//echo "trash ? = ".$ID."<br/>".$del__quizid_questionid."<br/>".$del_answers."<br/>".$del_questions."<br/>".$del_quiz_info."<br/>".$del_quiz_attempt;
		return $ID;
	}
			
	
	  
	 //TODO this was duplicated from take.php model, CAN MVC not reference other models functions? (return modified) 16/10
	 //TODO where should quizTable be coming from instead,.. 'inquisitivemodel'??
	 /*
	 * quiz table returns true or false
	 * pass the column name in plain text
	 
	function quizTable($columnName){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT '.$columnName.' FROM `#__quiz` WHERE 1';
		$db->setQuery( $queryQ );
		$result = $db->loadResult();
		
		if($db->getErrorMsg())$db->getErrorMsg();
		
		if(($result == NULL) or ($result == "0")) return false;
			else return $result;
	}*/
/*		
removed 16/08/2012 - no call to function
	
	function getFunction(){
		$db =& JFactory::getDBO();
		$user =& JFactory::getUser();
		$query = '';
		
		if($user->usertype == 'Publisher') $query = 'SELECT * FROM `#__quiz_info` WHERE teacher_name = "'.$user->username.'"';// LIMIT 0 , 10';
			elseif ($user->usertype == 'Super Administrator')  $query = 'SELECT * FROM `#__quiz_info`';
				else $query = 'SELECT * FROM `#__quiz_info` WHERE hidden = "NULL"';// LIMIT 0 , 10';
				
		$db->setQuery( $query );
		$db->query();

		$getRowCount = $db->getNumRows();
		
		return $query;
				
	}*/
		/*****************************************************************
			// check quiz life
				

				function quizAttempt($user, $quizid){
					return how many times completed 
				}

				function quizInfo($quizid){				
					//quizinfo table
					return how many times allowed				
				}

				// if var = 0 (do not display quiz in list)
				// if var >= 1 run quiz, remove life upon completion
				// new table - with UID from jos_users as relationship.
			*****************************************************************/
			
	
	/**************************************************************
	 * Return amount of times a student has accessed a quiz,
	 * if new user, create row in quiz_attempt
	 * return null for none left. 
	 * return 0 = first attempt yet to be used.
	 **************************************************************/
		 
	function quizAttemptNum($quizID){
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		$query = 'SELECT attemptNum FROM `#__quiz_attempt` WHERE quizID = "'.$quizID.'" and userID = "'.$user->username.'"';		
		$db->setQuery( $query );
		$db->query();
		$attemptNum = $db->loadRow();
		
		/*-echo "attempt - <pre>";
		print_r($attemptNum);
		echo "</pre>";//.$attemptNum;*/
		
		//row does not exist. Create (assumed new user!!)
		if($attemptNum['0'] == NULL) {			
			$queryAttempt = "INSERT INTO #__quiz_attempt (quizID, userID, attemptNum) VALUES ('".$quizID."', '".$user->username."', '0');";
			$db->setQuery( $queryAttempt );
			$db->query();
			
			return 1;
		}
		
		if($attemptNum['0'] == 0) return NULL;
			else return $attemptNum['0'];
	}

	
	function getQuizInfo($quizID){
		$db = JFactory::getDBO();
		$query = 'SELECT * FROM `#__quiz_info` WHERE timestamp = '.$quizID;			
		$db->setQuery( $query );
		$quizArray = $db->loadObject();

		return $quizArray;
	}
		
	function quizList($quizID = NULL, $filterCategory = NULL){
		if(isset($quizID)) $query = 'SELECT * FROM `#__quiz_info` WHERE timestamp = '.$quizID;
			elseif(isset($filterCategory)) $query = 'SELECT * FROM `#__quiz_info` WHERE catid = '.$filterCategory.' ORDER BY timestamp DESC';
				else $query = 'SELECT * FROM `#__quiz_info` ORDER BY timestamp DESC';
	
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$db->setQuery( $query );
		return $db->loadObjectList();		

//		return $quizArray;
		
		//remove quiz depending on attempt
		/*
		$i = 0;
		if(isset($quizArray)){	
			foreach($quizArray as $row){
				//$this->quizAttemptNum($row->timestamp);				
				//IF quiz info , attemptMax = u this quiz is displayed regardless.
				//echo "max ".$quizArray[$i]->attemptMax;
				if($quizArray[$i]->attemptMax != "u"){
					if(($this->quizAttemptNum($row->timestamp) >= 1) and ($user->username != $row->teacher_name)){ //AttemptMax should be referenced here
						//echo $this->quizAttemptNum($row->timestamp)." remove - <pre>";
						//print_r($quizArray[$i]);
						//echo "</pre>";
						unset($quizArray[$i]);	
					}					
				}
			$i++;
			}
			return $quizArray;
		}else{
			return NULL;
		}*/
	}

	//get state from db return true if published
	function toggle_quiz_info(){		
			$id = JRequest::getVar('ID');
			$toggle = JRequest::getVar('toggle');

			$user = JFactory::getUser();			
			$check = NULL;
									
			if($toggle == '10') $check = 10;
			if(($toggle == '1')|| ($toggle == '11')) $check = 11;
		
			if(($check == 11) || ($check == 10)){//update publish state into db 
				$db = JFactory::getDBO();
				$user = JFactory::getUser();
				$infoQuery =  "UPDATE #__quiz_info SET hidden = '".$check."' WHERE timestamp = '".$id."'";
				//echo "updating..".$infoQuery;
				$db->setQuery( $infoQuery );
				$db->query();		
			}else{
				return false;
			}
			return true;
	}
	
	function updateQuizInfo($postData){
		
		//TODO AJAXIFY quizid should be passed in as a variable
		$quizID = JRequest::getVar('quizID');
		$mode = JRequest::getVar('mode'.$quizID);
		$category = JRequest::getVar('category'.$quizID);
		
		if(isset($mode)){
			if($mode == "survey") $survey = 1;
				else $survey = 0;
		}
		
		//Get post and update per quiz
		echo $quizID."<pre>";
		print_r($postData);
		echo "</pre>mode= ".$mode." Survey=".$survey;

		
		
		$infoQuery =  "UPDATE #__quiz_info SET survey = '".$survey."', catid = '".$category."' WHERE timestamp = '".$quizID."'";
		echo "updating..".$infoQuery;

		$db = JFactory::getDBO();
		$db->setQuery( $infoQuery );
		$db->query();		
		return true;
	}

	//input select box with categories
	function getCategories() 
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$db->setQuery("SELECT asset_id AS catid, title FROM #__categories WHERE extension = 'com_inquisitive'");
		$categories = $db->loadObjectList();
		$options = array();
		if($categories){
			$options[0]['catid'] = 0;
			$options[0]['title'] = 'Uncategorised';
			foreach($categories as $category){
				$options[$category->catid]['catid'] = $category->catid;
				$options[$category->catid]['title'] = $category->title;
			}
		}
		return $options;
	}
}
