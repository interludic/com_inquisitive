DROP TABLE IF EXISTS `#__quiz_temp`; 

CREATE TABLE `#__quiz_report_question` (
  `index` int(11) AUTO_INCREMENT,
  `quiz_id` text,
  `user_id` text,
  `user_score` text,
  `timestamp` text,
  `question_id` text,
  PRIMARY KEY (`index`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE `#__quiz_report_quiz` (
  `index` int(11) AUTO_INCREMENT,
  `quiz_id` text,
  `user_id` text,
  `user_score` text,
  `timestamp` text,
  PRIMARY KEY (`index`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

