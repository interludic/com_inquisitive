DROP TABLE IF EXISTS `#__quiz`;
DROP TABLE IF EXISTS `#__quizid_questionid`;
DROP TABLE IF EXISTS `#__quiz_answer`;
DROP TABLE IF EXISTS `#__quiz_info`;
DROP TABLE IF EXISTS `#__quiz_question`;
DROP TABLE IF EXISTS `#__quiz_attempt`; 
DROP TABLE IF EXISTS `#__quiz_results`; 
DROP TABLE IF EXISTS `#__quiz_user_accuracy`; 
DROP TABLE IF EXISTS `#__quiz_report_question`; 
DROP TABLE IF EXISTS `#__quiz_report_quiz`; 


CREATE TABLE `#__quiz` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `quizID` text NOT NULL,
  `greeting` text NOT NULL,
  `email` text NOT NULL,
  `email_creator` text NOT NULL,
  `address` text,
  `anchor_tag` text,
  `notify_creator` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`index`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE `#__quiz_report_question` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` text NOT NULL,
  `user_id` text NOT NULL,
  `user_score` text NOT NULL,
  `timestamp` text NOT NULL,
  `question_id` text NOT NULL,
  PRIMARY KEY (`index`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE `#__quiz_report_quiz` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` text NOT NULL,
  `user_id` text NOT NULL,
  `user_score` text NOT NULL,
  `timestamp` text NOT NULL,
  PRIMARY KEY (`index`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;


CREATE TABLE `#__quizid_questionid` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `ID` text NOT NULL,
  `questionNum` text NOT NULL,
  `listOrder` text NOT NULL,
  PRIMARY KEY (`index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE `#__quiz_answer` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `answer` text NOT NULL,
  `keywords` text,
  `abs_answer` text,  
  `correct` text NOT NULL,
  `question_num` int(11) NOT NULL,
  `type` char(1) DEFAULT NULL,
  KEY `index` (`index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE `#__quiz_attempt` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `quizID` text NOT NULL,
  `userID` text NOT NULL,
  `attemptNum` text,
  PRIMARY KEY (`index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE `#__quiz_info` (
 `index` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) DEFAULT NULL,
  `attemptMax` text NOT NULL,
  `quiz_name` text,
  `result` text,
  `teacher_name` text,
  `hidden` int(11) DEFAULT NULL,
  `replicated` int(11) DEFAULT NULL,
  `survey` int(11) NOT NULL DEFAULT '1',
  `catid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`index`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE `#__quiz_question` (
  `questionIndex` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) DEFAULT NULL,
  `quizID` int(11) DEFAULT NULL,
  `question` text ,
  `location` text ,
  `flv` int(11) DEFAULT NULL,
  PRIMARY KEY (`questionIndex`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;


CREATE TABLE `#__quiz_results` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `student_name` text NOT NULL,
  `q` text NOT NULL,
  `a` text NOT NULL COMMENT 'answer',
  `result` text NOT NULL,
  `id` text NOT NULL,
  PRIMARY KEY (`index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE `#__quiz_user_accuracy` (
  `index` INT(11) NOT NULL AUTO_INCREMENT ,
  `uid` TEXT COMMENT 'User ID',
  `questionid` TEXT COMMENT 'Question ID',
  `percentage` TEXT COMMENT 'Percentage correct of the absolute question',
  PRIMARY KEY ( `index` ) ,
  INDEX ( `index` )
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


INSERT INTO `#__quiz` VALUES(0, '0', 'Hello', '1', '1', NULL, NULL, '1');


-- insert dummy data


INSERT INTO `#__quizid_questionid` (`index`, `ID`, `questionNum`, `listOrder`) VALUES
(11, '1350709824', '7', ''),
(12, '1350709824', '8', ''),
(13, '1350709824', '9', ''),
(14, '1350709824', '10', ''),
(15, '1350709824', '12', ''),
(16, '1350709824', '14', '');


INSERT INTO `#__quiz_answer` (`index`, `timestamp`, `num`, `answer`, `keywords`, `abs_answer`, `correct`, `question_num`, `type`) VALUES
(16, 1350709824, 1, 'Purple', '', '', '', 1, 'r'),
(17, 1350709824, 2, 'Blue', '', '', '', 1, 'r'),
(18, 1350709824, 3, 'Green', '', '', '', 1, 'r'),
(19, 1350709824, 4, 'All pastel colours', '', '', 'on', 1, 'r'),
(20, 1350709824, 1, 'Shopping', '', '', 'on', 2, 'c'),
(21, 1350709824, 2, 'Watch TV', '', '', '', 2, 'c'),
(22, 1350709824, 3, 'Dancing', '', '', 'on', 2, 'c'),
(23, 1350709824, 4, 'Exercise', '', '', 'on', 2, 'c'),
(24, 1350709824, 5, 'Cooking/baking', '', '', 'on', 2, 'c'),
(25, 1350709824, 6, 'Reading', '', '', 'on', 2, 'c'),
(26, 1350709824, 1, '1 hour', '', '', '', 3, 'r'),
(27, 1350709824, 2, '30 minutes', '', '', '', 3, 'r'),
(28, 1350709824, 3, '15 minutes', '', '', '', 3, 'r'),
(29, 1350709824, 4, '40 minutes', '', '', 'on', 3, 'r'),
(30, 1350709824, 1, 'Durian', '', '', '', 4, 'r'),
(31, 1350709824, 2, 'Chocolate', '', '', '', 4, 'r'),
(32, 1350709824, 3, 'Tiramisu', '', '', '', 4, 'r'),
(33, 1350709824, 4, 'Caramel', '', '', '', 4, 'r'),
(34, 1350709824, 5, 'Pistachio', '', '', 'on', 4, 'r'),
(35, 1350709824, 1, '', 'Kit Yen, kitchen', '', '', 5, 't'),
(36, 1350709824, 1, 'Malaysia', '', '', '', 6, 'c'),
(37, 1350709824, 2, 'America', '', '', '', 6, 'c'),
(38, 1350709824, 3, 'Hong Kong', '', '', 'on', 6, 'c'),
(39, 1350709824, 4, 'Taiwan', '', '', '', 6, 'c'),
(40, 1350709824, 5, 'Korea', '', '', 'on', 6, 'c'),
(41, 1350709824, 6, 'Germany', '', '', 'on', 6, 'c'),
(42, 1350709824, 7, 'Thailand', '', '', '', 6, 'c'),
(43, 1350709824, 8, 'Israel', '', '', '', 6, 'c');


INSERT INTO `#__quiz_question` (`questionIndex`, `num`, `quizID`, `question`, `location`, `flv`) VALUES
(3, NULL, NULL, NULL, NULL, NULL),
(7, 1, 1350709824, 'What is Jo''s favourite colour?', '', 0),
(8, 2, 1350709824, 'What does Jo like to do on the weekend?', '', 0),
(9, 3, 1350709824, 'How long does Jo take to get ready for work?', '', 0),
(10, 4, 1350709824, 'What is Jo''s favourite ice cream flavour?', '', 0),
(11, NULL, NULL, NULL, NULL, NULL),
(12, 5, 1350709824, 'What is Jo''s middle name?', '', 0),
(13, NULL, NULL, NULL, NULL, NULL),
(14, 6, 1350709824, 'Which country has Jo traveled to?', '', 0);


INSERT INTO `#__quiz_info` (`index`, `timestamp`, `attemptMax`, `quiz_name`, `result`, `teacher_name`, `hidden`, `replicated`, `survey`) VALUES
(1, 1350709824, 'u', 'How much do I know about Jo', '73', 'admin', 11, NULL, 0);