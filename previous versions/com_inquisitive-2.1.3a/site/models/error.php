<?php
/*ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);
*/
		
/**
 * Inquisitive Model for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport( 'joomla.application.component.model' );

/**
 * Inquisitive Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
 
 //Models contain Model classes. 
 //One Model class is equal to one DB table.*/
 
class InquisitiveModelError extends JModelLegacy
{	

	function error(){
		//get errorNum
		
		$msg = "<h1>Sorry your permissions are a bit low</h1><br/> <a href='http://webdesignfavourites.com/portfolio/blog/2012/02/05/inquisitive-quiz-survey-for-joomla-1-5/'> Upgrade them by contacting support!</a>";
		return $msg;
	}

}

