var $ij = jQuery.noConflict();
var q=0;


function toggleConfirm(currentStatus, url){
	var msg;
	if((currentStatus == 'publish_g.png') || (currentStatus == 'publish_ox.png')){
		 location.href = url;
		 //alert("ok!");
		 return;
	}else if(currentStatus == 'publish_x.png'){
		msg = "Once you publish this quiz you will not be able to edit it again, you may however make a copy for a new quiz";
	}

	if(confirm(msg)) location.href = url;

}
 
function trash(quizID){	 	
	var msg = "Are you sure you want to delete this quiz? \n\nWarning: Users will no longer be able to access their results \nClick ok to delete quiz. ";
	
	if(confirm(msg)){
		$ij.ajax({		
	    	url: 'index.php?option=com_inquisitive&controller=ajax&task=deletequiz&format=raw',
	    	type: 'post',
	    	data: {'quizid': quizID},
	    	success: function(data, status) {	      		
		        if(data == "trashed"+quizID) {		        	
			        $ij("#row"+quizID).fadeOut(300);
		        }
	      	},
		    error: function(xhr, desc, err) {
		    	console.log(xhr);
		        console.log("Details: " + desc + "\nError:" + err);
		    }
		}); // end ajax call		
	}		
}

//technically this doesnt need to be ajax. just an excercise.
function replicatequiz(quizID){
	$ij.ajax({
    	url: 'index.php?option=com_inquisitive&controller=ajax&task=replicatequiz&format=raw',
    	type: 'post',
    	data: {'quizid': quizID},
    	success: function(json, status) {	    		
			$ij.each(json, function(i, item) {
          		if(typeof item == 'object') {
          			if(quizID == item.quizid) {		        	
          				//alert("quizid="+item.quizid+" new = "+item.newquizid);
		        		//$ij("#row"+quizID).fadeOut(300);
				        //http://dev.webdesignfavourites.com/inquisitive.net.au/index.php/component/inquisitive/?controller=newquiz&task=edit&ID=1394586341
				        document.newQuestion.action = "index.php?option=com_inquisitive&controller=newquiz&task=edit&ID="+item.newquizid;
		        		document.newQuestion.submit();		     
			        }      
			    } 
		        else {
	            	return false;
	    	    }
	        }) // end $.each() loop

	        
      	},
	    error: function(xhr, desc, err) {
	    	console.log(xhr);
	        console.log("Details: " + desc + "\nError:" + err);
	    }
	}); // end ajax call		
}


function submitbutton(pressbutton){
				//	alert("press 1 = "+pressbutton);		
		switch (pressbutton) {
				
			case 'cancel':				
				document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist";
				document.newQuestion.submit();
				
				return;
				break;


			default:
				document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist&task=updateQuizInfo&quizID="+pressbutton;
				document.newQuestion.submit();
				
				return;
				break;
		}
}
				

$ij(".cp-img").click(function (e){	
	$ij("div#cp-"+e.currentTarget.id).toggle();
	e.preventDefault();
	e.currentTarget.src = "-up";
	if(q==0){	
		e.currentTarget.src = e.currentTarget.src.replace("-down","-up");
		q++;
	}
	else{
		e.currentTarget.src = e.currentTarget.src.replace("-up","-down");
		q--;
	}
});

//Filtering
$ij(".filtershowall").click(function (e){	
	document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist";
	document.newQuestion.submit();
});

$ij(".filterCategorySelect").change(function (e){	
	//alert("show all!");
	//uncheck showall
	//$ij(".filtershowall").attr('checked', false);

	document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist&filtercategory="+$ij(".filterCategorySelect").attr('value');
	document.newQuestion.submit();
});



/* if($.browser.mozilla) $("newQuestion").attr("autocomplete", "off"); */
