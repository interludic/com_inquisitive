<?php // no direct access

defined('_JEXEC') or die('Restricted access'); 

	$checkBoxCount = 0;      
	$checkBoxFlag = 0;
	$itemid = JRequest::getint( 'Itemid' );

	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);
?>

    <script type="text/javascript">
		function submitbutton(pressbutton){
			switch (pressbutton) {
				case 'cancel':

					document.takeQuiz.action = "<?php echo JRoute::_("index.php?option=".$this->comName."&controller=quizlist&Itemid=".$itemid); ?>";
					document.takeQuiz.submit();
					return;
					break;

				case 'publish':
					/* var additionalEmail = document.getElementById('additionalEmail');
					if(additionalEmail.value == 'Email a friend') additionalEmail.value = ""; */
					document.getElementById('loading').style.visibility='visible'; 
					//Itemid is being posted below.
					document.takeQuiz.action = "<?php echo JRoute::_("index.php?option=".$this->comName."&controller=take&task=publish&view=take"); ?>";
					document.takeQuiz.submit();
					return;
					break;				

			}
		}

		function inputTextbox(contents){
			var inputText = document.getElementById('additionalEmail');	
			if(inputText.value == "")	inputText.value = "Email a friend";
				else if(inputText.value == "Email a friend") keywords.value = "";
		}

</script>

<form name="takeQuiz" method="post" onsubmit="return validate_form(this)">  
<?php 
	$i = 0; //count total questions, pass to results as total answers.
	//QUESTIONS LOOP
	foreach ( $this->rawQuestions as $rowQ ) {
		$i++;
		echo $rowQ->location; ?>
<div class="table-row-question">
	<div class="headingLabel">
    	Question #
        <div class="numText" ID="questionNum"><?php echo $rowQ->num; ?></div>
    </div><!-- heading label end -->
	<input type="hidden" value="<?php echo $rowQ->questionIndex; ?>" name="QQ<?php echo $rowQ->num; ?>" />
	<div class="text-question "> <?php echo $rowQ->question; ?> </div><!-- text question end -->
	<input type="hidden" value="<?php echo $rowQ->question; ?>" name="questionQQ<?php echo $rowQ->num; ?>" />
	<div class="space-line"></div> <!-- space line end -->
</div> <!-- question end -->

		<?php
		foreach ( $this->quizAnswers as $rowA ) {
				
				
			//Convert id to number	
			if($rowQ -> num == $rowA->question_num){ //match question and answer.									
				if(($rowA -> type == 'c') && ($checkBoxFlag != 1)) $checkBoxCount++; 
		?>
<div class="table-row-answer">
	<div class="headingLabel">
		Answer:
	    <p class="numText" ID="answerNum" <?php if($rowA->type == "t") echo "style='display: none;'"; ?>><?php  echo $rowA->num; ?></p>
	</div><!-- heading label end -->
	<div class="left-layer11"></div><!-- left layer end -->
				<?php 
					if($rowA->type == "t"){?>
	<textarea cols="40" rows="5" name="<?php echo "Q".$rowA->question_num."[".$rowA->num."]"; ?>" tabindex="<?php  echo $rowA->question_num.$rowA->num; ?>"></textarea>
          		<?php
					} else { 
	echo $rowA->answer; 
					}?>
				<?php 
					if($rowA->type == "r"){?>
	<div class="right-layer11 dashed"><!-- right layer end -->
		<div class="rowMultiChoice">
	      	<input type="radio" name="<?php echo "Q".$rowA->question_num; ?>" value="<?php echo $rowA->num; ?>" tabindex="<?php  echo $rowA->question_num.$rowA->num; ?>"/>
		</div><!-- row multi choice end -->
	</div> <!-- end dashed -->                        
	<div class="space-line"></div><!-- space line end -->

		        <?php
					}elseif($rowA->type == "c"){						
	    				$checkBoxFlag = 1; ?>
	<div class="right-layer11 dashed">
		<div class="rowMultiChoice">
		<input type="checkbox" name="<?php echo "Q".$rowA->question_num."[".$rowA->num."]"; ?>" tabindex="<?php  echo $rowA->question_num.$rowA->num; ?>"/>
        </div> <!-- row multi choice end -->          				         		
	</div> <!-- end dashed -->
		      <?php } ?>
	<div class="space-line"></div><!-- space line end -->
</div><!-- table row answer end -->            
				<?php
				} //match question and answer.								 
				 //if total answers completed? what does it do?
                $totalQuestionNumPerAnswer = $rowA->question_num; 
				$totalQuestionNumPerAnswerNumber = $rowA->num;
				?>
					<input type="hidden" name="totalAnswers<?php echo $totalQuestionNumPerAnswer; ?>" value="<?php  echo $totalQuestionNumPerAnswerNumber; ?>" />	
        	    <?php
			} //END ANSWER LOOP  
			$checkBoxFlag = 0;//Prep for next question
			
		} //END QUESTION LOOP ?>   

<div class="space-line"></div>
<div class="table-row-footer"><?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'takeFooterButtons.php');?></div>

	<input type="hidden" value="<?php echo $this->ID; ?>" name="ID" />
	<input type="hidden" value="<?php echo $i; ?>" name="questionTotal" />
	<input type="hidden" value="<?php echo $this->username; ?>" name="username" />
   	<input type="hidden" value="<?php echo $itemid; ?>" name="Itemid" />
</form>
<!-- end of inquisitive take -->