<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Inquisitive Component
 *
 * @package		Joomla.Tutorials
 * @subpackage	Components
 */
 
 //View folder contains view classes and templates. Every view class may have few templates that are stored in tmpl folder. Every view class has the same name view.html.php. And tmpl folder contains html template files. 
 
class InquisitiveViewTake extends JViewLegacy
{
	
	function display($tpl = null)
	{			

		if(JRequest::getVar('ID') != NULL){
			
			$ID = JRequest::getVar('ID');			
			$user = JFactory::getUser();
			$take = $this->getModel('take');	
			$anchorTag = $take->quizTable('anchor_tag');
			
			$this->assignRef('anchorTag', $anchorTag);
			$this->assignRef('username', $user->username);
			$this->assignRef('ID', $ID);
			
			$rawQuestions = JRequest::getVar('rawQuizQuestions');
			$this->assignRef('rawQuestions', $rawQuestions);

			//function to output location URL
/*			$outputURLocator = $this->get( 'OutputURL' );
			$this->assignRef('quizQuestions', $outputURLocator);*/

			//Get question numbers with id. array for view.
			$questionArrayIDnums = JRequest::getVar('questionArrayIDnums');
			$this->assignRef('questionArrayIDnums', $questionArrayIDnums);
			
			$quizAnswers = $this->get( 'QuizAnswers' );
			$this->assignRef('quizAnswers', $quizAnswers);			
			
			$componentName = JRequest::getVar('option');
			$this->assignRef('comName', $componentName);
			
		}
		
		parent::display($tpl);
	}
}
?>
