<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

jimport( 'joomla.application.component.view');
defined('_JEXEC') or die('Restricted access'); 


class InquisitiveViewNewQuiz extends JViewLegacy
{
	
	
	
	function display($tpl = null)
	{

		/*
		 * 
		 *
		 *TODO this is handy to find out permissions status*/  
		jimport('joomla.access.access');
		$user   = JFactory::getUser();
		$result = new JObject;
		if (empty($messageId)) {
			$assetName = 'com_inquisitive';
		}else {
			$assetName = 'com_inquisitive.message.'.(int) $messageId;
		}
		
		$actions = JAccess::getActions('com_inquisitive', 'component');
		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName)); 
		}
			
			// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}
		$quiz_name = JRequest::getVar('quizname');
		$componentName = JRequest::getVar('option');
		$edit_check = JRequest::getVar('editcheck');				
		$ID = JRequest::getVar('ID', time());
		$question_num = JRequest::getVar('question_num', '0');
		if((JRequest::getVar('question') != NULL) or ($question_num == 0)) $question_num++;
		//TODO This anchor tag is broken
		$anchorTag = JRequest::getVar('anchorTag');
		       
		$this->assignRef('anchorTag', $anchorTag);
		$this->assignRef('quizname', $quiz_name);		
		$this->assignRef('comName', $componentName);
		$this->assignRef('ID', $ID);
		$this->assignRef('editcheck', $edit_check);		
		$this->assignRef('question_num', $question_num);
		parent::display($tpl);
	}
}
?>
