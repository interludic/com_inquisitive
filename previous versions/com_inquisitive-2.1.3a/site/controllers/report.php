<?php
/*ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);*/

/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

/**
 * Report Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerReport extends JControllerLegacy
{
	/**
	 * Method to display report to the creator
	 *
	 * @access	private?
	 */
	 
	function display($cachable = false, $urlparams = false){
		//FIXME does the user own the quiz?		
		
		if ((JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) || (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive'))){				
			//$user = JFactory::getUser();
			
			$quizID = JRequest::getVar('quizID');
			//$uid = JRequest::getVar('uid');
			$view = JRequest::getVar('view', 'give');
			$layout = JRequest::getVar('layout', 'default');
			$format = JRequest::getVar('format', 'html');

			$take =$this->getModel('take');
			$anchorTag = $take->quizTable("anchor_tag");
				
			JRequest::setVar ( 'view', 'report' );
			//check for survey or quiz mode!

			$quizlist = $this->getModel('quizlist');
			
			$getQuizInfo = $quizlist->getQuizInfo($quizID);
			/*if($getQuizInfo->survey == 1) JRequest::setVar ( 'layout', 'survey' );
				else JRequest::setVar ( 'layout', 'default' );*/
			
			JRequest::setVar ( 'getQuizInfo', $getQuizInfo );				
			JRequest::setVar ( 'layout', 'default' );				
								
			parent::display();

		}else{		
			//FIXME redirector ?			
			$link = "index.php?option=com_users";
			$this->setRedirect($link);
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
	}
}

?>
