<?php
/*ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);*/

/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

/**
 * New Quiz Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerAjax extends JControllerLegacy
{
	/**
	 * Method to create a new quiz, Insert into quiz_tbl, quizQuestion_tbl and quizAnswer_tbl tables
	 *
	 * @access	public
	 */
	 

	function display($cachable = false, $urlparams = false){
		$document	= JFactory::getDocument();
		$vName		= 'ajax';
		$vFormat	= 'raw';

		// Get and render the view.
		if ($view = $this->getView($vName, $vFormat)) {
			// Get the model for the view.
			$model = $this->getModel($vName);	
			// Push the model into the view (as default).
			$view->setModel($model, true);
			// Push document object into the view.
			$view->assignRef('document', $document);
			$view->display();
		}
	}


	function deletequiz(){	
//security - preventing injection.															
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$document	= JFactory::getDocument();
			$quizID = JRequest::getVar('quizid');
			$model = $this->getModel('quizlist');
			$response = $model->trashThis($quizID);		
			
			$vName		= 'ajax';
			$vFormat	= 'raw';
			if ($view = $this->getView($vName, $vFormat)) {
				$view->setModel($model, true);
				$view->assignRef('response', $response);
				$view->assignRef('document', $document);
				$view->display();
			}
		}
	}

	
	function replicateQuiz(){	
//security - preventing injection.															
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$document	= JFactory::getDocument();
			$quizID = JRequest::getVar('quizid');
			$model = $this->getModel('newquiz');
			$response = $model->replicateQuiz($quizID);		
			
			$vName		= 'ajax';
			$vFormat	= 'raw';
			if ($view = $this->getView($vName, $vFormat)) {
				$view->setModel($model, true);
				$view->assignRef('response', $response);
				$view->assignRef('document', $document);
				$view->display();
			}
		}
	}



}

?>
