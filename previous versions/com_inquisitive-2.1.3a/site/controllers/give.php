<?php
/*ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);*/

/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

/**
 * New Quiz Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerGive extends JControllerLegacy
{
	/**
	 * Method to give results back to users
	 *
	 * @access	public
	 */
	 
	function display($cachable = false, $urlparams = false){
		//FIXME does the user own the quiz?		
		
		if ((JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) || (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive'))){				
			$user = JFactory::getUser();
			
			$id = JRequest::getVar('qid');
			$uid = JRequest::getVar('uid');
			$view = JRequest::getVar('view', 'give');
			$layout = JRequest::getVar('layout', 'default');
			$format = JRequest::getVar('format', 'html');
	
			$give =$this->getModel('give');
			$take =$this->getModel('take');			
			$newquiz =$this->getModel('newquiz');
			$questionNumTotal = $newquiz->lastQuestionNum($id);		
			$quizlist = $this->getModel('quizlist');
			$getQuizInfo = $quizlist->getQuizInfo($id);

			if($getQuizInfo->survey == 1){				
				JRequest::setVar ( 'view', 'give' );
				JRequest::setVar ( 'layout', 'survey' );
				
			}else{			
				$giveQuiz = $give->getProcessQuiz($newquiz, $take, $id, $uid, $questionNumTotal);
				JRequest::setVar ( 'giveQuizString', $giveQuiz );		
				JRequest::setVar ( 'view', 'give' );
				JRequest::setVar ( 'layout', 'default' );
			}
			
			//$view = $this->getView($view, $format);
	
			//$view->setModel($take, true);
			//$view->setLayout($layout);
	
			
				
			parent::display();
		}else{		
			//FIXME redirector ?			
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check.$anchorTag;			
			$this->setRedirect($link);						
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
	}
}

?>
