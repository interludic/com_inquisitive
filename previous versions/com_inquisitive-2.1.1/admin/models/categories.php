<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */



defined('_JEXEC') or die;


jimport( 'joomla.application.component.modellist' );

/**
 * Methods supporting a list of weblink records.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_weblinks
 * @since		1.6
 */


class InquisitiveModelCategories extends JModelList 
{

}
