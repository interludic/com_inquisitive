<?php
/**
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Inquisitive helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_inquisitive
 * @since		2.0.9
 */
class InquisitiveHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	The name of the active view.
	 * @since	2.0.9
	 */
	public static function addSubmenu($vName = 'categories')
	{

		JSubMenuHelper::addEntry(JText::_('COM_INQUISITIVE_SUBMENU_ABOUT'),
			'index.php?option=com_inquisitive',
			$vName == 'about'
		);
		JSubMenuHelper::addEntry(JText::_('COM_INQUISITIVE_SUBMENU_CATEGORIES'),
			'index.php?option=com_categories&view=categories&extension=com_inquisitive', $vName == 'categories');
		$document = JFactory::getDocument();
		if ($vName == 'categories'){ 
			$document->setTitle(JText::_('COM_INQUISITIVE_ADMINISTRATION_CATEGORIES'));
		}
		
		
	}

}
