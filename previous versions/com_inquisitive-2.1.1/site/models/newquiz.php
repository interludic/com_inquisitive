<?php
//print_r($_POST);
/**
 * Inquisitive Model for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );

/**
 * Inquisitive Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
 
 //Models contain Model classes. 
 //One Model class is equal to one DB table.*/
 
class InquisitiveModelNewQuiz extends JModelLegacy
{

	function replicateQuiz($quizID = NULL){
		$db = JFactory::getDBO();				

		$newQuizID = $this->getNewQuizID();

		$query_temp_quiz_info = 'CREATE TEMPORARY TABLE `temp_quiz_info_'.$quizID.'` SELECT * FROM  `#__quiz_info` WHERE `timestamp` = '.$quizID.';';
		$db->setQuery( $query_temp_quiz_info );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_info = 'UPDATE `temp_quiz_info_'.$quizID.'` SET `index` = NULL;';
		$db->setQuery( $query_temp_quiz_info );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_info = 'UPDATE `temp_quiz_info_'.$quizID.'` SET `timestamp` = '.$newQuizID.';';
		$db->setQuery( $query_temp_quiz_info );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_info = 'UPDATE `temp_quiz_info_'.$quizID.'` SET `hidden` = 0;';
		$db->setQuery( $query_temp_quiz_info );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_info = 'UPDATE `temp_quiz_info_'.$quizID.'` SET `result` = NULL;';
		$db->setQuery( $query_temp_quiz_info );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_info = 'UPDATE `temp_quiz_info_'.$quizID.'` SET `replicated` = '.$quizID.';';
		$db->setQuery( $query_temp_quiz_info );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_info = 'INSERT INTO `#__quiz_info` SELECT * FROM `temp_quiz_info_'.$quizID.'`;';
		$db->setQuery( $query_temp_quiz_info );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_info = 'DROP TEMPORARY TABLE IF EXISTS `temp_quiz_info_'.$quizID.'`';
		$db->setQuery( $query_temp_quiz_info );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		

		$query_temp_quiz_answer = 'CREATE TEMPORARY TABLE temp_quiz_answer_'.$quizID.' SELECT * FROM  #__quiz_answer WHERE `timestamp` = '.$quizID.' ORDER BY `question_num` ASC;';
		$db->setQuery( $query_temp_quiz_answer );
		$db->query();			
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_answer = 'UPDATE temp_quiz_answer_'.$quizID.' SET `index` = NULL;';
		$db->setQuery( $query_temp_quiz_answer );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_answer = 'UPDATE temp_quiz_answer_'.$quizID.' SET `timestamp` = '.$newQuizID.';';
		$db->setQuery( $query_temp_quiz_answer );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_answer = 'INSERT INTO #__quiz_answer SELECT * FROM temp_quiz_answer_'.$quizID.';';
		$db->setQuery( $query_temp_quiz_answer );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_answer = 'DROP TEMPORARY TABLE IF EXISTS temp_quiz_answer_'.$quizID.'';
		$db->setQuery( $query_temp_quiz_answer );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	

		$query_temp_quiz_question = 'CREATE TEMPORARY TABLE temp_quiz_question_'.$quizID.' SELECT * FROM #__quiz_question WHERE quizID = '.$quizID.' ORDER BY `num` ASC;';
		$db->setQuery( $query_temp_quiz_question );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_question = 'UPDATE temp_quiz_question_'.$quizID.' SET questionIndex = NULL;';
		$db->setQuery( $query_temp_quiz_question );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_question = 'UPDATE temp_quiz_question_'.$quizID.' SET quizID = '.$newQuizID.';';
		$db->setQuery( $query_temp_quiz_question );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_question = 'INSERT INTO #__quiz_question SELECT * FROM temp_quiz_question_'.$quizID.';';
		$db->setQuery( $query_temp_quiz_question );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_question = 'INSERT INTO #__quizid_questionid (ID, questionNum) SELECT `quizID`, `questionIndex` FROM #__quiz_question WHERE `quizID` ='.$newQuizID.';';
		$db->setQuery( $query_temp_quiz_question );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	
		$query_temp_quiz_question = 'DROP TEMPORARY TABLE IF EXISTS temp_quiz_question_'.$quizID.'';
		$db->setQuery( $query_temp_quiz_question );
		$db->query();		
		if($db->getErrorMsg()) $error = $db->getErrorMsg();	

		$json = array(
	       array
	       (
	            'newquizid' => $newQuizID,
	            'quizid' => $quizID
	       )
		);

		JFactory::getDocument()->setMimeEncoding( 'application/json' );   		
		
		if(isset($error)) return $error;
			else return json_encode($json);

		/* outdated example
		CREATE TEMPORARY TABLE temp_quiz_info SELECT * FROM  #__quiz_info WHERE `timestamp` = 1394586338; 
		UPDATE temp_quiz_info SET `index` = NULL;
		UPDATE temp_quiz_info SET `timestamp` = 1394586341;
		UPDATE temp_quiz_info SET `hidden` = 0;
		UPDATE temp_quiz_info SET `replicated` = 1394586338;
		INSERT INTO #__quiz_info SELECT * FROM temp_quiz_info;
		DROP TEMPORARY TABLE IF EXISTS temp_quiz_info

		CREATE TEMPORARY TABLE temp_quiz_answer SELECT * FROM  #__quiz_answer WHERE `timestamp` = 1394586338 ORDER BY `question_num` ASC;
		UPDATE temp_quiz_answer SET `index` = NULL;
		UPDATE temp_quiz_answer SET `timestamp` = 1394586341;
		INSERT INTO #__quiz_answer SELECT * FROM temp_quiz_answer;
		DROP TEMPORARY TABLE IF EXISTS temp_quiz_answer

		CREATE TEMPORARY TABLE temp_quiz_question SELECT * FROM #__quiz_question WHERE quizID = 1394586338 ORDER BY `num` ASC;
		UPDATE temp_quiz_question SET questionIndex = NULL;
		UPDATE temp_quiz_question SET quizID = 1394586341;
		INSERT INTO #__quiz_question SELECT * FROM temp_quiz_question;
		INSERT INTO #__quizid_questionid (ID, questionNum) SELECT `quizID`, `questionIndex` FROM #__quiz_question WHERE `quizID` =1394586341;
		DROP TEMPORARY TABLE IF EXISTS temp_quiz_question
		*/
	}

	//Check the editability of a quiz
	//return read or write
	function editCheck($ID){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT hidden FROM `#__quiz_info` WHERE timestamp='.$ID;
		$db->setQuery( $queryQ );
		$result = $db->loadResult();
		
		if($db->getErrorMsg()) echo $db->getErrorMsg();	
		
		switch($result){	//get toggle state
			case ("11" || "10"):  //published (read only)					
				$editcheck = "read";
			break;
			case ("0" || NULL):	//Unpublished, newquiz (writeable)
				$editcheck = "write";						
			break;
			}
		return $editcheck;
	}
		
	//get highest question index number
	function getQuestionIndex($insert = NULL){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT MAX(questionIndex) FROM `#__quiz_question` WHERE 1';
		$db->setQuery( $queryQ );
		$result = $db->loadResult();
		
		if($result == NULL) $r = 1;
			else $r = $result+1;	
				
		if($insert == '1'){ //save the question index number
			$queryTemp = "INSERT INTO #__quiz_question (questionIndex) VALUES ('".$r."');";
			$db->setQuery( $queryTemp );
			$db->query();
		}
		return $r;			
	}
	
	//get the last question number from the quiz ID
	function lastQuestionNum($qID = NULL){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT MAX(num) FROM `#__quiz_question` WHERE quizID='.$qID;
		$db->setQuery( $queryQ );
		$result = $db->loadResult();	
				
		return $result;		
	}

	//get highest question number
	function getNextQuestionNum($index){
		$index = $index - 1;
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT MAX(num) FROM `#__quiz_question` WHERE questionIndex = '.$index;
		$db->setQuery( $queryQ );
		
		$result = $db->loadResult();

		if($db->getErrorMsg()){
			return $db->getErrorMsg();
		}else{
			return $result;
		}
	}
	
	function answerType($type){
		
		if($type == 'single') $type = 'r'; //Radio button (single select)
			else $type = 'c'; //Checkbox button (multiple choice)

		return $type;
	}
	
	/**
	 * Retrieve quiz question ID
	 *  return array to display
	 *	$question_num 		= guestion num 
	 *  $id 				= Quiz id
	 */
	function getQuizQuestionId($id, $question_num){ 	
		$db = JFactory::getDBO();	
		$sqlQuestionNum = 'SELECT * FROM `#__quizid_questionid` WHERE ID = "'.$id.'"';
		$db->setQuery( $sqlQuestionNum );
		$sqlQuestionNums = $db->loadObjectList(); //Get Quiz
			
/*		echo "$id , $question_num Question numbers = <pre>";
			print_r($sqlQuestionNums);
		echo "</pre>$sqlQuestionNum <br/>";*/
									
		foreach ( $sqlQuestionNums as $row ) { 	  //Get question data
				$sqlquizQ = 'SELECT questionIndex FROM `#__quiz_question` WHERE num = "'.$question_num.'" AND quizID = "'.$row->ID.'"'; 			
				$db->setQuery( $sqlquizQ );
				$sqlquizQ = $db->loadObject();
			if(isset($sqlquizQ->questionIndex)) return $sqlquizQ->questionIndex;		
		}
		
		if($db->getErrorMsg()) return $db->getErrorMsg();		
			else return false;
	}

	function getNewQuizID($quizID = NULL){
		if(isset($quizID)) $timestamp = $quizID;
			else $timestamp = time();
		
		$db = JFactory::getDBO();
		$query = 'SELECT `timestamp` FROM `#__quiz_info` WHERE `timestamp` = "'.$timestamp.'"';
		$db->setQuery( $query );
		$quizInfo = $db->loadObject();

		if(empty($quizInfo)) return $timestamp;
			else return $quizInfo->timestamp;
	}


	//Check if question num exists
	//Note ID not num
	function questionExistence($ID, $questionID){
		$db = JFactory::getDBO();
		$sqlquizQ = 'SELECT num FROM `#__quiz_question` WHERE questionIndex = "'.$questionID.'" AND quizID = "'.$ID.'"';
		$db->setQuery( $sqlquizQ );
		$sqlquiz = $db->loadObject();

		
//		echo "questionExistence = ".$sqlquizQ." for store method, affected rows = ".$db->getAffectedRows()."<br/>";
		//print_r($sqlquiz);
		
		if(isset($sqlquiz->num)) return true;
			else return false;
	}
	
	function answerExistence($ID, $question_num, $num){
		$db = JFactory::getDBO();
		$sqlquizQ = 'SELECT count(*) FROM `#__quiz_answer` WHERE question_num = "'.$question_num.'" AND timestamp = "'.$ID.'" AND num = "'.$num.'"';
		$db->setQuery( $sqlquizQ );
		$sqlquiz = $db->loadResult();
		
		echo " (answerExistence = $sqlquizQ ) ";
		 print_r($sqlquiz);
		
		if($sqlquiz == "0") return "insert";
			else return "update";
	}

	function answerExistsCheck($question_num, $num){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT type FROM `#__quiz_answer` WHERE question_num = '.$question_num.' AND '.$num;
		$db->setQuery( $queryQ );

		$result = $db->getResult();

		if($db->getErrorMsg())$db->getErrorMsg();
		
		return $result;	
	}
	
	
/*	JError::raiseError(404, JText::_("Resource Not Found"));
	$this->setError($this->_db->getErrorMsg()); */
	function updateQuiz($quiz_name_check, $getQuestionID, $take=NULL){
		echo "POST = <PRE>";
			print_r($_POST);
		echo "</PRE>";
	
		$id = JRequest::getVar('ID');
		$flv = JRequest::getVar('flv');
		$order = JRequest::getVar('order');
		$type = JRequest::getVar('questiontype');
		$location = JRequest::getVar('location');
		$quiz_name = JRequest::getVar('quizname');
		$addAnswer = JRequest::getVar('addAnswer');
		$question_name = JRequest::getVar('question');
		$question_num = JRequest::getVar('question_num');		
		$answerBankAmt = JRequest::getVar('answerBankAmt');	
		$question_index = JRequest::getVar('question_index');		
	
		$status = $this->updateQuizInfo($quiz_name_check, $id, $quiz_name, $take);			
		$quizQuestionId = $this->getQuizQuestionId($id, $question_num);											
		$question_exists = $this->questionExistence($id, $quizQuestionId);					

		//How many answers in db per question.
		$answerSum = $this->answerSum($question_num);	

		//UPDATE VS INSERT 		
		if($question_exists) $store_method = "update";
			else{
				$store_method = "insert";
				//Reserve and return current question index number
				$question_index = $this->getQuestionIndex("1");
			}			
				
		if($question_name != NULL ){
			$status .= $this->updateQuestion($store_method, $id, $question_num, $question_name, $location, $flv, $question_index, $order);

			if(!isset($answerBankAmt)) $answerBankAmt = 1;
			//Update the amount of answers
			for($j=1; $j <= $answerBankAmt; $j++){			 
				$trueC = JRequest::getVar('true'.$j);
				$true = JRequest::getVar('Q'.$question_num); //Radio
				$answer = JRequest::getVar('answer'.$j);	
				$keywords = JRequest::getVar('keywords'.$j);
				$answer_num = JRequest::getVar('answer_num'.$j);						
				$break = false;
				
				//check answer exists!			
				$store_method = $this->answerExistence($id, $question_num, $j); 
				echo "<br/>Answer = $j store_method = <strong>$store_method</strong> <br/>";
				
				switch ($type){
					case "questiontext":
						$type = 't';
					break;
					case ($type == "questionsingle" || $type =='r'):
						$type = 'r';
						if($true == $j) $true = "on";
					break;
					case ($type == "questionmulti" || $type =='c'):
						$type = 'c';
						$true = $trueC;
					break;
					default:						
						$break = true;
						$status .= " |questiontype unknown = ".$type;
					break;
				}
				
				echo "store_method=$store_method , id=$id , j=$j , answer=$answer , keywords=$keywords , addanswer=$addAnswer , true=$true , question_num=$question_num , type=$type <br/>";
												
				if($true != "on") $true = NULL;					
				echo "update answer!";
				$this->updateAnswer($store_method, $id, $j, $answer, $keywords, $addAnswer, $true, $question_num, $type);
	
			 }

			//RECONCILE REMOVE ANSWERS	
			if($answerBankAmt < $answerSum){
				$answer_diff = $answerSum - $answerBankAmt;
				//echo "answer_diff = $answerDiff";
				for($z = 0; $z < $answer_diff; $z++){
					echo "<br/><strong>remove $j</strong><br/>";
					$this->deleteAnswer($id, $j, $question_num);
					$j++;
				}
			}
		}				
		return $status;
	}

	//Update Question table:
	//User is saving a quiz question while editing
	function updateQuestion($store_method, $id, $question_num, $question_name, $location, $flv, $question_index, $order){	
		$db = JFactory::getDBO();						
		$sqlquizQ = "UPDATE #__quiz_question SET num = '".$question_num."', quizID = '".$id."', question = '".$this->cleanString($question_name, $db)."', location = '".$this->cleanString($location, $db)."', flv = '".$flv."' WHERE questionIndex = '".$question_index."';";
		$db->setQuery( $sqlquizQ );
		$db->query();
					
		if($store_method == "insert") $sqlquizQQ = "INSERT INTO #__quizid_questionid (ID, questionNum, listOrder)	VALUES ('".$id."', '".$question_index."', '".$order."');";
			else $sqlquizQQ = "UPDATE #__quizid_questionid SET listOrder = '".$order."' WHERE questionNum = '".$question_index."';";
			
		if(JDEBUG){
			echo "<br/>function updateQuestion - <strong>storemethod = ".$store_method."</strong> <br/> ".$sqlquizQQ."<br/>".$sqlquizQ;
		}
		
		echo "Fired updateQuestion";
			
		$db->setQuery( $sqlquizQQ );
		$db->query();
			
		if($db->getErrorMsg()) return $db->getErrorMsg();
		
		return " |updateQuestion = ".$db->getAffectedRows();//." sql = ".$sqlquizQ;
	}
	
	function updateAnswer($store_method, $id, $num, $answer, $keywords, $addAnswer, $true, $question_num, $type){
		$db = JFactory::getDBO();
						
		if($store_method == "insert") $query =  "INSERT INTO #__quiz_answer (timestamp, num, answer, keywords, abs_answer, correct, question_num, type) VALUES ('".$id."', '".$num."', '".$this->cleanString($answer, $db)."','".$this->cleanString($keywords, $db)."', '".$this->cleanString($addAnswer, $db)."', '".$true."', '".$question_num."' , '".$type."')"; 
			else $query = "UPDATE #__quiz_answer SET answer = '".$this->cleanString($answer, $db)."', keywords = '".$this->cleanString($keywords, $db)."', abs_answer = '".$this->cleanString($addAnswer, $db)."', correct = '".$true."' WHERE question_num = '".$question_num."' AND num = '".$num."' AND timestamp = '".$id."';";
			
		$db->setQuery( $query );
		$db->query();
		echo " (Fired updateAnswer - affected rows = ".$db->getAffectedRows().") ".$query."<br/><br/>";
		
		if($db->getErrorMsg()) JText::_( $db->getErrorMsg());
		
		return $db->getAffectedRows();
	}

	function deleteAnswer($id, $num, $question_index){
		$db = JFactory::getDBO();
		
		
		 $query = 'DELETE FROM #__quiz_answer WHERE timestamp='.$id.' AND num = '.$num.' AND question_num = '.$question_index;	
		 
		//echo "<strong>deleteAnswer</strong> ".$query." <br/> affected rows = ".$db->getAffectedRows()."<br/>";
		$db->setQuery( $query );
		$db->query();
		
		if($db->getErrorMsg()) JText::_( $db->getErrorMsg());
		
		return $db->getAffectedRows();
	}
	
	

	function answerSum($question_num){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT * FROM `#__quiz_answer` WHERE question_num = '.$question_num;
		$db->setQuery( $queryQ );
		
		//return the sum amount of answers stored in db
		$result = $db->loadRowList();
		//echo count($result)." <strong>answerSum</strong> = ".$result."<br/>";
		if($db->getErrorMsg())$db->getErrorMsg();
		
		return count($result);	
	}
	
	function cleanString($subject, $db){

		$search = "$";
		$replace = "\$";
		$subject2 = str_replace($search, $replace, $subject);
		$escaped_subject = $db->getEscaped($subject2); //getescaped is for 2.5 \ escape is for 3
		$quote_subject = $db->quote($escaped_subject);
		
		return $subject;
	}
	
	/**
	 *	getFinishQuiz
	 *
	 *	Publishes current quiz to users quiz list located at component default screen.
	 *
	 *	Retrieves Questions and Answers from temp then updates Question and answer tables completing the quiz. 
	 */

	//cut this out of newquiz publish 21/09
	function getFinishQuiz(){
		$db = JFactory::getDBO();		
		$id = JRequest::getVar('ID');
		$quizName = JRequest::getVar('quizname');		
 		$answerBankAmt = JRequest::getVar('answerBankAmt');
		$status = NULL;
		$r = 0; //array index variable
		
		$quizTempArray[0] = 'sqlquizQ';
		$quizTempArray[1] = 'sqlquizQQ';
		$quizTempArray[2] = 'sqlquizA';
		
		//get the sql query 
		foreach($quizTempArray as $row){ 
		
			$query = 'SELECT '.$quizTempArray[$r].' FROM `#__quiz_temp` WHERE id = "'.$id.'"'; 
				
			$db->setQuery( $query );
			$sqlquiz = $db->loadObjectList();//print_r($sqlquiz);
			/*echo "<pre>";
			print_r($sqlquiz);
			echo "</pre>";			*/
//			echo "sqlquiz = ".$sqlquiz->sqlquizA."<br/><br/>";

			if($db->getErrorMsg()) $status = "<br /> Q1 ".$db->getErrorMsg();//." <pre> sqlquiz = ".$sqlquiz." - ".print_r($sqlquiz)."</pre>";
		
			//process the sql statement	
			foreach ( $sqlquiz as $rows ) {
				//echo "<b>r".$r." sqlquiz = ".$rows->$quizTempArray[$r]."</b><br />";

				if($r == 2){
					//search for pipe (splitup answer bank for sql)
					$chars = explode('|', $rows->$quizTempArray[$r]); //there is something here stopping the answers coming from temp table to answer table.
					for($i=0; $i < $answerBankAmt; $i++){
						//echo $chars[$i]."     i = ".$i." <br />";
						$db->setQuery( $chars[$i] );
						$db->query();				
					}
				}else{				
					//echo "<br /> r".$r." sqlquiz = ".$rows->$quizTempArray[$r]."</b> ";
					$db->setQuery( $rows->$quizTempArray[$r] );
					$db->query();
				}
			}
		
			if($db->getErrorMsg()) $status .= "<br /><b>Q</b>2 ".$db->getErrorMsg();
			$r++;
		}
		
		//copied from inquisitive model, can improve?
		$this->quizAttemptNum($id);								
		
		return $status;				
	}
	
	//insert into quiz_info table	
	//	storemethod updates, or inserts if null.
	function updateQuizInfo($quiz_name_check, $id, $quiz_name, $take){
		$db = JFactory::getDBO();	
		$user = JFactory::getUser();		 
		
		if($quiz_name_check == NULL){
			//if(!(isset($quiz_name))) $quiz_name = $id;
			$infoQuery = "INSERT INTO #__quiz_info (timestamp, quiz_name, teacher_name, attemptMax, hidden) VALUES ('".$id."', '".$this->cleanString($quiz_name, $db)."', '".$user->username."', 'u', '0');";
			//Check if creator wants notifications
			$notify_creator = $take->quizTable("email_creator");
			if($notify_creator == '1'){
				//Notify quiz creator new quiz made
				$subject = JText::_('COM_INQUISITIVE_NOTIFY_CREATOR_SUBJECT');
				$message = JText::_('COM_INQUISITIVE_NOTIFY_CREATOR_MESSAGE');
				$message .= "\n\n";
				$message .= "<a href='";
				$link = JURI::base()."index.php?=".JRequest::getVar('option')."&controller=take&task=sitquiz&ID=".$id;
				$message .= $link;
				$message .= "'>".$link."</a>";
				$email = $user->email; //Recipient email
							
				//echo "Notify creator ? = ( ".$notify_creator." ) <br />";									
				echo $take->mailOutResponse($subject, $message, $email, $additionalEmail=NULL);
			}
			
		}else{
			$infoQuery = "UPDATE #__quiz_info SET quiz_name = '".$this->cleanString($quiz_name, $db)."' WHERE timestamp = '".$id."';";			
		} 
			
		//echo "infoQuery ".$infoQuery;
		$db->setQuery( $infoQuery );	
		if($id != NULL)	$db->query();
		
		if($db->getErrorMsg()) return " error ".$db->getErrorMsg()."<br />";
		
		
		return " updateQuizInfo = ".$db->getAffectedRows();
	}
	
	//DELETE THIS ITS DUPED
	function quizAttemptNum($quizID){ //copied from model inquisitive (can i access that one instead? how?)
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		$query = 'SELECT attemptNum FROM `#__quiz_attempt` WHERE quizID = "'.$quizID.'" and userID = "'.$user->username.'"';		
		$db->setQuery( $query );
		$db->query();
		$attemptNum = $db->loadRow();
		
		/*-echo "attempt - <pre>";
		print_r($attemptNum);
		echo "</pre>";//.$attemptNum;*/
		
		//row does not exist. Create (assumed new user)
		if($attemptNum['0'] == NULL) {			
			$queryAttempt = "INSERT INTO #__quiz_attempt (quizID, userID, attemptNum) VALUES ('".$quizID."', '".$user->username."', '0');";
			$db->setQuery( $queryAttempt );
			$db->query();
			
			return 1;
		}
		
		if($attemptNum['0'] == 0) return NULL;
			else return $attemptNum['0'];		
	}
}