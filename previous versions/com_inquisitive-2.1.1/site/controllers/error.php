<?php
/*ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);*/

/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

/**
 * New Quiz Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerError extends JControllerLegacy
{
	/**
	 * Method to create a new quiz, Insert into quiz_tbl, quizQuestion_tbl and quizAnswer_tbl tables
	 *
	 * @access	public
	 */
	 
	function display(){
		
		$errorNum = JRequest::getVar('num');

		$model = &$this->getModel('error');
		$errorMsg = $model->error($errorNum);
		
		JRequest::setVar( 'msg', $errorMsg );		
		JRequest::setVar( 'view', 'error' );
		JRequest::setVar( 'layout', 'default' );
	
		parent::display();
	}
	
}

?>
