<?php

/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

/**
 * New Quiz Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerQuizList extends JControllerLegacy
{
	/**
	 * Method to create a new quiz, Insert into quiz_tbl, quizQuestion_tbl and quizAnswer_tbl tables
	 *
	 * @access	public
	 */

	function display($cachable = false, $urlparams = false){
			
		//Check authority	
		if ($this->accessControl() == "write"){
			//Toggle the publish status. Update the quiz_info table hidden value to publish or unpublished.
			$quizlist = $this->getModel('quizlist'); //TODO return what is permissible		
			$toggle = $quizlist->toggle_quiz_info();
			JRequest::setVar( "usertype",  "Publisher");
		}else{
			JRequest::setVar( "usertype",  "Guest");
		}
			
		if (($this->accessControl() == "write") || ($this->accessControl() == "read")){
			JRequest::setVar( 'view', 'quizlist' );
			JRequest::setVar( 'layout', 'default' );	
			parent::display();
		
		}else{
			//XXX this link needs to be redirected properly after login.
			//FIXME  ^					
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check.$anchorTag;			
			$this->setRedirect($link);						
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
	}

	function getbin(){
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$ID = JRequest::getVar('ID');
					
			$model = $this->getModel('quizlist');
			$model->trashThis($ID);
			
			//$anchorTag = $model->quizTable("anchor_tag");
			$link = "index.php?option=".JRequest::getVar('option')."&view=quizlist";//.$anchorTag;
				
			$msg = JText::_( 'Quiz Deleted');
			$this->setRedirect($link, $msg);
			JRequest::setVar( 'view', 'quizlist' );
			JRequest::setVar( 'layout', 'default' );
	
			parent::display ();			
		}else{
			//XXX this link needs to be redirected properly after login.
			//FIXME  ^					
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check.$anchorTag;			
			$this->setRedirect($link);						
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}

	}
	
	function updateQuizInfo(){
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$quizID = JRequest::getVar('quizID');
				
			$model = $this->getModel('quizlist');
			//send model the post data
			$model->updateQuizInfo($_POST);
				
			//$anchorTag = $model->quizTable("anchor_tag");
			$link = "index.php?option=".JRequest::getVar('option')."&controller=quizlist";

//			echo "route: ".JRoute::_($link);			
	
			$msg = JText::_( 'Quiz updated');
			$this->setRedirect($link, $msg);
			JRequest::setVar( 'view', 'quizlist' );
			JRequest::setVar( 'layout', 'default' );
	
			parent::display ();
		}else{
			//XXX this link needs to be redirected properly after login.
			//FIXME  ^
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check.$anchorTag;
			$this->setRedirect($link);
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
	
	}
	
	

	//Return read / write / (super admin)
	function accessControl(){
		//5 = Publisher in 2.5 // 8 = super admin
		$groupsUserIsIn = JAccess::getGroupsByUser(JFactory::getUser()->id);
		if(in_array(8,$groupsUserIsIn)) return "write"; 
				
		if(JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')) return "write"; 
		
		if(JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) return "read";
	}
}

?>
