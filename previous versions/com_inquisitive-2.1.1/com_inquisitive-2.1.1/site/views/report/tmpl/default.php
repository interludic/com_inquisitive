<?php 
	defined('_JEXEC') or die('Restricted access');
	$reportQuestion = $this->reportQuestion;
	$reportQuiz = $this->reportQuiz;

	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$flotr2Css = "components/".$this->comName."/views/tmpl/flotr2.css";
	//http://labs.mintchaos.com/flippant.js/
	$flippantCss = "components/".$this->comName."/views/tmpl/flippant.css";
	$flippantJs = "components/".$this->comName."/views/tmpl/js/flippant.min.js";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);
	$document->addStyleSheet($flotr2Css);
	$document->addStyleSheet($flippantCss);
	$document->addScript($flippantJs);

	$i=1;
	$j=0;
	$questionsTick = NULL;
	$questionsData = NULL;
	$sumQuestionAverage = 0;
	$dataSet = NULL;
	foreach($this->sumQuestionArray as $questionAverage){			
		//echo data for javascript			
		$dataSet .= "d".$i." = [[".$questionAverage.", ".$i."],], ";			
		$questionsTick .= $i.", ";
		$questionsData .= "d".$i.", ";
		$sumQuestionAverage += $questionAverage;

		$i++;
		$j++;
	} // end for loop $questionAverage
	$totalQuizAverage = round($sumQuestionAverage / $j); 
	/*	d1 = [     
	[56, 1],
	],
	d2 = [			
		[42, 2],
	],
	d3 = [
		[61, 3],
	],
	d4 = [
		[56, 4],
	],
	d5 = [
		[50, 5],
	],
	d6 = [
		[20, 6],
	],*/
?>
<hr /><br/>
<div class="row">
<div class="span4">
	<h2 class="quizGive"><?php echo $totalQuizAverage; ?>%</h2>
    <p class="text-center">Average User Score</p>
</div>
<div class="span4">
	<h2 class="quizGive"><?php echo $this->countQuizUsers; ?></h2>
    <p class="text-center">Users Submitted</p>
</div>
<div class="span4">
   <p class="text-center"><?php echo $this->getQuizInfo->quiz_name."<br/> Creator: ".$this->getQuizInfo->teacher_name; ?></p>
    <?php //[index] => 9 [timestamp] => 1350709824 [attemptMax] => u [quiz_name] => How much do I know about Jo [result] => 132 [teacher_name] => admin [hidden] => 11 [incomplete] => [survey] => 0 ?>
   <p class="text-center"><a class="btnflip btn card" href="#">Show Users &raquo;</a></p>
</div>
</div>
<hr />
<div id="overall"></div>
<hr />
<div id="questionAverages"></div>


 <!--[if IE]>
    <script type="text/javascript" src="components/<?php echo $this->comName; ?>/views/tmpl/js/flashcanvas.js"></script>
 <![endif]-->
<script type="text/javascript" src="components/<?php echo $this->comName; ?>/views/tmpl/js/flotr2.min.js"></script>
<?php

/*	echo "<pre>";
	 //print_r($this->reportUsersVersusQuiz);
	//print_r($this->sumQuestionArray);
//	print_r($this->getReportQuiz);
	echo "</pre>";*/

/*	echo "<pre>";
	 print_r($reportQuiz);
	echo "</pre>";*/

?>


<script type="text/javascript">
;(function(e,t,n){function i(n,s){if(!t[n]){if(!e[n]){var o=typeof require=="function"&&require;if(!s&&o)return o(n,!0);if(r)return r(n,!0);throw new Error("Cannot find module '"+n+"'")}var u=t[n]={exports:{}};e[n][0].call(u.exports,function(t){var r=e[n][1][t];return i(r?r:t)},u,u.exports)}return t[n].exports}var r=typeof require=="function"&&require;for(var s=0;s<n.length;s++)i(n[s]);return i})({1:[function(require,module,exports){
// index.js

var flip = flippant.flip
var event = require('./event')

document.addEventListener('click', function(e) {
  if (e.target.classList.contains('btnflip')) {
    e.preventDefault()
    var flipper = e.target.parentNode.parentNode
    var back
    var back_button = "<button class='btn'>&laquo; Back <\/button>";
	var back_content = "<?php echo $this->quizUsers; ?>"+back_button;
	
    if (e.target.classList.contains('card')) {
      back = flip(flipper, back_content)
    } else {
      back = flip(flipper, back_content, 'modal')
    }

    back.addEventListener('click', function(e) {
      if (e.target.classList.contains('btn')) {
        event.trigger(back, 'close')
      }
    })

  }
})


},{"./event":2}],2:[function(require,module,exports){
exports.trigger = function(elm, event_name, data) {
  var evt = new CustomEvent(event_name, data)
  elm.dispatchEvent(evt)
}
},{}]},{},[1])
;




(function basic_bars(questionAverages, horizontal) {
	
    var
    horizontal = true, //(horizontal ? true : false),
        // Show horizontal bars
		 <?php	echo $dataSet; ?>			

		 questions = [
		 <?php	echo $questionsTick; //1, 2, 3, 4, 5, 6 ?>			
        ],
        // Ticks for the Y-Axis
        point, // Data point variable declaration
        i;
		
	function ticksFn(n) {
        return n + '%';
    }

    // Draw the graph
    Flotr.draw(
    questionAverages, [<?php echo $questionsData; //d1, d2, d3, d4, d5, d6 ?>], {
        bars: {
            show: true,
            horizontal: horizontal,
            shadowSize: 1,
            barWidth: 0.5
        },
        mouse: {
            track: true,
            relative: true,
			trackDecimals: 0,
			trackFormatter: function(obj){ return obj.x +'% Average'; } // => formats the values in the value box
        },
        yaxis: {			
			ticks: questions,
		   	title: 'Questions',
            // Set Y-Axis ticks
			//max: <?php echo $j; ?>,
			min: 1
        },
		xaxis:{
			min: 1,
			max: 100,
	    	tickFormatter: ticksFn
			
		},
		HtmlText: false,
		  grid: {
            horizontalLines: false,
            backgroundColor: 'white'
        },
		 title: 'Average per Question',
        subtitle: 'All users (most recent attempt only)'
    });
})(questionAverages);




(function basic_pie(overall) {

    var
   <?php echo $this->gradingUsersTotalScore; ?>
        graph;

    graph = Flotr.draw(overall, [{
        data: d1,
        label: '0 to 49',
        pie: {
            explode: 20
        }

    }, {
        data: d2,
        label: '50 to 65'
    }, {
        data: d3,
        label: '66 to 75'
    }, {
        data: d4,
        label: '76 to 85'
    }, {
        data: d5,
        label: '86 to 100'
    }], {
        HtmlText: false,
        grid: {
            verticalLines: false,
            horizontalLines: false
        },
        xaxis: {
            showLabels: false
        },
        yaxis: {
            showLabels: false
        },
        pie: {
            show: true,
            explode: 5
        },
        mouse: {
            track: true,
		    relative: true,
			trackDecimals: 0,
			trackFormatter: function(obj){ return obj.y +' Users <br/>' + obj.x; } // => formats the values in the value box		
        },
        legend: {
			title: 'Grades',
            position: 'se',
            backgroundColor: '#D2E8FF'
        },
	 	title: 'Group Results',
        subtitle: 'All users (most recent attempt only)'
    });
})(overall);




/*(function basic_bubble(overall) {
	var
	d1 = [     
		[56, 56, 5.5],
		],
		d2 = [			
			[42, 42, 10],
		],
		d3 = [
			[61, 61, 1.5],
		],
		d4 = [
			[56, 56, 6],
		],
		d5 = [
			[50, 50, 1],
		],
		d6 = [
			[20, 20, 4.6],
		],
		point, graph, i;
	

	// Draw the graph
	graph = Flotr.draw(overall, [d1, d2, d3, d4, d5, d6], {
		bubbles: {
			show: true,
			baseRadius: 5
		},
		xaxis: {
			min: 0,
			max: 100
		},
		yaxis: {
			min: 0,
			max: 100
		}
	});
})(overall);*/
</script>

<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>
<!-- end of inquisitive report -->