<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */
 
jimport( 'joomla.application.component.view');
defined('_JEXEC') or die('Restricted access'); 

/**
 * HTML View class for the Inquisitive Component
 *
 * @package		Joomla.Tutorials
 * @subpackage	Components
 */
 
 //View folder contains view classes and templates. Every view class may have few templates that are stored in tmpl folder. Every view class has the same name view.html.php. And tmpl folder contains html template files. 
 
class InquisitiveViewQuestionMulti extends JViewLegacy
{
	
	function display($tpl = null)
	{
		$ID = JRequest::getVar('ID', time());		
		$quiz_name = JRequest::getVar('quizname');
		$questionNum = JRequest::getVar('question_num', 1);
		$qn = ($questionNum - 1);
		$componentName = JRequest::getVar('option');		
		$question = JRequest::getVar('question', NULL);
		$questionArray = JRequest::getVar('questionArray', NULL);	//???	
		$answerArray = JRequest::getVar('answerArray', NULL);
		$answerArrayCount = count($answerArray);
		$edit_check = JRequest::getVar('editcheck', NULL);
		$question_index = JRequest::getVar('question_index', NULL);	
		$last_question = JRequest::getVar('last_question', NULL);
		$view = JRequest::getVar('view');
		$anchorTag = JRequest::getVar('anchorTag');

/*		echo "<pre> Question ";
			print_r($question);
			echo "answer";
			print_r($answerArray);				
		echo "</pre>";		
			
		if(isset($edit_check) && ($answerArray != NULL)){
			foreach($answerArray as $row){				
				$keywords = $row->keywords;					
					echo "Answer - <pre>";				
						print_r($row);
					echo "</pre>";
			}
		}*/
		//TODO Anchor broken around here..
		$this->assignRef('anchorTag', $anchorTag);
		$this->assignRef('answercount', $answerArrayCount);			
		$this->assignRef('last_question', $last_question);
		$this->assignRef('view', $view);
		$this->assignRef('editcheck', $edit_check);
		$this->assignRef('question_num', $questionNum);
		$this->assignRef('question_array', $questionArray);
		$this->assignRef('answer_array', $answerArray);		
		$this->assignRef('question_index', $question_index);				
		$this->assignRef('question', $question);
		$this->assignRef('quizname', $quiz_name);	
		$this->assignRef('comName', $componentName);
		$this->assignRef('ID', $ID);	

		$this->assignRef( 'header', 	$header);//???
		//$this->assignRef('debug', $error = print_r(JRequest::get( 'post' )));

		
/* OLD(pre edit)	$ID = JRequest::getVar('ID', time());
		$question_num = JRequest::getVar('question_num', '0');
		$header= "New Quiz</ br>";	
				$editCheck = JRequest::getVar('editcheck');
		$this->assignRef('editcheck', $editCheck);		


		$componentName = JRequest::getVar('option');
		$quizName = JRequest::getVar('quizname');
		$this->assignRef('quizname', $quizName);		

		$this->assignRef('comName', $componentName);
		$this->assignRef('ID', $ID);
		$this->assignRef('question_num', $question_num);
		$this->assignRef( 'header', 	$header);		
		//$this->assignRef('debug', $error = print_r(JRequest::get( 'post' )));
*/
//		echo $db->getErrorMsg();

		parent::display($tpl);
	}
}
?>
