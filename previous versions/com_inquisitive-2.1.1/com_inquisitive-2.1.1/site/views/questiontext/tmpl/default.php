<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 

$user = JFactory::getUser();
if(!$user->guest) { 
	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);
?>
<form name="newQuestion" method="post" onsubmit="return validate_form(this)">  
<div class="base-container base-layer">
 	<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'questionHeader.php'); ?>
	<div class="table-row-answer">
		<div class="headingLabel">
        	<!-- <p class="numText" ID="answerNum" style="display:none;">1</p> <br/>-->
            <a class="tooltip-inquisitive" href="#">Keywords:<span class="custom info"><em>Keywords</em><br />Returns a percentage based on the number of keywords found correct from the users response.</span></a                   	    
        ></div>
		<div class="text-box-inquisitive">
	            <input  name="keywords1" id="keywords1" type="text" size="52" <?php if($this->editcheck == "read") echo "disabled=true"; ?> value="<?php echo $this->keywords; ?>" onBlur="keywordsCheck(this, 1)" onFocus="keywordsCheck(this, 1)" /> 
        </div>             
	
   		<div class="space-line"></div>
	</div>
	</div></div>
    <div class="space-line"></div>
</div>

	<input type="hidden" id="question_num" value="<?php echo $this->question_num; ?>" name="question_num" />   
    <input type="hidden" id="question_index" value="<?php if(isset($this->question_index)) echo $this->question_index; ?>" name="question_index" />
   	<input type="hidden" id="answerBankAmt" value="1" name="answerBankAmt" />
	<?php include(JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'questionFooterButtons.php'); ?>
</div>
<?php include(JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>

</form>
<?php
	}else{		
		echo "Your session has <b>expired</b>,<br />Please <i>log in</i> to view this resource.";
	}//User Access
	?>
    
    
