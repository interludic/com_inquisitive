<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */
 
jimport( 'joomla.application.component.view');
defined('_JEXEC') or die('Restricted access'); 

 
class InquisitiveViewQuestionText extends JViewLegacy
{	
	function display($tpl = null)
	{		
		$keywords = NULL;
		$ID = JRequest::getVar('ID', time());		
		$quiz_name = JRequest::getVar('quizname');
		$questionNum = JRequest::getVar('question_num', 1);
		$qn = ($questionNum - 1);
		$componentName = JRequest::getVar('option');		
		$question = JRequest::getVar('question', NULL);
		$questionArray = JRequest::getVar('questionArray', NULL);		
		$answerArray = JRequest::getVar('answerArray', NULL);
		$edit_check = JRequest::getVar('editcheck', NULL);
		$question_index = JRequest::getVar('question_index', NULL);	
		$last_question = JRequest::getVar('last_question', NULL);
		$view = JRequest::getVar('view');
		
		if(isset($edit_check) && isset($answerArray)){
			foreach($answerArray as $row){				
				$keywords = $row->keywords;	
			}
		}
		if($keywords == NULL) $keywords = 'Seperate keywords with commas.';			 		

		$this->assignRef('last_question', $last_question);
		$this->assignRef('view', $view);
		$this->assignRef('keywords', $keywords);
		$this->assignRef('editcheck', $edit_check);
		if(isset($question->question)) $this->assignRef('question_label', $question->question);
		$this->assignRef('question_num', $questionNum);
		$this->assignRef('question_array', $questionArray);
		$this->assignRef('question_index', $question_index);				
		$this->assignRef('question', $question);
		$this->assignRef('quizname', $quiz_name);	
		$this->assignRef('comName', $componentName);
		$this->assignRef('ID', $ID);

		parent::display($tpl);
	}
}
?>
