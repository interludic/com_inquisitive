<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 

$user = JFactory::getUser();
if(!$user->guest) { 	
	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);
?>
<form name="newQuestion" method="post" onsubmit="return validate_form(this)">
<div class="base-container base-layer">

 	<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'questionHeader.php'); ?>

	<div class="table-row-answer">
		<div class="headingLabel">
	        Answer:
        	<p class="numText" ID="answerNum" style="display:none;">1</p>
        </div>
		<div class="left-layer11">
        	<textarea name="answer1" id="answer1" cols="40" rows="4" <?php if($this->editcheck == "read") echo "disabled=true"; ?>><?php if(isset($this->answer_array[0])) echo $this->answer_array[0]->answer; ?></textarea>
        </div>             

		<div class="right-layer11">
			<div class="rowMultiChoice"><?php echo JText::_('COM_INQUISITIVE_CREATE_ANSWER_CORRECT'); ?></div>
		   	<div class="space-line"></div>
			<div class="rowMultiChoice"><input <?php if($this->editcheck == "read") echo "disabled=true"; ?> type="radio" name="Q<?php echo $this->question_num; ?>" value="1" id="correct" <?php if(isset($this->answer_array[0]->correct)) if($this->answer_array[0]->correct != NULL) echo 'checked=checked'; ?> />	<?php //if(isset($this->answer_array[0]->correct)) if($this->answer_array[0]->correct != NULL) echo "correct=".$this->answer_array[0]->correct; ?>
    	    </div>    
			<div class="space-line"></div>
		</div>
		<div class="space-line"></div>
	</div>
	<div id="content-inquisitive"></div>
    <div class="space-line"></div>
</div>
	<input type="hidden" id="answerBankAmt" value="1" name="answerBankAmt" />
    <input type="hidden" id="count" name="skill_count" value="2" />
	<?php include(JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'questionFooterButtons.php'); ?>
</div>
<?php include (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>

</form>
<?php
	}else{		
		echo "Your session has <b>expired</b>,<br />Please <i>log in</i> to view this resource.";
	}//User Access
?>