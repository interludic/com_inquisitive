<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
 
jimport( 'joomla.application.component.view');
defined('_JEXEC') or die('Restricted access'); 

 
class InquisitiveViewQuestionMulti extends JViewLegacy
{
	
	function display($tpl = null)
	{
		$ID = JRequest::getVar('ID', time());		
		$quiz_name = JRequest::getVar('quizname');
		$questionNum = JRequest::getVar('question_num', 1);
		$qn = ($questionNum - 1);
		$componentName = JRequest::getVar('option');		
		$question = JRequest::getVar('question', NULL);
		$questionArray = JRequest::getVar('questionArray', NULL);	//???	
		$answerArray = JRequest::getVar('answerArray', NULL);
		$answerArrayCount = count($answerArray);
		$edit_check = JRequest::getVar('editcheck', NULL);
		$question_index = JRequest::getVar('question_index', NULL);	
		$last_question = JRequest::getVar('last_question', NULL);
		$view = JRequest::getVar('view');
			
		
		$this->assignRef('answercount', $answerArrayCount);			
		$this->assignRef('last_question', $last_question);
		$this->assignRef('view', $view);
		$this->assignRef('editcheck', $edit_check);
		$this->assignRef('question_num', $questionNum);
		$this->assignRef('question_array', $questionArray);
		$this->assignRef('answer_array', $answerArray);		
		$this->assignRef('question_index', $question_index);				
		$this->assignRef('question', $question);
		$this->assignRef('quizname', $quiz_name);	
		$this->assignRef('comName', $componentName);
		$this->assignRef('ID', $ID);			

		parent::display($tpl);
	}
}
?>
