<?php // no direct access
/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

	defined('_JEXEC') or die('Restricted access'); 
	$user = JFactory::getUser();

	if(!$user->guest) {		

	}else{		
		echo JText::_('COM_INQUISITIVE_SESSION_EXPIRED_ERROR');
	}?> 