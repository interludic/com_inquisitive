<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

jimport( 'joomla.application.component.view');
defined('_JEXEC') or die('Restricted access'); 

/**
 * HTML View class for the Inquisitive Component
 *
 * @package		Inquisitive
 * @subpackage	Components
 */
 
 //View folder contains view classes and templates. Every view class may have few templates that are stored in tmpl folder. Every view class has the same name view.html.php. And tmpl folder contains html template files. 
 
class InquisitiveViewQuizList extends JViewLegacy
{
	
	function display($tpl = null)
	{													
	
		if ($this->accessControl() == "write"){
			$usertype = "Publisher";//JRequest::setVar( "usertype",  "Publisher");
		}else{
			$usertype = "Guest"; //JRequest::setVar( "usertype",  "Guest");
		}
						
		$componentName = JRequest::getVar('option');
		$filterCategory = JRequest::getVar('filtercategory');
		
		$quizList = $this->getModel('quizlist');
		$getQuizList = $quizList->quizList(NULL, $filterCategory);
		$categoriesList = $quizList->getCategories();
		$categoriesList = array_filter($categoriesList);

		// if (empty($categoriesList)) {
		// 	echo "empty!";
		// }
		// 		var_dump($categoriesList);
		
		//super user
		$groupsUserIsIn = JAccess::getGroupsByUser(JFactory::getUser()->id);
		if(in_array(8,$groupsUserIsIn)) $superUser = true;
				
		//DEBUG //echo "[".$componentName." quizList ] usertype = ".$usertype." group = ";
		//print_r($groupsUserIsIn);
		//print_r($getQuizList);
		$this->assignRef( 'filterCategory', $filterCategory);
		$this->assignRef( 'categoriesList', $categoriesList);		
		$this->assignRef( 'superUser',	$superUser );				
		$this->assignRef( 'newQuiz',	$newQuiz );		
		$this->assignRef( 'comName',	$componentName);
		$this->assignRef( 'quizArray',	$getQuizList );
		$this->assignRef( 'usertype',	$usertype );								

		parent::display($tpl);
	}
	
	function accessControl(){
		//5 = Publisher in 2.5 // 8 = super admin
		$groupsUserIsIn = JAccess::getGroupsByUser(JFactory::getUser()->id);
		if(in_array(8,$groupsUserIsIn)) return "write"; 
				
		if(JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')) return "write"; 
		
		if(JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) return "read";
	}
}
?>
