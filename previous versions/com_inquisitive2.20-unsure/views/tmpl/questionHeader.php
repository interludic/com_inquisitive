<?php 
	//Check for jquery
	require_once JPATH_COMPONENT . '/views/tmpl/jQuery.php';
		
	//Setup the answer box type
	switch(JRequest::getVar('view')){
		case "questionsingle" :
			$aSwitch = 'r';
		break;
		case "questionmulti" :
			$aSwitch = 'c';
		break;
		case "questiontext" :
			$aSwitch = 't';
		break;
		default :
			$aSwitch = 'questionType';
	}
					
?>
<input type="hidden" id="editcheck" value="<?php if(isset($this->editcheck)) echo $this->editcheck; ?>" name="editcheck" />
<input type="hidden" id="questiontype" value="<?php echo JRequest::getVar('view'); ?>" name="questiontype" />
<input type="hidden" value="<?php echo $this->ID; ?>" name="ID" />
<input type="hidden" id="question_num" value="<?php echo $this->question_num; ?>" name="question_num" />   
<input type="hidden" id="questionIndex" value="<?php echo $this->questionIndex; ?>" name="questionIndex" />   


<div class="table-row-header" id="questionElements">
	<div class="questionHeaderButton" >
		<?php echo JText::_('COM_INQUISITIVE_CREATE_QUIZ_NAME'); ?><input id="quizname" type="text" name="quizname" value="<?php if(isset($this->quizname)) echo htmlspecialchars($this->quizname, ENT_QUOTES, "UTF-8"); ?>" <?php if($this->editcheck == "read") echo "disabled=true"; ?> />
    </div>
</div>

<div class="table-row-question">
    <div class="headingLabel">
        <?php echo JText::_('COM_INQUISITIVE_CREATE_QUESTION_NAME'); ?>
        <div id="questionNum" class="numText"><?php echo $this->question_num; ?></div>
    </div>
    <div class="text-question">
        <?php //require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'questionAddUrl.php'); ?> 
        <textarea name="question" id="question" <?php if($this->editcheck == "read") echo "disabled=true"; ?>><?php if(isset($this->question->question)) echo $this->question->question; ?></textarea>
        </div>

    <!-- Trash Question -->
    <div class="right-cell-small">	            					
	   <span class="editlinktip hasTip" title="<?php echo JText::_( "Trash question" );?>">
	   <a href="<?php echo $_SERVER["REQUEST_URI"] ?>#">                	
	    	<div onclick="trashquestion('<?php echo $this->question_index; ?>'); return false">
	    		<?php echo $this->question_index; ?>
	   		    <img src="components/com_inquisitive/views/tmpl/images/trash.png" width="16" height="16" alt="Trash question" />
	        </div>
	   </a>
	   </span>
	</div>

    <div class="space-line"></div> 
</div>
