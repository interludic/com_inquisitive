<?php
/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

class InquisitiveControllerError extends JControllerLegacy
{
	 
	function display(){
		
		$errorNum = JRequest::getVar('num');

		$model = &$this->getModel('error');
		$errorMsg = $model->error($errorNum);
		
		JRequest::setVar( 'msg', $errorMsg );		
		JRequest::setVar( 'view', 'error' );
		JRequest::setVar( 'layout', 'default' );
	
		parent::display();
	}
	
}

?>
