<?php
/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

class InquisitiveControllerTake extends JControllerLegacy{
	 
	function display($cachable = false, $urlparams = false){
				
		parent::display();
	}
	
	function sitQuiz(){
		if ((JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) || (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive'))){
			JRequest::setVar ( 'view', 'take' );		
			JRequest::setVar ( 'layout', 'default' );
			$model = $this->getModel('take');		
			$getQuizQuestions = $model->getQuizQuestions(NULL, NULL, NULL);
		/*	$questionArrayIDnums = $model->questionArrayIDnums(NULL);

			JRequest::setVar ( 'questionArrayIDnums', $questionArrayIDnums );*/
			//print_r($getQuizQuestions);
			JRequest::setVar ( 'rawQuizQuestions', $getQuizQuestions );
			//TODO consider ACL in the view		
			JRequest::setVar ( 'usertype', 'Guest');			
			parent::display();	
		
		}else{
			//FIXME this link needs to be redirected properly after login. 
			//FIXME XXX Consider adding to control panel					
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;			
			$this->setRedirect($link);						
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
		
	}

	function publish() {
		if ((JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) || (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive'))){		
			JRequest::setVar ( 'view', 'results' );		
			JRequest::setVar ( 'layout', 'default' );
			$model = $this->getModel('take');
			$give = $this->getModel('give');		
			
			//delete previous quiz attempt...
			$model->deleteQuizAttempt($_POST['username'], $_POST['ID']);
	
			//insert & format the quiz results
			$quizResultFormatted = $model->quizResult();			
			
			//Email or..
			$publishStatus = $model->publish($_POST['username'], $_POST['ID'], $quizResultFormatted, $give);
			
			$link = 'index.php?option=com_inquisitive&controller=give&uid='.$_POST['username'].'&qid='.$_POST['ID'].'&Itemid='.$_POST['Itemid'];
			//processing the quiz checking results.. comment redirect to see results
/* DEBUG */	$this->setRedirect($link);
			echo "<br/>publish Status:<br/> ".$publishStatus."";
			// <pre><br/>Post = ";
			//print_r($_POST);
			/*echo "<br/>QuizResults = ";
			//print_r($quizResultFormatted);
			echo "</pre>";*/
			//echo "<a href=".$link."> Proceed </a>";
			//parent::display();
		}else{
			//FIXME this link needs to be redirected properly after login. 
			//FIXME XXX Consider adding to control panel					
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;			
			$this->setRedirect($link);						
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;				
		}
	}
}

?>
