<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

		
/**
 * Inquisitive Model for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport( 'joomla.application.component.model' );

 
class InquisitiveModelReport extends JModelLegacy{	

	/**
	 * Retrieve student results from report table
	 *   return object for display
	 *   userID can be NULL to return more
	 */
	function getReportQuiz($quizID, $userID = NULL){
		if($userID != NULL) $userAttribute = ' AND user_id= "'.$userID.'"';
			else $userAttribute = NULL;

		$sql =  'SELECT * FROM `#__quiz_report_quiz` WHERE quiz_id = "'.$quizID.'" '.$userAttribute.' ORDER BY `index`';
		$db = JFactory::getDBO();
		$db->setQuery( $sql );
		$report = $db->loadObjectList();
		
		/*echo "getStudentResults - <PRE>";
			print_r($sqlquiz);
		echo "</PRE>$sqlquizR";*/

		if($db->getErrorMsg()){
			$status = "<br />".$db->getErrorMsg();
			return $status;
		}
		
		return $report;
	}											



	/**
	 * Retrieve student results from report table
	 *   return object for display
	 *   userID must exist
 	 *   questionID can be NULL to return more
	 */
	function getReportQuestion($quizID, $userID, $questionID = NULL){
		$userAttribute = NULL;
		if($questionID != NULL) $userAttribute .= ' AND question_id= "'.$questionID.'"';
			elseif($userID != NULL) $userAttribute .= ' AND user_id= "'.$userID.'"';
		
		$sql =  'SELECT * FROM `#__quiz_report_question` WHERE quiz_id = "'.$quizID.'" '.$userAttribute.' ORDER BY `index`';	
		$db = JFactory::getDBO();
		$db->setQuery( $sql );
		$report = $db->loadObjectList();
		
		if($db->getErrorMsg()){
			$status = "<br />".$db->getErrorMsg();
			return $status;
		}
		
		return $report;
	}					
	
	/**
	 * Retrieve overall results for scores per question and user from report question table
	 *   return object for display
	 *   quizID must exist
	 */
	function getReportUsersVersusQuiz($quizID){
		$sql =  'SELECT `user_score` , `question_id` , `user_id` FROM `#__quiz_report_question` WHERE quiz_id = "'.$quizID.'" ORDER BY `index`';	
		$db = JFactory::getDBO();
		$db->setQuery( $sql );
		$report = $db->loadObjectList();
		
		if($db->getErrorMsg()){
			$status = "<br />".$db->getErrorMsg();
			return $status;
		}
		
		return $report;
	}											
	

	/**
	 *  Return total score for question ID
	 *   return sum
	 */
	function getReportSumQuestion($questionID){
		$sql =  'SELECT sum(user_score) AS `total_question_score` FROM `#__quiz_report_question` WHERE `question_id` = "'.$questionID.'"';	
		$db = JFactory::getDBO();
		$db->setQuery( $sql );
		$report = $db->loadObjectList();
		
		if($db->getErrorMsg()){
			$status = "<br />".$db->getErrorMsg();
			return $status;
		}
		
		return $report;
	}											

	//Return total score for question ID	
	function getQuestionAllId($quizID){	
		$db = JFactory::getDBO();	
		$sqlQuestionNum = 'SELECT `questionNum` FROM `#__quizid_questionid` WHERE ID = "'.$quizID.'"';
		$db->setQuery( $sqlQuestionNum );
		$sqlQuestionNums = $db->loadObjectList();
		
		return $sqlQuestionNums;
	}

	//How many total attempts per question by all users
	function getQuestionAttemps($questionID){	
		$db = JFactory::getDBO();	
		$sqlQuestionNum = 'SELECT count(question_id) AS `question_attempts` FROM `#__quiz_report_question` WHERE `question_id` = "'.$questionID.'"';
		$db->setQuery( $sqlQuestionNum );
		$sqlQuestionNums = $db->loadObjectList();
		
		return $sqlQuestionNums;
	}	
	
	

	//DUPED from newquiz 17/06/2013
	//get the last question number from the quiz ID
	function lastQuestionNum($qID = NULL){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT MAX(num) FROM `#__quiz_question` WHERE quizID='.$qID;
		$db->setQuery( $queryQ );
		$result = $db->loadResult();	
				
		return $result;		
	}


}