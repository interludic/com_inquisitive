<?php // no direct access

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */


	defined('_JEXEC') or die('Restricted access'); 
	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);

	$uncorrected = false;
	$i = 1;		
	$c = $this->giveQuizStrings;
	$total = round(($c['percentages']['total'] * 100));
				
	//false survey, means this is a quiz, so return results for the user to learn from their mistakes.
	if(!$c['survey']){
		$feedbackResults = "<div class='base-layer'><!-- Begin results list -->"; 	
		$rowDivQuestion = "<div class='table-row-question clearfix'> <!--  Begin Question row -->";
		$rowDivAnswer = "<div class='table-row-answer-result clearfix'> <!--  Begin Answer row -->";
		$rowDivAnswerCorrect = "<div class='table-row-answer-result clearfix' id='green'> <!--  Begin Answer correct row -->";
		$leftCellCorrect = "<div class='left-cell-correct'>";
		$spaceLine = "<div class='space-line'></div>";
		$leftCellAnswer = "<div class='left-layer11'>";
		
		foreach($c['displayResults'] as $question){
			$feedbackResults .= $rowDivQuestion."<div>".$i." : ".$question[1]['question']."</div>".$spaceLine;
			$feedbackResults .= "</div> <!-- End question row -->";
						
			foreach($question as $answer){
				if($answer['creatorResult'] == "1")	$feedbackResults .= $rowDivAnswerCorrect;//highlight true
					else $feedbackResults .= $rowDivAnswer;
				
				if(($answer['studentsResult'] == $answer['creatorResult']) && ($answer['creatorResult'] == "1")){
					$feedbackMark = "<img src='components/".$this->comName."/views/tmpl/images/tick.png' alt='yes'/>";					
				}elseif(($answer['studentsResult'] != $answer['creatorResult']) && ($answer['studentsResult'] == "1")){
					$feedbackMark = "<img src='components/".$this->comName."/views/tmpl/images/cross.png' alt='no'/>";							
				}elseif(($answer['studentsResult'] != $answer['creatorResult']) && ($answer['creatorResult'] == "1")){
					$feedbackMark = "<img src='components/".$this->comName."/views/tmpl/images/correctArrow.png' alt='no'/>";						
				}else{
					$feedbackMark = "&nbsp;";
				}
						
				$feedbackResults .= $leftCellCorrect.$feedbackMark;
			
				$feedbackResults .= "</div>";
				$feedbackResults .= $leftCellAnswer;
				if($question[1]['question_type'] != 't') $feedbackResults .= $answer['aDesc']->answer;
					else $feedbackResults .= $question[1]['user_input']." <br/> <i>".JText::_('COM_INQUISITIVE_GIVE_KEYWORDS_TITLE').$answer['aDesc']->keywords."</i>";//ignore user_input, incorrectly labelled
				$feedbackResults .= "</div><!-- end left cell -->";
				$feedbackResults .= "</div> <!-- End answer row -->";
				$feedbackResults .= $spaceLine;
				
			}
			//Percentage per question
				$questionPercent = ($c['percentages'][$i]*100);
				$feedbackResults .= $rowDivAnswer."<strong>".$questionPercent."%</strong>  </div> <!-- End % answer row -->";
				$feedbackResults .= $spaceLine;
	
			$i++;		
		}
		
		$feedbackResults .="</div> <!-- end results list -->";
		$feedbackResults .= "<h1 class='quizGive'>".JText::_('COM_INQUISITIVE_GIVE_RESULTS_TITLE');."</h1><h2 class='quizGive'>$total%</h2><br/><br/>";
		echo $feedbackResults;
		
	}else{		
		echo JText::_('COM_INQUISITIVE_SURVEY_COMPLETE');
	}	
?>
<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>
<!-- end of inquisitive give -->
