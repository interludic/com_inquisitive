<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');

class InquisitiveViewError extends JViewLegacy
{
	
	function display($tpl = null)
	{			
	
		$error = JRequest::getVar('msg');
		
		$this->assignRef('error', $error);	
		parent::display($tpl);
	}
}
?>
