function addResource(addContent){
	if(!document.getElementById(addContent+"input")){
	    var input = document.createElement('input');
		input.setAttribute('name', addContent);
		input.setAttribute('id', addContent+"input");
		input.setAttribute('type', 'text');
		input.setAttribute('size', '54');
		input.setAttribute('value', '');
		document.getElementById(addContent).appendChild(input);
//		alert(addContent);

		var orig = document.getElementById('addURL');
		var removeDiv = document.getElementById('addURLbtn');
		orig.removeChild(removeDiv);
	}
}

function addResourceTextarea(addContent){
	var addURLbtn = document.getElementById(addContent);
    var input = document.createElement('textarea');
	input.setAttribute('name', addContent);
	//input.setAttribute('type', 'text');
	input.setAttribute('cols', '40');
	input.setAttribute('rows', '5');
	document.getElementById(addContent).appendChild(input);

	var orig = document.getElementById('addAnswer');
	var removeDiv = document.getElementById('addAnswerbtn');
	orig.removeChild(removeDiv);
}

function keywordsCheck(contents, num){
	var keywords = document.getElementById('keywords'+num);	
	if(keywords.value == "")	keywords.value = "Seperate keywords with commas.";
		else if(keywords.value == "Seperate keywords with commas.") keywords.value = "";
}

function questionCheck(){
	var questionNum = document.getElementById('question_num');	
	var question = document.getElementById('question');	
	if((questionNum.value == "1") && (question.value == "")) return false;
		else return true;
}

/* http://wheatfield.wordpress.com/2008/04/10/javascript-add-elements-on-the-fly/ */
function add(answerSwitch, answer_label, answer_correct, disable_element) {		
		if(answer_label == undefined) answer_label = '';
		if(answer_correct == undefined) answer_correct = '';
		if(disable_element == undefined) disable_element = '';		
		//alert("answerSwitch ="+answerSwitch+" answer_label ="+answer_label+" answer_correct ="+answer_correct);
		
		if(answer_correct == "on") answer_correct = 'checked=checked';
		//window.scrollBy(0,150);//get x/y of mouse?
    	document.getElementById('answerNum').style.display="block";
//		document.getElementById('answer').focus();
		var orig = document.getElementById('content-inquisitive');
		var count = parseInt(document.getElementById('count').value);
  		var answerBankAmt = document.getElementById('answerBankAmt');
  		var questionNum = document.getElementById('question_num');
		var newDiv = document.createElement('div');
		newDiv.setAttribute("id", "item"+count);

		var multiCheckbox = 
		  '<div class="table-row-answer">'+ 
		  	'<div class="headingLabel">'+ 
			'Answer:<p class="numText" ID="answerNum" style="display: block;">' + count + '</p></div>' + 
			'<div class="left-layer11">'+
				'<textarea name="answer' + count + '" id="answer' + count + '" cols="40" rows="4"'+disable_element+'>'+answer_label+'</textarea>&nbsp;</div>'+
			'<div class="right-layer11 dashed"><div class="rowMultiChoice">Correct'+
			'</div><div class="space-line"></div>'+
			'<div class="rowMultiChoice">&nbsp;&nbsp;&nbsp;'+
				'<input type="checkbox" name="true' + count + '" id="correct" '+answer_correct+' '+disable_element+'/>'+
			'</div>'+
			'<div class="space-line"></div>'+
		  '</div><div class="space-line"></div></div>';
		  
		  var singleRadio = 
		 '<div class="table-row-answer">'+ 
		  	'<div class="headingLabel">'+ 
			'Answer:<p class="numText" ID="answerNum" style="display: block;">' + count + '</p></div>' + 
			'<div class="left-layer11">'+
				'<textarea name="answer' + count + '" id="answer' + count + '" cols="40" rows="4" '+disable_element+'>'+answer_label+'</textarea>'+'&nbsp;</div>'+
			'<div class="right-layer11 dashed"><div class="rowMultiChoice">'+
			'Correct'+
			'</div><div class="space-line"></div>'+
			'<div class="rowMultiChoice">&nbsp;&nbsp;&nbsp;'+
				'<input type="radio" name="Q'+questionNum.value+'" value="'+count+'" id="Answer'+count+'" '+answer_correct+' '+disable_element+'/>'+
			'</div>'+
			'<div class="space-line"></div>'+
		  '</div><div class="space-line"></div></div>';
		  
		  if(answerSwitch == "r") newDiv.innerHTML = singleRadio;
		  	else if(answerSwitch == "c") newDiv.innerHTML = multiCheckbox;
			
		orig.appendChild(newDiv);
		document.getElementById('count').value = count+1;			
		answerBankAmt.value = count;
		window.location.hash = "#footer-inquisitive";
	}	
		
function removeAnswer() {
//		document.newQuestion.rem-answer.focus();
		var count = parseInt(document.getElementById('count').value);
		if (count > 0) {				
			var orig = document.getElementById('content-inquisitive');
			var removeDiv = document.getElementById('item'+(count-1));
//			alert(removeDiv+"Removed);
			orig.removeChild(removeDiv);			
			document.getElementById('count').value = count - 1;
			document.getElementById('answerBankAmt').value = count - 2;			
			window.location.hash = "#footer-inquisitive";
		}
		//alert("Mesg removed "+document.getElementById('answerBankAmt').value+" left");
	}
	
function checkAnswered(question_check){			
		//alert("question check = "+question_check);
		if(question_check == false) return true;
		
		var i=1;//i = answerNum
		var answerBankAmt = document.getElementById('answerBankAmt');
		var question = document.getElementById('question');
		var questiontype = document.getElementById('questiontype');
		
		if (question.value == null || question.value == '') {
			var nullAlert = true;		
			alert("Please fill in the question box number ");
			return false;
		}
		

		if(questiontype.value == "questiontext") return true;
			else{
				for(h=0; h < answerBankAmt.value; h++){	
					var answer = document.getElementById('answer'+i);					
					//alert("i "+i+" total="+answerBankAmt.value+" answer="+answer.value+" nullAlert =");		
					if (answer.value == null || answer.value == ''){// && nullAlert == false (used when question should not be null)
						alert("Please fill in the answer box number "+i);
						return false;
						break;			
					}	
				i++;
				}			
			}	
		return true;
	}

	
function submitbutton(pressbutton){
	var h=0;
	var answerNull=0; //boolean check to see if answer is null
	var keywordsNum = document.getElementById('answerBankAmt');		
	
//					alert("press 2= "+pressbutton);		
		switch (pressbutton) {
				
			case 'cancel':	//obsolete due to router			
				document.newQuestion.action = "index.php?option=com_inquisitive&controller=quizlist";
//				alert(document.newQuestion.action);
				document.newQuestion.submit();
				
				return;
				break;

			case 'publish':		
				var question_check = document.getElementById('question_check');	
				if(question_check == null) question_check = true;
					else question_check = false;				

				if(checkAnswered(question_check)){
					var quizName = document.getElementById('quizname');
					//alert("quizname "+quizName.value);
					if (quizName.value != '' && quizName.value != null){// && answerNull == 0
						document.newQuestion.action = "index.php?option=com_inquisitive&controller=newquiz&task=publish&view=newquiz";
						document.newQuestion.submit();
					}else{
						alert("Please enter a quizname.");
					}
				}
				return;
				break;

			case 'nextQuestion':
				//Check if quiz name has been entered
				var quizName = document.getElementById('quizname');
				if (quizName.value != '' && quizName.value != null){					
					if(checkAnswered()){
						document.newQuestion.action = "index.php?option=com_inquisitive&controller=newquiz&task=save&view=newquiz";
						document.newQuestion.submit();
					}	
					
				}else{
					alert("Please enter a quizname.");
				}
			
				
				return;
				break;	
			
			case 'prevQuestion':				
				//Check if quiz name has been entered
				var quizName = document.getElementById('quizname');
				if (quizName.value != '' && quizName.value != null){
					
					if(checkAnswered()){
						document.newQuestion.action = "index.php?option=com_inquisitive&controller=newquiz&task=save&previous=1";
						document.newQuestion.submit();
					}				
					
				}else{
					alert("Please enter a quizname.");
				}

				
				return;
				break;		
				
						
		}
	}

/* End Question / Answer handling */