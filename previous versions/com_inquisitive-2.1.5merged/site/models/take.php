<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

		
/**
 * Inquisitive Model for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport( 'joomla.application.component.model' );


class InquisitiveModelTake extends JModelLegacy{	

	/**
	 *  Retrieve quiz answers from answers table
	 *  	$q should be the question ID
	 *  	return array 
	 */
	function getQuizAnswers($q = NULL, $a = NULL, $id = NULL){		
		$db = JFactory::getDBO();
		if(isset($q)) {
			$qr = ' AND question_num= "'.$q.'"';
			$idx = 'question_num';
		}else{
			$qr = "";
			$idx = NULL;
		}

		if(isset($a)) $ar = ' AND num= "'.$a.'"';
			else $ar = "";

		if(!isset($id)) $id = JRequest::getVar('ID');
		$sqlquizA =  'SELECT * FROM `#__quiz_answer` WHERE timestamp = "'.$id.'"'.$qr.$ar." ORDER BY num ASC";
				
		//Get question option (multiple choice?) how many?
		$db->setQuery( $sqlquizA );
		$sqlquizAnswers = $db->loadObjectList();
		//echo "<br/><strong>getQuizAnswers</strong> -  question_id =".$q."<br/>".$sqlquizA." </br>";// .$sqlquizA[0]->question_num;//.$sqlquizA." /";
		//print_r($sqlquizAnswers);
		
		if($db->getErrorMsg()) echo $db->getErrorMsg();

		return $sqlquizAnswers;
	}

	/**
	 * User takes the quiz now
	 *
	 */
	function getQuizName($id){
		$sqlquizName =  'SELECT quiz_name FROM `#__quiz_info` WHERE timestamp = "'.$id.'"';

		$db = JFactory::getDBO();
		$db->setQuery( $sqlquizName );
		$sqlquizName = $db->loadObjectList();
		
/*		echo "<pre>";
		print_r($sqlquizName);
		echo "</pre>";*/
		
		if($db->getErrorMsg()) return "<br />A-".$sqlquizName."<br />".$db->getErrorMsg();

		if(isset($sqlquizName[0]->quiz_name)) return $sqlquizName[0]->quiz_name;
			else return;
	}

	function getQuizCreator($id, $email){
		$sqlquizName =  'SELECT teacher_name FROM `#__quiz_info` WHERE timestamp = "'.$id.'"';
		$db =JFactory::getDBO();
		$db->setQuery( $sqlquizName );
		$sqlquizName = $db->loadObjectList();
		
		//TODO store teacher_name as id
		$sqlUser =  'SELECT * FROM `#__users` WHERE username = "'.$sqlquizName[0]->teacher_name.'"';
		$db->setQuery( $sqlUser );
		$sqlUserDetails = $db->loadObjectList();
		
		$user = JFactory::getUser($sqlUserDetails[0]->id);
		$creator['name'] = $sqlquizName[0]->teacher_name;
		$creator['email'] = $user->email;

		if($db->getErrorMsg()){
			$status .= "<br />A-".$sqlquizName."<br />".$db->getErrorMsg();
			return $status;
		}else{
			return $creator;
		}
	}


	/**
	 * Retrieve quiz questions from question table
	 *  return array to display
	 *  $question_index 	= question_index
	 *	$question_num 		= guestion num 
	 *  $id 				= Quiz id
	 */
	function getQuizQuestions($question_index = NULL, $id, $question_num = NULL){ 	
		$orderby = ' ORDER BY `questionIndex`';
		$r = 0;
		$db = JFactory::getDBO();
		if($id == NULL)	$id = JRequest::getVar('ID'); //problem with the view passing ID to model functions.
		
		if(($question_index == NULL) && ($question_num == NULL)){
			$sqlquizQ = 'SELECT * FROM `#__quiz_question` WHERE quizID = "'.$id.'"'.$orderby; 			
			$db->setQuery( $sqlquizQ );
			$sqlquizArray = $db->loadObjectList();		
			return $sqlquizArray;
		}
					
		//echo "getQuizQuestions INPUT question_num= ".$question_num." id = ".$id." question_index = $question_index<br/>";

		if($question_index == NULL){	
			//Lets get the question id's per quiz id
			$sqlQuestionNum = 'SELECT * FROM `#__quizid_questionid` WHERE ID = "'.$id.'"'.$orderby;
			$db->setQuery( $sqlQuestionNum );
			$sqlQuestionNums = $db->loadObjectList(); //Get Quiz
					
			if($sqlQuestionNums == NULL) return "ID not in quizID_questionID - ".$sqlQuestionNum."</br>";																				 
			foreach ( $sqlQuestionNums as $row ) { 	
				//Get question data
				$sqlquizQ = 'SELECT * FROM `#__quiz_question` WHERE questionIndex = "'.$row->questionNum.'"'.$orderby; 			
				$db->setQuery( $sqlquizQ );
				$sqlquizQ = $db->loadObject();
				$sqlquizArray[$r] = $sqlquizQ;
				$r++;
								
	/*			echo "<h3>getQuizQuestions</h3> - <pre>";
					print_r($row);
					print_r($sqlquizQ);
				echo "</pre>". $sqlquizQ->num." ".$row->questionNum;
*/
				//switch to get question id, then call this function again with the correct question ID
				if($sqlquizQ != NULL){
					//Question number Navigation will stem from here most likely					
					if(($question_num != NULL) && ($question_num == $sqlquizQ->num)){
						return $row->questionNum;
					}
				}else{
					return false;
				}
			}
		}else{
			$sqlquizQ = 'SELECT * FROM `#__quiz_question` WHERE questionIndex = "'.$question_index.'"'; 			
			$db->setQuery( $sqlquizQ );
			$sqlquizArray = $db->loadObject();		
			return $sqlquizArray; //." timestamp(id)= ".$timestamp." SQL =".$sqlquiz;	
		}
				
		if($db->getErrorMsg()) return $db->getErrorMsg();		
		return false;
	}

	/**
	 * Retrieve quiz questions from question table
	 *  return array to display
	 *  $question_index 	= question_index
	 *  $id 				= Quiz id
	 */
	function getQuestionRow($question_index){ 	
		$db = JFactory::getDBO();
		$sqlquizQ = 'SELECT * FROM `#__quiz_question` WHERE questionIndex = "'.$question_index.'"'; 			
		$db->setQuery( $sqlquizQ );
		$sqlquizArray = $db->loadObject();		

		if($db->getErrorMsg()) return $db->getErrorMsg();

		return $sqlquizArray; 	
	}

	
	/**
	 * Return quiz questions with the location updated to reflect media output
	 * EG: youtube.com videos displayed in iframe
	 */
	function getOutputURL(){
/*		$questionsArray = $this->getQuizQuestions();
		foreach($questionsArray as $row){
			// get host name from URL
			if($row->location != NULL){
				preg_match('@^(?:http://)?([^/]+)@i', $row->location, $matches);
				$host = $matches[1];
		
				//echo "row ".$row->location." host =".$host."<br />";
				if(($host == "www.youtube.com") or ($host == "youtube.com")){
					//http://apiblog.youtube.com/2010/07/new-way-to-embed-youtube-videos.html
					if($this->getYTid($row->location) != false){
						$row->location = "<div class='youtube' ><P ALIGN=center><a href='".$row->location."' target='_blank'>Video not playing correctly?</a><br/><iframe class='youtube-player' align='center' type='text/html' width='425' height='325' src='http://www.youtube.com/embed/".$this->getYTid($row->location)."' frameborder='0'></iframe></p></div>";

					}
				}
			}
		}
		return $questionsArray;*/
	}


	/**
     * Retrieve the video ID from a YouTube video URL
     * @param $ytURL The full YouTube URL from which the ID will be extracted
     * @return $ytvID The YouTube video ID string
	 */
	function getYTid($ytURL) {
	    $ytvIDlen = 11; // This is the length of YouTube's video IDs     

    	// The ID string starts after "v=", which is usually right after
	    // "youtube.com/watch?" in the URL

    	$idStarts = strpos($ytURL, "?v=");
	    // In case the "v=" is NOT right after the "?" (not likely, but I like to keep my
    	// bases covered), it will be after an "&":

		if($idStarts == FALSE) $idStarts = strpos($ytURL, "&v=");
		if($idStarts == FALSE) return false;

		// If still FALSE, URL doesn't have a vid ID
		//  if($idStarts === FALSE) $idStarts = 10;//die("YouTube video ID not found. Please double-check your URL.");

		// Offset the start location to match the beginning of the ID string
	    $idStarts +=3;
		
		// Get the ID string and return it
  	    $ytvID = substr($ytURL, $idStarts, $ytvIDlen);

		return $ytvID;
	}

	
	/* email results or prepare for other output */
	function publish($username, $id, $quizResult, $giveModel){
		/*echo "Publish (email)<pre>";
		print_r($_POST);
		echo "</pre>";*/
		
		//check if sending emails is enabled for quiz creator.
		if($this->quizTable("email_creator") == "on"){
			//$user = JFactory::getUser();
			$creator = $this->getQuizCreator($id, TRUE);			
			if(isset($creator[0]->teacher_name)) $creator_name = JFactory::getUser( $creator[0]->teacher_name );
			
			$quizName = $this->getQuizName($id);
			$subject = "[Inquisitive: ".$quizName."] results for ".$username;//quiz name, student name
			$message = $this->emailQuizBody($id, $username, $giveModel); //select results from DB

			$output = $subject.$message;

			// email test
			/*
			$headers = "From: myplace@here.com\r\n";
			$headers .= "Return-Path: myplace@here.com\r\n";
			mail($creator->email, $subject, $message, $headers);
			 * */

			$this->mailOutResponse($subject, $message, $creator['email']); 

		}
		return $output;
	}
	
	/**
	 * quiz table returns true or false
	 * pass the column name in plain text
	 */
	function quizTable($columnName){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT '.$columnName.' FROM `#__quiz` WHERE 1';
		$db->setQuery( $queryQ );
		$result = $db->loadResult();
		
		if($db->getErrorMsg())$db->getErrorMsg();
		
		if(($result == NULL) or ($result == "0")) return false;
			else return $result;
	}
	
	function mailOutResponse($subject, $message, $email, $additionalEmail=NULL){
		$mailer = JFactory::getMailer();
		$user = JFactory::getUser();
		$config = JFactory::getConfig();
		
		$sender = array($user->email, $user->name);
		
		if($additionalEmail != NULL) $recipient = array($additionalEmail, $email );	
			else $recipient = $email;

		//echo "<br />".$email." <br />recipient =".$recipient."<br />";
		
		$mailer->addRecipient($recipient);
		if($user->email == $email) $sender = JText::_('COM_INQUISITIVE_NOTIFY_SENDER_EMAIL');// inquisitive //Send from inquisitive.
		$mailer->setSender($sender);		
		$mailer->setSubject($subject);
		$mailer->setBody($message);
		$mailer->isHTML(true);

		// Optional file attached
		//$mailer->addAttachment(PATH_COMPONENT.DS.'assets'.DS.'document.pdf');
		//$mailer->AddEmbeddedImage( PATH_COMPONENT.DS.'assets'.DS.'logo128.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );
		
		$send = $mailer->Send(); 
		if ( $send != true ) $status = "Error sending email: ".$send->message . " Error ".$mailer->ErrorInfo;
			else $status = "Mail sent";		

		$mailer->ClearAddresses();
		$mailer->ClearAttachments(); 
			
		return $status;
	}
	
	/**
	 * Return quiz ready to email.
	 * !!!!!!!!!!!!!this needs to be revisited when a need to format output 
	 */
	function emailQuizBody($id, $user, $giveModel){
		$questions = $this->getQuizQuestions(NULL, $id, NULL);
		$email = " ";
		$totalAccuracy = 0;
		$i = 0;//question index num
		/*	echo "Answers<PRE>";
			print_r($answers);
		echo "</PRE>";

		echo "Questions ".count($questions)."<PRE>";
			print_r($questions);
		echo "</PRE>";	
		*/	

		foreach ( $questions as $rowQ ) {
			$email .= "<br /><br />Question".$questions[$i]->num.": ".$rowQ->question;
     		$answers = $this->getQuizAnswers($questions[$i]->num, NULL, $id);	
			
      		//Display answers relative to their question
			foreach ( $answers as $rowA ) {        		
        		//match question and answer.
    			//echo "<br/>Match answers question index ".$rowQ -> questionIndex." and question num ".$rowA -> question_num; 
				if($rowQ -> num == $rowA -> question_num){
						//return result and accuracy for each answer
						$email .= "<br />Answer ".$rowA->num.": ".$rowA->answer."<br />";						
						$results = $giveModel->getStudentResults($id, $user, $rowA->question_num, $rowA->num);
						
						echo "Results<PRE>";
							print_r($results);
						echo "</PRE>";
						if($results[0]->result != NULL){							
							$email .= "User response = ".$results[0]->result."<br/>";																								
							if($rowA->correct != NULL) $email .= "<strong>Quiz response = ".$rowA->correct." </strong><br/>";
							$textResult = $rowA->type;
							if(($textResult == "t") && (isset($rowA->keywords))){
								$email .=  "<strong>Quiz response = ".$rowA->keywords." </strong><br/>";							
							}
						}					
				} 
			}//END ANSWER FOR LOOP 	
			$i++;
		}//END Question
		
		//controller Publish
		$link = JURI::base().'index.php?option=com_inquisitive&controller=give&uid='.$_POST['username'].'&qid='.$_POST['ID'];				
		$email .= "<br />";
		$email .= "<a href=".$link."> Accuracy</a>";
		$email .= "<br />";
		//--------------------
/*		$QN = count($questions);
		$RN = count($results);
		$resultCount = 1;	
		print_r($RN);
		
		$email = '<table width="400" border="0" cellspacing="3" cellpadding="3">';

		for($i=0; $i < $QN; $i++){//Look through each question number
		
			$email .="
	          <tr>
   		         <th scope='row'>Question # ".$questions[$i]->questionIndex."</th>
   	   		     <td>".$questions[$i]->question."</td>
   	       		 <td>&nbsp;</td>
	   	       </tr>";
	
			for($j=0; $j < $RN; $j++){//Loop through each result.
			  	if($resultCount == $results[$j]->q){//Problem, this only displays the first answer.
					$email .= "
					  <tr>
		        	    <th scope='row'>Response # ".$results[$j]->index." resultcount=".$resultCount."</th>
	    	        	<td>".$results[$j]->result."</td>
		    	        <td>&nbsp;</td>
	    	    	  </tr>";
				}
			$resultCount++;				
			}
		}
		$email .= "</table>";*/
		return $email;
	}
	
	/**
	 * Return question number per question type
	 */
	 
	function questionNumType($questionID, $ID){
		$db = JFactory::getDBO();		
		$queryQ = 'SELECT num FROM `#__quiz_question` WHERE questionIndex = '.$questionID;
		$db->setQuery( $queryQ );
		$question_num = $db->loadResult();
		
		//echo "question type = (".$questionID.") (".$ID.") (".$question_num.") <br/>";
		
		$queryA = 'SELECT type FROM `#__quiz_answer` WHERE question_num = '.$question_num.' AND timestamp = '.$ID;
		$db->setQuery( $queryA );
		$type = $db->loadResult();
		
		if($db->getErrorMsg()) echo $db->getErrorMsg();
		
		return $type;	
	}
	
	
		/**
	 *	FIXME this should return the results via ready to display with css or pdf etc..
	 * quizResult
	 *	Take the student data and insert into table
	 *  Not testing for accuracy (this done in controllerGive)
	 */
	function quizResult(){
		//echo "quizResult() take model - <pre>";
		/*	print_r($_POST);
		echo "</pre> end quizResult";*/		
		$actualResult = NULL;
		$user = JFactory::getUser();
		$id = JRequest::getVar('ID');
		$q = JRequest::getVar('questionTotal'); //total questions
		$output = NULL;		

		//Question loop
		for($j = 0; $j < $q; $j++){			
			if($j == 0) $i = 1; //i is question number
			$questionID = JRequest::getVar('QQ'.$i);
			$totalAnswers = JRequest::getVar('totalAnswers'.$i); //Total amount of answers per question
			$aa = JRequest::getVar('Q'.$i); //Answer array (from user response)
			$row = $totalAnswers+1;
			$output .= "<br/>question# $i questionID ".$questionID."  totalA= ".$totalAnswers." user= ".$user->username." quizID= ".$id."";						
				
			//Answer Loop - check each answer
			for($ta = 1; $ta < $row; $ta++){							
				//get question type
				$questionType = $this->questionNumType($questionID, $id);
				$output .=  "<br /> questionType= ".$questionType." Answer ".$ta."  "; 
				
				switch ($questionType){
					case "c":	
						//Begin Answer Output
							if(isset($aa[$ta])){
								$actualResult = "on";
							}else{
								//user has not responded to this answer, so it's incorrect					
								$actualResult = "off"; 								
							}
											
						break;						
					case "t": //TEXT
						$actualResult = $aa[$ta];
						break;
					case "r":

							if($aa == $ta){
								$actualResult = "on";
							}else{
								$actualResult = "off"; //user has not responded to this answer, so it's incorrect					
							}
										
						break;
					case NULL:

						break;
				}			
				//if(isset($aa[$ta])) echo " aa[ta] = ".$aa[$ta];
				$output .=  " Actualresult = ".$actualResult." <br/>";
				
				
				//insert user response and correct result into result table.
				$this->insertResult($user->username, $i, $ta, $actualResult, $id);
				$actualResult = NULL;
			}
			$i++;
		}								
		//Update the current amount of quiz vs user attempts
		$this->quizAttemptNum($id);
		
		//Add quiz being published as result to quiz_info				
		$this->updateResults($id); 
		
		//echo $output;
			
		return $output;
	}
	

	/**
	 * accuracy:
	 *  Return correct %
	 * 		Get correct results from answer table for question 
	 *			?check answer type convert to MAX percentage of accuracy.?
	 */

	function accuracy($correctAmt, $totalAnswers){
		$percent = ($correctAmt / $totalAnswers);
				
		return $percent;
	} 
		
	//Update Quiz_info table:
	//get total number of results, then add one more.
	function updateResults($id){
		$db = JFactory::getDBO();	
		$subTotalQuery = 'SELECT result FROM `#__quiz_info` WHERE timestamp = "'.$id.'"';
		$db->setQuery( $subTotalQuery );
		$subTotal = $db->loadObject();
		$status = true;

		if($db->getErrorMsg()) $status .= "<br /> result ".$db->getErrorMsg();

		if($subTotal->result == NULL) $total = 1;
			else $total = $subTotal->result + 1;
				
		$infoQuery =  "UPDATE #__quiz_info SET result = '".$total."' WHERE timestamp = '".$id."'";
		$db->setQuery( $infoQuery );
		$db->query();		
		
		if($db->getErrorMsg()) $status .= "<br /> SET ".$db->getErrorMsg();		
		
		return $status;
	}
	
	/**
	 * Returns the correct results from answer table per answer
	 * correctAmount switch returns sum of correct answers per question
	 */
	function correctResult($id, $q, $a, $correctAmount){
		if($correctAmount == "correctAmount") $checkAmount = 'and correct = "on"';
			else $checkAmount = 'and num = "'.$a.'"';
			
		$db = JFactory::getDBO();
		$id = JRequest::getVar('ID');
		$sqlquizA =  'SELECT correct FROM `#__quiz_answer` WHERE question_num = "'.$q.'" '.$checkAmount;

		$db->setQuery( $sqlquizA );
		$sqlquizA = $db->loadObjectList();

		if($db->getErrorMsg()) echo $db->getErrorMsg();
		
		if($correctAmount == "correctAmount") {
					$db->query();
		$num_rows = $db->getNumRows();
		return $num_rows;
		}
			else return $sqlquizA;
	}

	
	function insertResult($user, $q, $a, $result, $id){
	//how many times can/has quiz be submitted by user?

		if($a != NULL) {
			$aa = '"'.$a.'", ';
			$attributes = "(student_name, q, a, result, id)";
		}else{
			$attributes = "(student_name, q, result, id)";
			$aa = NULL;
		}
		
		$db = JFactory::getDBO();
		$infoQuery =  "INSERT INTO #__quiz_results ".$attributes." VALUES ('".$user."', '".$q."', ".$aa."'".$this->cleanString($result, $db)."', '".$this->cleanString($id, $db)."');";
		$db->setQuery( $infoQuery );
		$db->query();

		echo "<br /><strong>insertResult</strong> - ".$infoQuery;
		if($db->getErrorMsg()) $db->getErrorMsg();
		
		return $infoQuery;
	}
	
	/**	getFinishQuiz
	 * 
	 *	Publishes current quiz to users quiz list located at component default screen.
	 *	Retrieves Questions and Answers from temp then updates Question and answer tables completing the quiz.				
	 */
		
	function getFinishQuiz(){
		$user = JFactory::getUser();
		$db = JFactory::getDBO();		
		$id = JRequest::getVar('ID');
		$quizName = JRequest::getVar('quizname');
		$status = NULL;
		
		//get the SQL 
		$queryQ = 'SELECT sqlquizQ FROM `#__quiz_temp` WHERE id = "'.$id.'"'; //secure? need user auth?
		$queryA = 'SELECT sqlquizA FROM `#__quiz_temp` WHERE id = "'.$id.'"'; //secure? need user auth?

		$db->setQuery( $queryQ );
		$sqlquizQ = $db->loadObjectList();
		$db->setQuery( $queryA );
		$sqlquizA = $db->loadObjectList();

		if($db->getErrorMsg()) $status = "<br /> Q1 ".$db->getErrorMsg();
		//." <pre> sqlquiz = ".$sqlquiz." - ".print_r($sqlquiz)."</pre>";
		
		//process the sql statement	
		foreach ( $sqlquizQ as $row ) {
			$db->setQuery( $row->sqlquizQ );
			$db->query();
//			$status .= "<br />".$row->sqlquiz."\n<br /><br />S-".$sql."<br />";	
		}

		foreach ( $sqlquizA as $row ) {
			$db->setQuery( $row->sqlquizA );
			$db->query();
//			$status .= "<br />".$row->sqlquiz."\n<br /><br />S-".$sql."<br />";	
		}

		
		if($db->getErrorMsg()) $status .= "<br /> Q2 ".$db->getErrorMsg();
				
		//insert into quiz_info table
/*		$infoQuery =  "INSERT INTO #__quiz_info (timestamp, quiz_name, teacher_name) VALUES ('".$id."', '".$this->cleanString($quizName)."', '".$user->username."');"; 		
		$db->setQuery( $infoQuery );	
		$db->query();*/
		
		if($db->getErrorMsg()) $status .= "<br /><br />Q3 ".$db->getErrorMsg()."<br />";
		
		//what if table name exists (#__quiz_info)????
		//restore quiz if crashed
		
//		$status .= /*"<br /> info Query =".$infoQuery.*/" <br />Query = ".$query." <br />";/*print_r($sqlquiz)." = Finish Quiz<br /> Error = ".*/
/*		$componentname = JRequest::getVar('option'); //get component name
		$url="index.php?option=".$componentname;
//		redirect( $url, $msg='Published!', $msgType='message' );
		$msg="Published!";
		
		$this->setRedirect($url, $msg);*/

		//remove temp table ref
		return $status;
				
	}	
	
	/**
	 * Return the amount of times a student has accessed a quiz, otherwise null for none.
	 */
	function quizAttemptNum($quizID){
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
				
		$query = 'SELECT attemptNum FROM `#__quiz_attempt` WHERE quizID = "'.$quizID.'" and userID = "'.$user->username.'"';		
		$db->setQuery( $query );
		$db->query();
		$attemptNum = $db->loadRow();

		if($attemptNum['0'] != NULL) $attemptAmt = $attemptNum['0']+1;
			else $attemptAmt = 1;			

		//upate quiz_attempt table
		$infoQuery =  "UPDATE #__quiz_attempt SET attemptNum = '".$attemptAmt."' WHERE quizID = '".$quizID."' and userID = '".$user->username."'";
		$db->setQuery( $infoQuery );
		$db->query();		

		if($db->getErrorMsg()) $status = "<br /> Cannot update user attempt -".$db->getErrorMsg();
			else $status = $attemptNum['0'];
			
		return $status;
	}
	
	//delete previous quiz attempt...
	function deleteQuizAttempt($user, $quizId){
		$db = JFactory::getDBO();
		$query = 'DELETE FROM `#__quiz_results` WHERE id="'.$quizId.'" AND student_name = "'.$user.'"';	
		 
		//echo "<br/><strong>deleteQuizAttempt</strong> - ".$query." <br/> affected rows = ".$db->getAffectedRows()."<br/>";		
		$db->setQuery( $query );
		$db->query();
		
		if($db->getErrorMsg()) JText::_( $db->getErrorMsg());
		
		return $db->getAffectedRows();		
	}
	
	//Duped from newquiz model, too lazy to pass the model.
	function jVersion(){	
		JLoader::import( 'joomla.version' );
		$version = new JVersion();
		if (version_compare( $version->RELEASE, '2.5', '<=')) return "2.5";
			else return "3";
	}

	function cleanString($subject, $db){
		$jVersion = $this->jVersion();
		/*
		http://css-tricks.com/snippets/php/sanitize-database-inputs/
		$in_string = ltrim($string);      
		$in_string = rtrim($in_string);
		mysqli_real_escape_string
		*/

		/*
		$search = "$";

		$replace = "\$";

		$subject2 = str_replace($search, $replace, $subject);*/
		
		if($jVersion == "2.5") $escaped_subject = $db->getEscaped($subject); //getescaped is for 2.5
			else $escaped_subject = $db->escape($subject); //escape is for 3

		//$quote_subject = $db->quote($escaped_subject);

		echo "<h2> escaping... </h2>".$escaped_subject;

		return $escaped_subject;
	}
}