<?php

/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );


/**
 * New Quiz Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerNewQuiz extends JControllerLegacy
{
	/**
	 * Method to create a new quiz, Insert into quiz_tbl, quizQuestion_tbl and quizAnswer_tbl tables
	 *
	 * @access	public
	 */

	 
	function display($cachable = false, $urlparams = false){
		//Check authority	
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$ID = JRequest::getVar('ID');
			$take = $this->getModel('take');		
					
			$model = $this->getModel('newquiz');
			
			if(isset($ID)){
				$quiz_name = $take->getQuizName($ID);
				$editCheck = $model->editCheck($ID);
				JRequest::setVar('editcheck', $editCheck);
			} 
			
			if(isset($quiz_name)) JRequest::setVar('quizname', $quiz_name);
						
			JRequest::setVar('view', 'newquiz');
			JRequest::setVar('layout', 'default');
			parent::display();
		}else{		
			//FIXME redirector ?	
			//TODO  Message for student not enough permission		
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;			
			$this->setRedirect($link);						
			////JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
	}

	//Save from previous, Next and submit buttons.
	//update db, then move to next question
	function save($state = NULL){
		//Check authority	
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){ 			
			echo "Save = <pre>";
				print_r($_POST);
			echo "</pre>";
			$id = JRequest::getVar('ID');
			$model = $this->getModel('newQuiz');
			$quiz_name = JRequest::getVar('quizname');
			$question_num = JRequest::getVar('question_num');
			$answer_count = JRequest::getVar('answerCount', "1");
			$edit_check = JRequest::getVar('editcheck');
			$previous = JRequest::getVar('previous');	
			$question_index = JRequest::getVar('question_index');		
			
			$take = $this->getModel('take');
						
			$quiz_name_check = $take->getQuizName(JRequest::getVar('ID'));
			$getQuestionID = $model->getQuizQuestionId($id, $question_num);
			
			//Check quiz publish status
			$quizlist = $this->getModel('quizlist');				
			$getQuizList = $quizlist->quizList($id);
			
			//echo "quiz status = ".$getQuizList[0]->hidden."<br />";
			//print_r($getQuizList);

			//Save if unpublished quiz	
			if($getQuizList[0]->hidden == 0) $status = $model->updateQuiz($quiz_name_check, $getQuestionID, $take);
				else $status = "Quiz is published - no change.";
						
			if(isset($previous)) $question_num = $question_num - 1;				
				else $question_num = $question_num + 1;
						
			$link_edit = "index.php?option=".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;
	
			$link_publish = "index.php?option=".JRequest::getVar('option')."&controller=quizlist";
			
			
			//DEBUG post info									
			if($state == "publish") $this->setRedirect($link_publish);
				else $this->setRedirect($link_edit);
				
			echo "<a href=".$link_edit."> Proceed </a> - $link_edit<br/>$status";
					
			//parent::display();
			return "Question saved ( ".$status." ) ";
				
		}else{		
			//FIXME redirector ?			
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;			
			$this->setRedirect($link);						
			////JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}		
	}

	//Get the quiz questions from the db
	//pass the question variables/array to the view	
	function edit() {
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$take1 = $this->getModel('take');
			$model = $this->getModel('newquiz');
			$quiz_name = JRequest::getVar('quizname');
			$previous = JRequest::getVar('previous');		
			$edit_check = JRequest::getVar('editcheck');
			$ID = JRequest::getVar('ID');
			$question_index = JRequest::getVar('question_index');
			$question_num = JRequest::getVar('question_num', 1);					
							
	//!!!!!!!check current users permissions with ID		
	
			$getQuestionID = $model->getQuizQuestionId($ID, $question_num);
			//echo "<br/><strong>EDIT - question_num</strong> = ".$question_num." get question id = $getQuestionID <br/>";						
	
			//Check if question exists
			$questionExists = $model->questionExistence($ID, $getQuestionID);			
			
			//last_question	identifies the last question number stored in the db
			$last_question_num = $model->lastQuestionNum($ID);
			
			//Check read and write status
			$editCheck = $model->editCheck($ID);		
	
			if(($question_num == $last_question_num) && ($editCheck == "read")) JRequest::setVar('last_question', 'true');		
			
			//Switch back to new question if there is no more questions to edit.
			if((!$questionExists) && ($previous == NULL)){
				//ANYTHING NEED SAVING?
				$link = "index.php?option=".JRequest::getVar('option')."&controller=newquiz&question_num=".$question_num."&ID=".$ID."&view=newquiz";
				
				$this->setRedirect($link, $msg);
				echo "<a href=".$link.">Proceed</a> ";//.$status;
			}
			
			$questionArray = $take1->getQuizQuestions(NULL, $ID, NULL);
					
			//get questions
			$question = $take1->getQuizQuestions($getQuestionID, $ID, NULL);
	
			//get answers
			$answerArray = $take1->getQuizAnswers($question_num, NULL, $ID);
						
	/*		echo "Edit <strong>question ID </strong>".$getQuestionID." count = ".count($questionArray)." ID=".$ID."<pre>";
				print_r($questionArray);
			echo "<br/> Question = ";
				print_r($question);
			echo "<br/> Answer - ";
				print_r($answerArray);	
			echo "</pre>";*/
			
			if(isset($answerArray[0]->type)){
				switch($answerArray[0]->type){	//Set the approriate view for the question			
					case "r":
						$viewType = "questionsingle";
						break;
					case "c":
						$viewType = "questionmulti";
						break;
					case "t":
						$viewType = "questiontext";
						break;
				}
			}else{
				$viewType = "questiontext";
			}
						
			//Create an index for easy navigation
			//echo " getQuestionID = ".$getQuestionID." (ID ".$ID." question_num ".$question_num.") question_index = ".$question_index." previous ?".$previous." ".$previous."<br/>";
	
			//View should detect the edit/variables/array and populate the fields.
			JRequest::setVar('quizname', $take1->getQuizName($ID));
			
			JRequest::setVar('questionArray', $questionArray);		
			JRequest::setVar('question', $question);	
			if(isset($question->num)) JRequest::setVar('question_num', $question->num);
			if(isset($question->questionIndex)) JRequest::setVar('question_index', $question->questionIndex);		
			JRequest::setVar('answerArray', $answerArray);
			JRequest::setVar('editcheck', $editCheck);
			JRequest::setVar('ID', $ID);
			JRequest::setVar('view', $viewType);
			parent::display();
			}else{
			//FIXME redirector ?			
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;			
			$this->setRedirect($link);						
			////JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
	}

	function publish() {
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){		
			//print_r($_POST);
			$model = $this->getModel('newQuiz');		
			$take = $this->getModel('take');
			$quiz_name_check = $take->getQuizName(JRequest::getVar('ID'));
			$qn = JRequest::getVar('quizname');//BUG?		
			
			$status = $this->save("publish");
			
	
			
			JRequest::setVar('quizname', $qn);
			JRequest::setVar ( 'view', '' );
			JRequest::setVar ( 'status', $status );
			JRequest::setVar ( 'layout', 'default' );
			
			//echo "quiz_name_check = ".$quiz_name_check;
			//check if quiz exitst then update if true.
			//echo $model->updateQuiz($quiz_name_check, NULL);//redirect outputs to view
			
			//echo $model->getFinishQuiz();
			
			parent::display ();
		}else{		
			//FIXME redirector ?			
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;			
			$this->setRedirect($link);						
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
	}
}

?>
