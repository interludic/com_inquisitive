<?php
/*ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);*/

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Inquisitive Component
 *
 * @package		Joomla.Tutorials
 * @subpackage	Components
 */
 
 //View folder contains view classes and templates. Every view class may have few templates that are stored in tmpl folder. Every view class has the same name view.html.php. And tmpl folder contains html template files. 
 
class InquisitiveViewResults extends JViewLegacy
{
	
	function display(){	

/*		$model = &$this->getModel('take');
		echo $model->publish();*/

/*		$model =& $this->getModel();
		$output = $model->publish();
		$this->assignRef('output', $output);*/
		
		parent::display();
	}
}
?>
