<?php // no direct access

	defined('_JEXEC') or die('Restricted access'); 
	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);

	$uncorrected = false;
	$i = 1;		
	$c = $this->giveQuizStrings;
	$total = round(($c['percentages']['total'] * 100));
				
	//false survey, means this is a quiz, so return results for the user to learn from their mistakes.
	if(!$c['survey']){
		$feedbackResults = "<div class='base-layer'><!-- Begin results list -->"; 	
		$rowDivQuestion = "<div class='table-row-question clearfix'> <!--  Begin row -->";
		$rowDivAnswer = "<div class='table-row-answer-result clearfix'> <!--  Begin row -->";
		$rowDivAnswerCorrect = "<div class='table-row-answer-result clearfix' id='green'> <!--  Begin row -->";
		$leftCellCorrect = "<div class='left-cell-correct'>";
		$spaceLine = "<div class='space-line'></div>";
		$leftCellAnswer = "<div class='left-layer11'>";
		
		foreach($c['displayResults'] as $question){
			$feedbackResults .= $rowDivQuestion."<div>".$i." : ".$question[1]['question']."</div>".$spaceLine;
			$feedbackResults .= "</div> <!-- End question row -->";
						
			foreach($question as $answer){
				if($answer['creatorResult'] == "1")	$feedbackResults .= $rowDivAnswerCorrect;//highlight true
					else $feedbackResults .= $rowDivAnswer;
				
				if(($answer['studentsResult'] == $answer['creatorResult']) && ($answer['creatorResult'] == "1")){
					$feedbackMark = "<img src='components/".$this->comName."/views/tmpl/images/tick.png' alt='yes'/>";					
				}elseif(($answer['studentsResult'] != $answer['creatorResult']) && ($answer['studentsResult'] == "1")){
					$feedbackMark = "<img src='components/".$this->comName."/views/tmpl/images/cross.png' alt='no'/>";							
				}elseif(($answer['studentsResult'] != $answer['creatorResult']) && ($answer['creatorResult'] == "1")){
					$feedbackMark = "<img src='components/".$this->comName."/views/tmpl/images/correctArrow.png' alt='no'/>";						
				}else{
					$feedbackMark = "&nbsp;";
				}
						
				$feedbackResults .= $leftCellCorrect.$feedbackMark;
			
				$feedbackResults .= "</div>";
				$feedbackResults .= $leftCellAnswer;
				if($question[1]['question_type'] != 't') $feedbackResults .= $answer['aDesc']->answer;
					else $feedbackResults .= $question[1]['user_input']." <br/> <i>Keywords = ".$answer['aDesc']->keywords."</i>";//ignore user_input, incorrectly labelled
				$feedbackResults .= "</div><!-- end left cell -->";
				$feedbackResults .= "</div> <!-- End answer row -->";
				$feedbackResults .= $spaceLine;
				
			}
			//Percentage per question
				$questionPercent = ($c['percentages'][$i]*100);
				$feedbackResults .= $rowDivAnswer."<strong>".$questionPercent."%</strong>  </div> <!-- End % answer row -->";
				$feedbackResults .= $spaceLine;
	
			$i++;		
		}
		
		$feedbackResults .="</div> <!-- end results list -->";
		$feedbackResults .= "<h1 class='quizGive'>Result</h1><h2 class='quizGive'>$total%</h2><br/><br/>";
		echo $feedbackResults;
		
	}else{
		// if the quiz creator has not entered correct options, (probably a survey) hide percentage.
		echo "Thanks for taking our survey";
	}
	

		/*
	echo "<pre>";
	 print_r($c);
	echo "</pre>";
	*/
	//time for graph plugin???
	
	
	
	/*echo "<pre>";
	print_r($this->giveQuizStrings['percentages']['5']);
	echo "</pre>";*/
	/*
	$i = 1;
	$j = 0;
	$questionSwitch = ""; //Check for next question
	foreach($this->giveQuizStrings as $row){
		if(isset($row['num']) && ($i == $row['num'])){
			if($j != 0) echo $this->giveQuizStrings[$j]['totalPercent']."<br/>";			
			echo "<hr/><br/> Question # ".$i." : ".$row['question']."</br></br>";

			if($row['question_type'] == 't'){
				echo "Answer : ".$row['user_input']."<br/>"; 
			}else{			
				if(isset($row['num'])) echo "Answer # ".$row['num']." :"; 
			}

			if(isset($row['result'])) echo " <br/> Result = ".$row['result']."<br/>";	
			
			$questionSwitch = $row['question'];
			$i++;
			$j++;
		}
	}
			echo "<br/><br/><hr /><br/>";
	//echo "<br/>".$this->giveQuizStrings[$j]['totalPercent']."<br/>";
	if(isset($this->giveQuizStrings['total'])) echo "<strong>Total = ".round($this->giveQuizStrings['total'], 0)."%</strong><br/><br/>";*/
?>
<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>


<!-- end of inquisitive give -->
