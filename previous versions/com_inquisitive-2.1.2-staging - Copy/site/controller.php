<?php
/**
 * Inquisitive default controller
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$user = JFactory::getUser();
if($user->guest) {
//	JError::raiseError( 403, JText::_("ALERTNOTAUTH") );
}


jimport('joomla.application.component.controller');

/**
 * Inquisitive Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveController extends JControllerLegacy
{
	/**
	 * Method to display the view
	 *
	 * @access	public
	 */
	public function display($cachable = true, $urlparams = true)
	{		
		//XXX should permission be moved here?
		return parent::display();
	}

}

?>
