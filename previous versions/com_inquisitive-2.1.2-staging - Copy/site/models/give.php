<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

		
/**
 * Inquisitive Model for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport( 'joomla.application.component.model' );

/**
 * Inquisitive Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
 
 //Models contain Model classes. 
 //One Model class is equal to one DB table.*/
 
class InquisitiveModelGive extends JModelLegacy
{	

	//TODO replace this with the quizlist getQuizInfo [14/06/2013 Found]
	function detectSurvey($qid = NULL){
		$sql =  'SELECT `survey` FROM `#__quiz_info` WHERE timestamp = "'.$qid.'"';
		$db = JFactory::getDBO();
		$db->setQuery( $sql );
		$quizMode = $db->loadObject();

		return $quizMode;		
	}
	
	/**
	 * The quiz results are calculated here.
	 * This is done at execution and not stored in the database. 
	 * thus quiz should not be modified after publish. 
	 * 
	 *	$results = user results;
	 *
	 * return $a array (for display not db)
	 *   
	 *
	 */
 	function getProcessQuiz($newQuiz = NULL, $take = NULL, $id = NULL, $uid = NULL, $questionNumTotal = NULL){
		$i=0;//student result Index
		$total = 0;
		$correctSum = 0;
		$a = array();
		$b = array();
		$totalPercent=0; //Sum of all the questions
		$totalPerQuestion=0;
		$questionNum = 1;
		$timestamp = time();
		
		//Detect quiz/survey
		//check survey field in quiz_info table, if 0 do not display output.
		$quizMode = $this->detectSurvey($id);
		
		$surveyMode = ($quizMode->survey == '1' ? true : false);
		
		//check if reports exist, update results if true
		$reportExists = $this->reportQuizUser($id, $uid);		
		if(!empty($reportExists)) $oldTimestamp = $reportExists[0]->timestamp;

		//SUM of ANSWERS BY QUESTION
		
		//QUESTION Loop
		for($z=0; $z < $questionNumTotal; $z++){
			$answerSum = 0;
			$results = $this->getStudentResults($id, $uid, $questionNum, NULL);//user results	
			$answerWeight = $this->getAnswerWeight($questionNum, $id);	
			$answerSumByQuestion = 0;
									
			//Answer Loop
			foreach($results as $userRow){
				$j = $userRow->a; //Answer Number

				//Get the quiz source from the database
				$getQuestionID = $newQuiz->getQuizQuestionId($id, $userRow->q);
				$questionRow = $take->getQuestionRow($getQuestionID); 
				$answerRow = $take->getQuizAnswers($userRow->q, $userRow->a, $userRow->id);									

				switch ($answerRow[0]->type){
					case "c": //check box
						//echo "<strong>Match Check Box </strong>";				
						$a = $this->matchCheckBox($answerRow[0]->correct, $userRow->result, $correctSum, $questionRow->question, $questionRow->questionIndex, $answerWeight, $j);										
						break;
					case "t": //Text
						//echo "<strong>Match Text</strong>";	
						$a = $this->matchTextBox($answerRow[0]->keywords, $userRow->result, $questionRow->question, $questionRow->questionIndex, $j, $take);																													
						break;
					case "r": //Radio Button
						//echo "<strong>Match Radio Button</strong>";
						$a = $this->matchRadioButton($answerRow[0]->correct, $userRow->result, $correctSum, $questionRow->question, $questionRow->questionIndex, $totalPercent, $j);
						break;
					case NULL:
						break;
				}
							
	
				//sum the answers
				if(($a[$j]['result'] == "Correct") && ($a[$j]['decimalWeight'] != 0)){
					$answerSum += 1;// $answerSum + $a[$j]['decimalWeight'];
				}elseif($a[$j]['result'] == "Incorrect"){
					$answerSum -= 1;
				}

				//Student results to be check for tick or cross in front end
				//true = show tick for correct, false = show cross for wrong				
				$a[$j]['studentsResult'] = $this->defineResult($results[$i]->result);
				$a[$j]['creatorResult'] = $this->defineResult($answerRow[0]->correct);				
				$a[$j]['aDesc'] = $answerRow[0];
				
				$b[$questionNum][$j] = $a[$j];
				$i++; //student result index
			}//End Answer Loop

			//update the percentage array	
			if($a[$j]['question_type'] == 't'){
				$answerSumByQuestion = $a[$j]['result'];
			}else{
				if($answerSum > 0) $answerSumByQuestion = $answerSum * $answerWeight;
					else $answerSumByQuestion = 0;
			}

			$c['percentages'][$questionNum] = $answerSumByQuestion;
			$totalPerQuestion += $answerSumByQuestion;
			
			//Insert or update result for reporting
			if(empty($reportExists)) $this->insertReportQuestion($id, $uid, $answerSumByQuestion, $timestamp, $getQuestionID);
				else $this->updateReportQuestion($id, $uid, $answerSumByQuestion, $oldTimestamp, $timestamp, $getQuestionID);

		
			$questionNum++;
			$i = 0;
		}//End Question Loop
		
		$questionNumWeight = (1 / $questionNumTotal);
		$totalSum = $questionNumWeight * $totalPerQuestion;

		//Insert or update result for reporting
		if(empty($reportExists)) $this->insertReportQuiz($id, $uid, $totalSum, $timestamp);
				else $this->updateReportQuiz($id, $uid, $totalSum, $oldTimestamp, $timestamp);
		
		$c['displayResults'] = $b;
		$c['percentages']['total'] = $totalSum;
		$c['survey'] = $surveyMode;
				
		return $c;	
	}
	
	//Get row from quiz report table
	function reportQuizUser($quizID, $userID){
		$sql =  'SELECT * FROM `#__quiz_report_quiz` WHERE quiz_id = "'.$quizID.'" AND user_id = "'.$userID.'"';
		$db = JFactory::getDBO();
		$db->setQuery( $sql );
		$report = $db->loadObjectList();
		
		if($db->getErrorMsg()){
			$status = "<br />".$db->getErrorMsg();
			return $status;
		}
		
		return $report;
	}
	
	
	/**
	 *	Update users data for reporting
	 *
	 */

	function updateReportQuestion($quizID, $userID, $userScore, $oldTimestamp, $newTimestamp, $questionID){
		$db = JFactory::getDBO();
		$query = "UPDATE #__quiz_report_question SET user_score = '".$userScore."', timestamp = '".$newTimestamp."' WHERE quiz_id = '".$quizID."' AND question_id = '".$questionID."' AND user_id = '".$userID."' AND timestamp = '".$oldTimestamp."';";
		$db->setQuery( $query );
		$db->query();

		if($db->getErrorMsg()){
			$status = "<br />".$db->getErrorMsg();
			return $status;
		}
		
		return $query;		
	}
	
	/**
	 *	Insert users total question score for reporting
	 *
	 */

	function insertReportQuestion($quizID, $userID, $userScore, $timestamp, $questionID){
		$attributes = "(quiz_id, user_id, user_score, timestamp, question_id)";	
		$db = JFactory::getDBO();
		$query =  "INSERT INTO #__quiz_report_question ".$attributes." VALUES ('".$quizID."', '".$userID."', '".$userScore."', '".$timestamp."', '".$questionID."');";
		$db->setQuery( $query );
		$db->query();

		//echo "<br />insertResult - ".$query;
		if($db->getErrorMsg()) $db->getErrorMsg();
		
		return $userScore;		
	}
	
	/**
	 *	Update users total quiz score for reporting
	 *
	 */
	function updateReportQuiz($quizID, $userID, $userScore, $oldTimestamp, $newTimestamp){
		$db = JFactory::getDBO();
		$query = "UPDATE #__quiz_report_quiz SET user_score = '".$userScore."', timestamp = '".$newTimestamp."' WHERE quiz_id = '".$quizID."' AND user_id = '".$userID."' AND timestamp = '".$oldTimestamp."';";
		$db->setQuery( $query );
		$db->query();

		if($db->getErrorMsg()) $db->getErrorMsg();
		
		return true;		
	}

	
	/**
	 *	Insert users total quiz score for reporting
	 *
	 */
	function insertReportQuiz($quizID, $userID, $userScore, $timestamp){
		$attributes = "(quiz_id, user_id, user_score, timestamp)";	
		$db = JFactory::getDBO();
		$query =  "INSERT INTO #__quiz_report_quiz ".$attributes." VALUES ('".$quizID."', '".$userID."', '".$userScore."', '".$timestamp."');";
		$db->setQuery( $query );
		$db->query();

		//echo "<br />insertResult - ".$query;
		if($db->getErrorMsg()) $db->getErrorMsg();
		
		return $userScore;
	}

	
	/**
	 * Retrieve student results from result table
	 *   return array to display
	 *   Question can be NULL to return more
	 *   Answer can be NULL to return more	 
	 */
	function getStudentResults($id, $user, $q, $a){
		if($q != NULL) $qr = ' AND q= "'.$q.'"';
			else $qr = "";
		if($a != NULL) $ar = ' AND a= "'.$a.'"';
			else $ar = "";

		$sqlquizR =  'SELECT * FROM `#__quiz_results` WHERE id = "'.$id.'" AND student_name= "'.$user.'"'.$qr.$ar.' ORDER BY `index`';
		
		$db = JFactory::getDBO();
		$db->setQuery( $sqlquizR );
		$sqlquiz = $db->loadObjectList();
		
		/*echo "getStudentResults - <PRE>";
			print_r($sqlquiz);
		echo "</PRE>$sqlquizR";*/

		if($db->getErrorMsg()){
			$status = "<br />A-".$sqlquiz."<br />".$db->getErrorMsg();
			return $status;
		}
		return $sqlquiz;//." timestamp(id)= ".$timestamp." SQL =".$sqlquiz;				
	}											

	/**
	 * Match user response with keywords
	 * returns the percentage of (each keyword found / amount of keywords )
	 * returns NULL if no keywords found for question
	 * FIXME broken 24/10 (radio and check box AOK!)
	 */
	function matchWords($keywords, $answer){
		$i = 0;
		$j = 0;		
		$totalMatches = 0;
		$nokeywords = 0; //if set to 1
		$result = NULL; //if result remains null, indicates no accuracy required

		//seperate keywords
  		$keywordsArray = explode(",", $keywords);
		
		//Match keywords with users input
		foreach($keywordsArray as $row){
			$j++;
			//echo "row =".$row."<br/>";
			if($row != NULL){
				if($row !="Seperate keywords with commas."){
					$row2 = trim($row);				//Remove breaks from textarea inputs
					$answer2 = preg_replace('/[,\r\n]/',' ',$answer);
					$matches = preg_match("/\\b".$row2."\\b/si", $answer2, $out);
					/*echo "<br />".$i." <b>row =</b>'".nl2br($row)."'<br /> matches? ".$matches."<br/><b>answer</b>:".nl2br($answer2)."<br />";
				print_r($out);
				echo "<br />";*/			
					$totalMatches += $matches;
					$i++;
				}else{
					$nokeywords++;
					//echo "<strong>non</strong> ".$nokeywords."<br/>";
				}
			}
		}
		
		if($nokeywords == $j) {
			//echo " res".$result." j".$j."<br/>";
			return NULL;
		}
		//echo "<br /> totalMatches = ".$totalMatches;
		if($totalMatches != 0) $result = $totalMatches / $i; //return accuracy per matches
			else $result = "false"; //return 0 score
			
		return $result;
	}
	
	function matchRadioButton($correct, $result, $correctSum, $questionDesc, $questionNum, $totalPercent, $answerNum){			
		$decimalWeight = NULL;
		
		/*
		 * TODO might need to find which answer is incorrect.
		 * if(($correct == "on")){
			$decimalWeight = 1;
			$result = "Incorrect";		
		}*/
					
		if(($correct == "on") && ($result == "on")){
			$decimalWeight = 1;
			$result = "Correct";
		}
		
		$a[$answerNum] = array(
			"question" => $questionDesc,
			"q" => $questionNum,
			"a" => $answerNum,									
			"result" => $result,
			"question_type" => "r",
			"decimalWeight" => $decimalWeight
		);
		
		return $a;
	}
		
	function matchCheckBox($correct, $result, $correctSum, $questionDesc, $questionNum, $decimalWeight, $answerNum){
		if($correct == NULL) $decimalWeight = 0;
		if($correct != NULL) $correct = "on"; //$answerRow[$userRow->q]->correct
			else $correct = "off";
							
		if($correct == $result){
			//$actualResult = 1; //Match
			$result = "Correct";
			//$correctSum++;
		}else{
			//$actualResult = -1; //mistake - SUBTRACT FROM TOTAL.		
			$result = "Incorrect";
		}

/*		$subTotalPercent = $percent * $actualResult;	
		$totalPercent += $subTotalPercent;*/
											
		//echo "<br/> answer weight total = ".$answerWeight['total']." i = ".$i." sub total percent >= 1 = (".$subTotalPercent.") totalPercent = ".$totalPercent."<br/>";
												
						//echo "<i>Answer Correct =".$aCorrect."</i> <strong> actualResult = ".$actualResult."</strong> weight = ".round($percent, 2)."% Total =".round($subTotalPercent, 0)."</strong><br/>( Answer : Weight = ".$answerWeight['weight']." Total =".$answerWeight['total']." )<br/><br/><hr />";
						
						//$resultsOut .=  "<br /><strong> weight = ".$subTotalPercent."% </strong><br/><br/><hr />";
		$a[$answerNum] = array(
			"question" => $questionDesc,
			"q" => $questionNum,
			"a" => $answerNum,
			"result" => $result,
			"question_type" => "c",
			"decimalWeight" => $decimalWeight
		);		
										
/*		$a[$answerNum]['accuracy'] = array(
			"decimalWeight" => $decimalWeight,	
	//		"totalPercent" => $totalPercent, 				

		);*/
		
		return $a;		
	}
	
	function matchTextBox($keywords, $result, $questionDesc, $questionNum, $answerNum){			
		$subTotalResult = $this->matchWords($keywords, $result); //Trigger the correct or incorrect switch.
		$keywordsArray = explode(",", $keywords);
		$weight = (1/count($keywordsArray));

		/*if($subTotalResult != NULL) $correctSum = "Correct";
														
		if($subTotalResult == "false") $totalPercent = "0";									
			else $totalPercent = $subTotalResult;*/
	
		$a[$answerNum] = array(								
					"question" => $questionDesc,
					"q" => $questionNum, 
					"a" => $answerNum,
					"result" => $subTotalResult,
					"question_type" => "t",
					"decimalWeight" => $weight, 
					"user_input" => $result,
					"keywords" => $keywords
		);
		
		return $a;
	}
	
	//Marking student vs creator
	/*function marking($student, $creator){
		if(($student == $creator) && ($student == 1)) $a = true;
		
		//if(($student == $creator) && ($student == 0)) $a = false;
		
		if(($student != $creator) && ($student == NULL)) $a = true
	}*/
	
	//Switch text to create consistency when marking
	//return True, False and null
	function defineResult($result){
				switch ($result){
					case "off": //check box or radio button				
						$a = false;										
						break;
					case "Incorrect": //check box or radio button
						$a = false;
						break;						
					case NULL: //Text	
						$a = false;																													
						break;
					case "on": //check box or radio button
						$a = true;
						break;
					case "Correct": //check box or radio button
						$a = true;
						break;	
					default:
						$a = NULL;
				}
		return $a;		
	}

	//count the sum of correct answers:						
	//return Weight for each question per Radio or checkbox answer.
	function getAnswerWeight($questionNum, $quizId){
		$correctWeight = 0;
		$sqlCountCorrect =  'SELECT COUNT(correct) as `correctCount` FROM `#__quiz_answer` WHERE question_num = "'.$questionNum.'" AND timestamp = "'.$quizId.'" AND
		 correct = "on"';		
//		$sqlCountTotal =  'SELECT COUNT(*) as `totalCount` FROM `#__quiz_answer` WHERE question_num = "'.$questionNum.'" AND timestamp = "'.$quizId.'"';
		$db = JFactory::getDBO();
		$db->setQuery( $sqlCountCorrect );
		$countCorrect = $db->loadObject();
	/*	$db->setQuery( $sqlCountTotal );
		$sqlCountTotalList = $db->loadObjectList();*/
		
		/*echo "<PRE>".$sqlCountCorrect;
			print_r($sqlCountCorrectList);
			print_r($sqlCountTotalList);
		echo "</PRE>";*/
		
		//print_r($countCorrect);
		
		if($countCorrect->correctCount != 0) $weight = (1 / $countCorrect->correctCount);
			else $weight = 0;
		
		/*	
		if($correctWeight == 1) $decimalWeight = 1;
			elseif( $correctWeight == $sqlCountTotalList[0]->totalCount ) $decimalWeight = 1 / $correctWeight;
				else $decimalWeight = $correctWeight / $sqlCountTotalList[0]->totalCount;
		
		$weight = array( 
					'decimalWeight' => $decimalWeight,
					'weight' => $correctWeight,
				 	'total' => $sqlCountTotalList[0]->totalCount
		);*/

		if($db->getErrorMsg()){
			$status = "<br />getAnswerWeight-<br />".$db->getErrorMsg();
			return $status;
		}

		return $weight;
	}	
}