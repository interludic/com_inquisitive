<?php
/**
 * @version       $Id$
 * @copyright     Copyright (C) 2007 - 2014 Favourites Multimedia. All rights reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @author        Harry Bosh - Inquisitive
 */
defined('_JEXEC') or die;


// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
if(version_compare(JVERSION,'3.0.0','ge')) {
    JHtml::_('formbehavior.chosen', 'select');
}
?>
<form action="<?php echo JRoute::_('index.php?option=com_inquisitive&layout=edit&id='); ?>" method="post" name="adminForm" id="adminForm" class="form-validate">
    <div class="row-fluid">
        <!-- Begin Content -->
        <div class="span10 form-horizontal">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#general" data-toggle="tab"><?php echo JText::_('COM_INQUISITIVE_CONFIG_NAV_WELCOME_TITLE');?></a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <div class="row-fluid">
                        <div class="span10">
                            <div class="control-group">
                                 <?php echo JText::_( 'Configuration:' ); ?>
                                <div class="controls">
                                	 <?php echo JText::_( 'The options button allows you and your users to have the appropriate permissions while using Inquisitive. <strong>Permission MUST BE granted</strong> to the creator and user appropriatley(Button is located above top left), Otherwise you will not be able to access Inquisitive. Click the help button above, this links to the inquisitive.net.au support & documentation page.' ); ?>

                                </div>
                            </div>
                            <div class="control-group">
                                <?php echo JText::_( 'Feature Request:' ); ?>
                                <div class="controls">
                                    <?php echo JText::_( 'Feel free to send feature requests to the help link. Features added to inquisitive benefit everyone.' ); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <?php echo JText::_( 'Updates:' ); ?>
                                <div class="controls">
                                    <?php echo JText::_( 'Inquisitive is constantly being updated, this then gets sent to your site automatically.' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
		    </div>
<div class="clr"></div>
