<?php

/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
require_once JPATH_COMPONENT.'/helpers/UploadHandler.php';

/**
 * Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerAjax extends JControllerLegacy
{
	/**
	 * Methods
	 *
	 * @access	public
	 */
	 

	function display($cachable = false, $urlparams = false){
		$document	= JFactory::getDocument();
		$vName		= 'ajax';
		$vFormat	= 'raw';

		// Get and render the view.
		if ($view = $this->getView($vName, $vFormat)) {
			// Get the model for the view.
			$model = $this->getModel($vName);	
			// Push the model into the view (as default).
			$view->setModel($model, true);
			// Push document object into the view.
			$view->assignRef('document', $document);
			$view->display();
		}

	}


	function deleteQuiz(){	
		//security - preventing injection.															
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$document	= JFactory::getDocument();
			$quizID = JRequest::getVar('quizid');
			$model = $this->getModel('quizlist');
			//$response = "trashed".$quizID." and model = ".$model;
			$response = $model->trashThis($quizID);		
			
			$vName		= 'ajax';
			$vFormat	= 'raw';
			if ($view = $this->getView($vName, $vFormat)) {
				$view->setModel($model, true);
				$view->assignRef('response', $response);
				$view->assignRef('document', $document);
				$view->display();
			}
		}
	}

	function deleteQuestion(){			
		//security - preventing injection.															
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$document	= JFactory::getDocument();
			$questionID = JRequest::getVar('questionID');
			$ID = JRequest::getVar('ID');
			$model = $this->getModel('newquiz');			
			$response = $model->trashThisQuestion($questionID, $ID);		
			//$response = "Delete a question".$questionID." model = ".$model;
			
			$vName		= 'ajax';
			$vFormat	= 'raw';
			if ($view = $this->getView($vName, $vFormat)) {
				$view->setModel($model, true);
				$view->assignRef('response', $response);
				$view->assignRef('document', $document);
				$view->display();
			}
		}
	}

	
	function replicateQuiz(){	
		//security - preventing injection.															
		if (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive')){
			$document	= JFactory::getDocument();
			$quizID = JRequest::getVar('quizid');
			$model = $this->getModel('newquiz');
			$response = $model->replicateQuiz($quizID);		
			
			$vName		= 'ajax';
			$vFormat	= 'raw';
			if ($view = $this->getView($vName, $vFormat)) {
				$view->setModel($model, true);
				$view->assignRef('response', $response);
				$view->assignRef('document', $document);
				$view->display();
			}
		}
	}

}

?>
