<?php

/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
require_once JPATH_COMPONENT.'/helpers/UploadHandler.php';

/**
 * Component Controller
 *
 * @package		Inquisitive
 */
class InquisitiveControllerUploadHandler extends JControllerLegacy
{
	/**
	 * Methods
	 *
	 * @access	public
	 */
	 

	function display($cachable = false, $urlparams = false){
		$document	= JFactory::getDocument();
		$vName		= 'uploadhandler';
		$vFormat	= 'raw';

		// Get and render the view.
		if ($view = $this->getView($vName, $vFormat)) {
			// Get the model for the view.
			//$model = $this->getModel($vName);	
			// Push the model into the view (as default).
			//$view->setModel($model, true);
			// Push document object into the view.
			//$view->assignRef('format', 'raw');
			$view->assignRef('document', $document);
			$view->display();
		}
		

	}



}

?>
