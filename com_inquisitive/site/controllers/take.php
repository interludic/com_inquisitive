<?php
/**
 * Inquisitive entry point file for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );

class InquisitiveControllerTake extends JControllerLegacy{
	 
	function display($cachable = false, $urlparams = false){
				
		parent::display();
	}
	
	function sitQuiz(){
		if ((JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) || (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive'))){
			JRequest::setVar ( 'view', 'take' );		

			//Get the layout for this quiz
			$quizList = $this->getModel('quizlist');			
			$quizInfo = $quizList->getQuizInfo($_GET['ID']);
			$quizParams = json_decode($quizInfo->params);
			$layoutOptions = $quizList->getLayouts();			
//			echo "<br/>decode = ".$quizParams->layout." options = ".$layoutOptions[$quizParams->layout]['title'];
			
			$layout = @$layoutOptions[$quizParams->layout]['title'];

			JRequest::setVar ( 'layout', $layout);

			$model = $this->getModel('take');		
			$getQuizQuestions = $model->getQuizQuestions(NULL, NULL, NULL);
		/*	$questionArrayIDnums = $model->questionArrayIDnums(NULL);

			JRequest::setVar ( 'questionArrayIDnums', $questionArrayIDnums );*/
			//print_r($getQuizQuestions);
			JRequest::setVar ( 'rawQuizQuestions', $getQuizQuestions );
			//TODO consider ACL in the view		
			JRequest::setVar ( 'usertype', 'Guest');			
			parent::display();	
		
		}else{
			//FIXME this link needs to be redirected properly after login. 
			//FIXME XXX Consider adding to control panel					
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;			
			$this->setRedirect($link);						
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
		
	}

	function publish() {
		if ((JFactory::getUser()->authorise('inquisitive.read', 'com_inquisitive')) || (JFactory::getUser()->authorise('inquisitive.write', 'com_inquisitive'))){		
			JRequest::setVar ( 'view', 'results' );		
			JRequest::setVar ( 'layout', 'default' );
			$take = $this->getModel('take');
			$give = $this->getModel('give');		
			$quizList = $this->getModel('quizList');		
			$quizInfo = $quizList->getQuizInfo($_POST['ID']);			
			$quizParams = json_decode($quizInfo->params);

			//delete previous quiz attempt...
			$take->deleteQuizAttempt($_POST['username'], $_POST['ID']);
	
			//insert & format the quiz results
			$quizResultFormatted = $take->quizResult();			
			$notification = $take->notifyQuizCreatorEmail($_POST['username'], $_POST['ID'], $quizResultFormatted, $give);
			// Get default menu - JMenu object, look at JMenu api docs

//want ITEMID??
// $menu = JFactory::getApplication()->getMenu();
// // Get menu items - array with menu items
// $items = $menu->getMenu();
// // Look through the menu structure, once you understand it
// // do a loop and find the link that you need.
// echo "<pre>";
// 	print_r($items);
// echo "</pre>";
// die();

			if($quizParams->article != NULL) $link = "index.php?option=com_content&view=article&id=".$quizParams->article;
				else $link = 'index.php?option=com_inquisitive&controller=give&uid='.$_POST['username'].'&qid='.$_POST['ID'].'&Itemid='.$_POST['Itemid'];

			//processing the quiz checking results.. comment redirect to see results
/* DEBUG */	$this->setRedirect($link);
			if($notification['mailStatus'] == 1) JFactory::getApplication()->enqueueMessage('Email sent to creator');
				elseif($notification['mailStatus'] != NULL) JError::raiseWarning( 100, 'Warning: '.$publishStatus['mailStatus'] );
			
			// echo "<br/>notification: ".$notification['mailStatus']."<br/> ";
			// print_r($notification);

			// <pre><br/>Post = ";
			//print_r($_POST);
			/*echo "<br/>QuizResults = ";
			//print_r($quizResultFormatted);
			echo "</pre>";*/
			//echo "<a href=".$link."> Proceed </a>";
			//parent::display();
		}else{
			//FIXME this link needs to be redirected properly after login. 
			//FIXME XXX Consider adding to control panel					
			$link = "index.php?option=com_users";//".JRequest::getVar('option')."&controller=newquiz&task=edit&ID=".$id."&question_num=".$question_num."&question_index=".$question_index."&previous=".$previous."&editcheck=".$edit_check;			
			$this->setRedirect($link);						
			//JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;				
		}
	}
}

?>
