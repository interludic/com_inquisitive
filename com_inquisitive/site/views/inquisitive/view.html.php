<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access'); 

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Inquisitive Component
 *
 * @package		Inquisitive
 * @subpackage	Components
 */
 
 //View folder contains view classes and templates. Every view class may have few templates that are stored in tmpl folder. Every view class has the same name view.html.php. And tmpl folder contains html template files. 
 
class InquisitiveViewInquisitive extends JViewLegacy
{
	function display($tpl = null){
		
		parent::display($tpl);
	}
	
}
?>
