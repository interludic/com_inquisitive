<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');

 
class InquisitiveViewTake extends JViewLegacy
{
	
	function display($tpl = null)
	{			

		if(JRequest::getVar('ID') != NULL){
			
			$ID = JRequest::getVar('ID');			
			$user = JFactory::getUser();
			$take = $this->getModel('take');					
			$componentName = JRequest::getVar('option');
			$rawQuestions = JRequest::getVar('rawQuizQuestions');			
			$questionArrayIDnums = JRequest::getVar('questionArrayIDnums');
			$quizAnswers = $this->get( 'QuizAnswers' );

			//function to output location URL
/*			$outputURLocator = $this->get( 'OutputURL' );
			$this->assignRef('quizQuestions', $outputURLocator);*/

			
			$this->assignRef('questionArrayIDnums', $questionArrayIDnums);						
			$this->assignRef('quizAnswers', $quizAnswers);						
			$this->assignRef('username', $user->username);
			$this->assignRef('userID', $user->id);			
			$this->assignRef('ID', $ID);						
			$this->assignRef('rawQuestions', $rawQuestions);		
			$this->assignRef('comName', $componentName);			
		}
		
		parent::display($tpl);
	}

}
?>
