<?php // no direct access

defined('_JEXEC') or die('Restricted access'); 
$checkBoxCount = 0;      
$checkBoxFlag = 0;
$itemid = JRequest::getint( 'Itemid' );

$styleCss = "components/".$this->comName."/views/tmpl/style.css";
$carouselCss = "components/".$this->comName."/views/tmpl/carousel.css";
$carouselJs = "components/".$this->comName."/views/tmpl/js/carousel.js";
$document = JFactory::getDocument();
$document->addStyleSheet($styleCss);
//$document->addStyleSheet($carouselCss);
//$document->addScript($carouselJs);

?>

<!-- carouselJs must be loaded later -->
<script src="<?php echo $carouselJs; ?>" type="text/javascript"></script>
<script type="text/javascript">
function submitbutton(pressbutton){
  switch (pressbutton) {
    case 'cancel':
//TODO CONFIRM THIS ROUTING IS WORKING
document.takeQuiz.action = "<?php echo JRoute::_("index.php?option=".$this->comName."&controller=quizlist&Itemid=".$itemid); ?>";
document.takeQuiz.submit();
return;
break;

case 'publish':
    /* var additionalEmail = document.getElementById('additionalEmail');
    if(additionalEmail.value == 'Email a friend') additionalEmail.value = ""; */
    document.getElementById('loading').style.visibility='visible'; 
    //Itemid is being posted below.
    document.takeQuiz.action = "<?php echo JRoute::_("index.php?option=".$this->comName."&controller=take&task=publish&view=take"); ?>";
    document.takeQuiz.submit();
    return;
    break;        

  }
}

function inputTextbox(contents){
  var inputText = document.getElementById('additionalEmail'); 
  if(inputText.value == "") inputText.value = "Email a friend";
  else if(inputText.value == "Email a friend") keywords.value = "";
}

//fix for mootools conflicting with bootstrap
window.addEvent('domready', function(){
if (typeof jQuery != 'undefined' && typeof MooTools != 'undefined' ) {

Element.implement({
slide: function(how, mode){
return this;
}
});
}
 });
</script>

<style>

  .carousel-caption{
      color:#000;
      position:static;
      padding-left: 30px;
      padding-right: 30px;
      background: none;
      text-shadow: none;
  }

  .carousel-indicators{
    bottom:-20px;
  }

  .carousel-control {
    min-width:40px;
    background-color: rgba(0,0,0,.5);

    border: none;
    border-radius: none;
    color: none;
    font-size: none;
    font-weight: none;
    height: none;
    left: none;
    line-height: none;
    margin-top: none;
    opacity: none;
    position: none;
    text-align: none;
    top: none;
    width: none;
}

  }

  .carousel-control.right, .carousel-control.left{
    background-image: none;
  }
</style>

<form name="takeQuiz" id="fileupload" action="<?php //echo $fileUploadUrl2; ?>" method="POST" enctype="multipart/form-data" onsubmit="return validate_form(this)">  

<div id="carousel-layout" class="carousel slide" data-ride="carousel" data-interval="false">
  <!-- Indicators -->
<!--   <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol> -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

  <?php 
  $i = 0; //count total questions, pass to results as total answers.
  //QUESTIONS LOOP
  foreach ( $this->rawQuestions as $rowQ ) {
    $i++;

    //carousel setup
    if($i==1)  $active = "active"; 
      else $active = NULL;
  ?>

    <div class="item <?php echo $active; ?>">
      <div class="carousel-caption">

        <?php echo $rowQ->location; ?>
        <div class="table-row-question">
          <div class="headingLabel">
            <?php echo JText::_('COM_INQUISITIVE_QUESTION'); ?>       
            <div class="numText" ID="questionNum"><?php echo $rowQ->num; ?></div>
          </div><!-- heading label end -->
          <input type="hidden" value="<?php echo $rowQ->questionIndex; ?>" name="QQ<?php echo $rowQ->num; ?>" />
          <div class="text-question "> <?php echo $rowQ->question; ?> </div><!-- text question end -->
          <input type="hidden" value="<?php echo $rowQ->question; ?>" name="questionQQ<?php echo $rowQ->num; ?>" />
          <div class="space-line"></div> <!-- space line end -->
        </div> <!-- question end -->

        <?php
        foreach ( $this->quizAnswers as $rowA ) {
          //Convert id to number  
          if($rowQ -> num == $rowA->question_num){      
          //match question and answer.                  
            if(($rowA -> type == 'c') && ($checkBoxFlag != 1)) $checkBoxCount++; 
            ?>
            <div class="table-row-answer">
              <div class="headingLabel">
                <?php echo JText::_('COM_INQUISITIVE_ANSWER'); ?>
                <p class="numText" ID="answerNum" <?php if(($rowA->type == "t") || ($rowA->type == "u")) echo "style='display: none;'"; ?>><?php  echo $rowA->num; ?></p>
              </div><!-- heading label end -->
              <div class="left-layer11"></div><!-- left layer end -->
              <?php 
              echo "<!-- Question type = ".$rowA->type." -->";          
              //file upload buttons
              if($rowA->type == "u"){             
                $questionIndex=$rowQ->questionIndex;
                echo "<input type='hidden' value='".$rowQ->questionIndex."' name='questionIndexFromQuestionNum".$rowQ->num."' />";

                require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'questionUploadButtons.php');
              }elseif($rowA->type == "t"){  ?>
                <textarea cols="40" rows="5" name="<?php echo "Q".$rowA->question_num."[".$rowA->num."]"; ?>" tabindex="<?php  echo $rowA->question_num.$rowA->num; ?>"></textarea>
              <?php } else { 
                echo $rowA->answer; 
              }

              if($rowA->type == "r"){ ?>
              <div class="right-layer11 dashed"><!-- right layer end -->
                <div class="rowMultiChoice">
                  <input type="radio" name="<?php echo "Q".$rowA->question_num; ?>" value="<?php echo $rowA->num; ?>" tabindex="<?php  echo $rowA->question_num.$rowA->num; ?>"/>
                </div><!-- row multi choice end -->
              </div> <!-- end dashed -->                        
              <div class="space-line"></div><!-- space line end -->
              <?php   
              }elseif($rowA->type == "c"){            
                $checkBoxFlag = 1; ?>
                <div class="right-layer11 dashed">
                  <div class="rowMultiChoice">
                    <input type="checkbox" name="<?php echo "Q".$rowA->question_num."[".$rowA->num."]"; ?>" tabindex="<?php  echo $rowA->question_num.$rowA->num; ?>"/>
                  </div> <!-- row multi choice end -->                              
                </div> <!-- end dashed -->
                <?php } ?>
                <div class="space-line"></div><!-- space line end -->
              </div><!-- table row answer end -->            
              <?php
            } //match question and answer.                 
             //if total answers completed? what does it do?
            $totalQuestionNumPerAnswer = $rowA->question_num; 
            $totalQuestionNumPerAnswerNumber = $rowA->num;
            ?>
            <input type="hidden" name="totalAnswers<?php echo $totalQuestionNumPerAnswer; ?>" value="<?php  echo $totalQuestionNumPerAnswerNumber; ?>" /> 
            <?php
          } //END ANSWER LOOP  
          $checkBoxFlag = 0;//Prep for next question
          ?>
      
      </div>
    </div>

  <?php    
  } //END QUESTION LOOP ?>   

    
  </div>

    <div class="space-line"></div>
    <div class="table-row-footer"><?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'takeFooterButtons.php');?></div>
  
</div>


    <input type="hidden" value="<?php echo $this->ID; ?>" name="ID" />
    <input type="hidden" value="<?php echo $this->userID; ?>" name="userID" />
    <input type="hidden" value="<?php echo $i; ?>" name="questionTotal" />
    <input type="hidden" value="<?php echo $this->username; ?>" name="username" />
    <input type="hidden" value="<?php echo $itemid; ?>" name="Itemid" />
  </form>
<!-- end of inquisitive take -->