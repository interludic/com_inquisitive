<!-- Begin NewQuiz Question Footer Buttons -->
<script type="text/javascript">
<?php
	if($this->editcheck == "read"){
		//echo "$('#questionElements :input').attr('disabled', true);"; 
		$disable_element = "disabled=disabled";		
	} 
?>
</script>

<div class="table-row-footer">
<a name="footer-inquisitive"></a>
<?php

//ADD ANSWER
 if(JRequest::getVar('view') != "newquiz"){
	if((strtolower(JRequest::getVar('view')) != "questiontext") && (strtolower(JRequest::getVar('view')) != "questionupload")) {
		if($this->editcheck != "read"){?>        
			<div class="buttons-footer text-btn" id="add-element"><button  type="button" onclick="javascript:add(answerSwitch);"><?php echo JText::_('COM_INQUISITIVE_QUESTION_BUTTON_ADD_ANSWER'); ?></button></div>
    	    <div class="buttons-footer text-btn" id="rem-element"><button  type="button" onclick="javascript:removeAnswer();"><?php echo JText::_('COM_INQUISITIVE_QUESTION_BUTTON_REMOVE_ANSWER'); ?></button></div>
 <?php  } 
	}
 }//new quiz view
 if($this->question_num == 1){
	$disable_previous_div = "textdisabled";
	$disable_previous_btn = "disabled";
 }else{
	$disable_previous_div = NULL;
	$disable_previous_btn = NULL;
 }
?>
	<div class="buttons-footer text-btn <?php echo $disable_previous_div; ?>">
    	<button type="button" <?php echo $disable_previous_btn; ?> onclick="submitbutton('prevQuestion')"><?php echo JText::_('COM_INQUISITIVE_QUESTION_BUTTON_PREV') ?></button></div>
    
	<div class="buttons-footer text-btn"><button type="button" <?php if((JRequest::getVar('view') == 'newquiz') || ($this->last_question == true)) echo "disabled='disabled'"; ?> onclick="submitbutton('nextQuestion')" id="next"><?php echo JText::_('COM_INQUISITIVE_QUESTION_BUTTON_NEXT') ?></button></div>

<?php 
//SUBMIT BUTTON
if($this->editcheck != "read"){ ?>
	<div id="submitSaveParent" class="buttons-footer text-btn">
            <input type="button"  name="Submit" value="Submit" onclick="submitbutton('publish');"/> <!-- id="submitsave" -->
    </div>
<?php } ?>

	<div class="buttons-footer text-btn"><button type="button" onclick="submitbutton('cancel')"><?php echo JText::_('COM_INQUISITIVE_BUTTON_CANCEL') ?></button></div>
	<div class="space-line"></div>

<!-- this needs to exists after the body elements have loaded -->
<script type="text/javascript" src="components/<?php echo $this->comName; ?>/views/tmpl/js/javascript.js"></script>

<script type="text/javascript"> <?php
	if($aSwitch != "t"){//Start view check ?>
		var answerSwitch = "<?php echo $aSwitch; ?>";	
		var i = 0;



	<?php 
		//create javascript answers //swap array from php to js
		$j = $this->answercount;
		for($i = 0; $i < $j; $i++){	?>
			//alert("i  <?php echo $i; ?>  j  <?php echo $j; ?>");			
			var answer_label ="<?php if(isset($this->answer_array[$i]->answer)) echo str_replace(array("\r\n", "\r", "\n"), '\n', $this->answer_array[$i]->answer); ?>"; // json_encode(
			var answer_correct = "<?php echo $this->answer_array[$i]->correct; ?>";	
			var disable_element = "<?php if(isset($disable_element)) echo $disable_element; ?>";
			if(i != 0) add(answerSwitch, answer_label, answer_correct, disable_element); //json_encode(
			answer_label = null;
			answer_correct = null;			
			i++;
	<?php } ?>
<?php }//End view check ?>
</script>
<!-- End NewQuiz Question Footer Buttons -->