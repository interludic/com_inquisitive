<?php if(JRequest::getVar('layout') == 'carousel'){ ?>

	<!-- Carousel Controls -->
	<div class="buttons-footer">
		<a class="text-btn" href="#carousel-layout" role="button" data-slide="prev">
	    <?php echo JText::_('COM_INQUISITIVE_QUESTION_BUTTON_PREV') ?>
	    <!-- <span class="sr-only">Previous</span> -->
		</a>
	</div>

	<div class="buttons-footer">
		<a class="text-btn" href="#carousel-layout" role="button" data-slide="next">
	    <?php echo JText::_('COM_INQUISITIVE_QUESTION_BUTTON_NEXT') ?>
	    <!-- <span class="sr-only">Next</span> -->
		</a>
	</div>

<?php } ?>


<div class="buttons-footer text-btn">
	<button type="button" onclick="submitbutton('publish')"><?php echo JText::_('COM_INQUISITIVE_BUTTON_SUBMIT') ?></button>
</div>
<div class="buttons-footer text-btn">
	<button type="button" onclick="submitbutton('cancel')"><?php echo JText::_('COM_INQUISITIVE_BUTTON_CANCEL') ?></button>
</div>
	<!-- <div class="buttons-footer">
	  <p class="text">	 //TODO the user may want to keep/email results?   	<input id="additionalEmail" type="text" value="Email a friend" size="28"></input>
</p></div> -->
<div id="loading" class="buttons-footer" style="vertical-align: middle; visibility:hidden;"><?php echo JText::_('COM_INQUISITIVE_MESG_SAVING') ?>  
	<img src="components/<?php echo $this->comName; ?>/views/tmpl/images/3MA_processingbar.gif" alt="Please wait" height="15" width="145" />
</div>
<div class="space-line"></div>