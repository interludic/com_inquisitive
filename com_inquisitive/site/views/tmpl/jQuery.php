<?php
	//fix for mixed mode http/https
	$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/')));
	//Check for jquery
	JLoader::import( 'joomla.version' );
	$version = new JVersion();
	if (version_compare( $version->RELEASE, '2.5', '<=')) {
		//JHTML::_('behavior.mootools'); // this code make sure that mootools loads first
		if(JFactory::getApplication()->get('jquery') !== true) {
			// load jQuery here
			JFactory::getApplication()->set('jquery', true);
			?>
			<script src="<?php echo $protocol; ?>://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script> 
		<?php
		}
	} else {
		//JHtml::_('behavior.framework');// this code make sure that mootools loads first
		JHtml::_('jquery.framework');
	}
?>