<?php // no direct access
/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */


defined('_JEXEC') or die('Restricted access'); 

$user = JFactory::getUser();

if(!$user->guest) {
	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);
?>

<script type="text/javascript" src="components/<?php echo $this->comName; ?>/views/tmpl/js/javascript.js"></script>

<?php $link = "option=".$this->comName."&ID=".$this->ID."&question_num=".$this->question_num."&quizname=".$this->quizname; ?>

<h2><?php echo JText::_('COM_INQUISITIVE_CREATE_QUIZ_LABEL'); ?></h2>


<div class="createQuiz">
			
	<div class="one-third column">
		<a href="index.php?view=questionsingle&<?php echo $link; ?>"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/single.jpg" class="scale-with-grid" alt="<?php echo JText::_('COM_INQUISITIVE_CREATE_SINGLE_IMG_ALT'); ?>" /></a>
		<p class="quiz-title"><a href="index.php?view=questionsingle&<?php echo $link; ?>"><?php echo JText::_('COM_INQUISITIVE_CREATE_SINGLE_TITLE'); ?></a></p>
		<p class="quiz-desc"><?php echo JText::_('COM_INQUISITIVE_CREATE_SINGLE_DESC'); ?></p>
	</div> <!-- end one-third column -->

	<div class="one-third column">
		<a href="index.php?view=questiontext&<?php echo $link; ?>"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/text.jpg" class="scale-with-grid" alt="<?php echo JText::_('COM_INQUISITIVE_CREATE_TEXT_IMG_ALT'); ?>" /></a>
		<p class="quiz-title"><a href="index.php?view=questiontext&<?php echo $link; ?>"><?php echo JText::_('COM_INQUISITIVE_CREATE_TEXT_TITLE'); ?></a></p>
		<p class="quiz-desc"><?php echo JText::_('COM_INQUISITIVE_CREATE_TEXT_DESC'); ?></p>
	</div> <!-- end one-third column -->

	<div class="one-third column">
		<a href="index.php?view=questionmulti&<?php echo $link; ?>"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/multi.jpg" class="scale-with-grid" alt="<?php echo JText::_('COM_INQUISITIVE_CREATE_MULTI_IMG_ALT'); ?>" /></a>
		<p class="quiz-title"><a href="index.php?view=questionmulti&<?php echo $link; ?>"><?php echo JText::_('COM_INQUISITIVE_CREATE_MULTI_TITLE'); ?></a></p>
		<p class="quiz-desc"><?php echo JText::_('COM_INQUISITIVE_CREATE_DESC_TITLE'); ?></p>
	</div> <!-- end one-third column -->
	
	<div class="clear"></div>	
							
</div> 
<!-- end createQuiz 1 -->

<!-- <h2><?php echo JText::_('COM_INQUISITIVE_CREATE_QUIZ_MORE'); ?></h2>
<div class="createQuiz">
	<div class="one-third column">
		<a href="index.php?view=questionupload&<?php echo $link; ?>"></a>
		<p class="quiz-title"><a href="index.php?view=questionupload&<?php echo $link; ?>"><?php echo JText::_('COM_INQUISITIVE_CREATE_UPLOAD_TITLE'); ?></a></p>
		<p class="quiz-desc"><?php echo JText::_('COM_INQUISITIVE_CREATE_UPLOAD_DESC'); ?></p>
	</div>
</div>
 -->
<!-- end createQuiz 2 -->
			
<form id="newQuestion" name="newQuestion" method="post" style="visibility: hidden;">  
   	<input type="hidden" id="quizname" value="<?php echo $this->quizname; ?>" name="quizname" />  
 	<input type="hidden" id="answerBankAmt" value="1" name="answerBankAmt" />
   	<input type="hidden" id="question_check" value="false" name="question_check" /> <!-- do not check form, none exists-->    
	<input type="hidden" value="<?php echo $this->ID; ?>" name="ID" />
	<input type="hidden" id="add-element" value="<?php echo $this->question_num; ?>" name="question_num" /> <!-- id="add-element" removes IE bug -->
</form>
	<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>

<?php
	}else{		
		echo JText::_('COM_INQUISITIVE_SESSION_EXPIRED_ERROR');
	}
?>
