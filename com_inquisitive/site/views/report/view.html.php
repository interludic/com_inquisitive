<?php

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view');

class InquisitiveViewReport extends JViewLegacy
{
	
	function display($tpl = null)
	{			
		$quizID = JRequest::getVar('quizID');
		$userID = JRequest::getVar('userID');
		$questionID = JRequest::getVar('questionID');
		$componentName = JRequest::getVar('option');		
		$getQuizInfo = JRequest::getVar('getQuizInfo');		
		$report = $this->getModel('report');	
		
		$reportUsersVersusQuiz = $report->getReportUsersVersusQuiz($quizID);
		$questionNumTotal = $report->lastQuestionNum($quizID);	
		$questionAllId = $report->getQuestionAllId($quizID);
		$getQuestionAttemps = $report->getQuestionAttemps($questionAllId[0]->questionNum);	
		$question_attempts = $getQuestionAttemps[0]->question_attempts;
		$getReportQuiz = $report->getReportQuiz($quizID);
	
		for($i=0; $i < $questionNumTotal; $i++){
			//Return total score for question ID
			//EG: SELECT sum(user_score) AS `total_question_score` FROM `lsdkf_quiz_report_question` WHERE `question_id` = 7
			$sumQuestion = $report->getReportSumQuestion($questionAllId[$i]->questionNum);
			$sum = $sumQuestion[0]->total_question_score;

			//Question Attempts			
			@$totalQuestionPercentage = round(($sum / $question_attempts)*100);
			$sumQuestionArray[$i] = $totalQuestionPercentage;	// this question ID = $questionAllId[$i]->questionNum
			//echo "<br />".$i." ".$totalQuestionPercentage;
		}
		
		/*echo "<pre>";
		//print_r($sumQuestionArray);
		echo "</pre>";			*/

		//var static = "<table class=\"table\"><thead><tr><th>#<\/th><th>Username<\/th><th>Score<\/th><th>Results<\/th><\/tr><\/thead><tbody><tr><td>1<\/td><td>teacher_temp<\/td><td>0%<\/td><td><a href=\"index.php?option=com_inquisitive&amp;controller=give&amp;uid=teacher_temp&amp;qid=1386196324\">Results&nbsp;»<\/a><\/td><\/tr><\/tbody><\/table>";

		$quizUsers = '<table class=\"table\"><thead><tr><th>#<\/th><th>Username<\/th><th>Score<\/th><th>Results</th><\/tr><\/thead><tbody>';
						
		$user_score = $i = $f = $d = $c = $b = $a = 0;
		//TODO convert to function
		//get the data from the quiz table
		//switch ranges into datasets ready for pie graphing
		foreach($getReportQuiz as $quizRow){
			$i++;			
			@$quizUsers .= '<tr><td>'.$i.'<\/td><td>'.$quizRow->user_id.'<\/td><td>'.round(($quizRow->user_score)*100).'%<\/td><td><a href=\"index.php?option='.JRequest::getVar('option').'&amp;controller=give&amp;uid='.$quizRow->user_id.'&amp;qid='.$quizID.'\">Results&nbsp;&raquo;<\/a><\/td><\/tr>';			
			//echo "<br />".$quizRow->user_score."<br />";
			$user_score = $quizRow->user_score;
			
			if($user_score == 0){
				//echo "user score = ".$user_score." ";//." f = $f = $d = $c = $b = $a <br/><br/>";
				//print_r($quizRow);
				//wierd fix for case($user_score == 0) not evaluating correctly...
				$f++;
			}else{
			
				switch($user_score){
					case ($user_score > 0 && $user_score <= .49):
						$f++;
						break;
					case ($user_score >= .50 && $user_score <= .65):
						$d++;					
						break;
					case ($user_score >= .66 && $user_score <= .75):
						$c++;					
						break;
					case ($user_score >= .76 && $user_score <= .85):
						$b++;					
						break;
					case ($user_score >= .86 && $user_score <= 1):
						$a++;				
						break;
				}
			}
			$user_score = 0;
		}
				                
		$quizUsers .= '<\/tbody><\/table>';
		
		$gradingUsersTotalScore = "
		 d1 = [
    	    ['0% - 49%', ".$f."]
	    ],
        d2 = [
            ['50% - 65%', ".$d."]
        ],
        d3 = [
            ['66% - 75%', ".$c."]
        ],
        d4 = [
            ['76% - 85%', ".$b."]
        ],
		d5 = [
            ['86% - 100%', ".$a."]
        ],";

		
		$this->assignRef('comName', $componentName);
		$this->assignRef('reportQuestion', $reportQuestion);
		$this->assignRef('reportQuiz', $reportQuiz);
		$this->assignRef('reportUsersVersusQuiz', $reportUsersVersusQuiz);
		$this->assignRef('questionNumTotal', $questionNumTotal);	
		$this->assignRef('sumQuestionArray', $sumQuestionArray);	
		$this->assignRef('gradingUsersTotalScore', $gradingUsersTotalScore);	
		$this->assignRef('quizUsers', $quizUsers);
		$this->assignRef('countQuizUsers', $i);
		$this->assignRef('getQuizInfo', $getQuizInfo);
		
		parent::display($tpl);
	}
}
?>
