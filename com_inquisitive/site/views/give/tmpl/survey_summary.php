<?php // no direct access

/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

///////////////////////////////
// Display each response 
//


	defined('_JEXEC') or die('Restricted access'); 
	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);

	// $uncorrected = false;
	$i = 1;		
	$s = $this->giveSurveySummary;
	// $total = round(($c['percentages']['total'] * 100));



//probably create a new function to quickly get human readible =)


	$feedbackResults = "<div class='base-layer'><!-- Begin results list -->"; 	
	$rowDivQuestion = "<div class='table-row-question clearfix'> <!--  Begin Question row -->";
	$rowDivAnswer = "<div class='table-row-answer-result clearfix'> <!--  Begin Answer row -->";
	$rowDivAnswerCorrect = "<div class='table-row-answer-result clearfix' id='green'> <!--  Begin Answer correct row -->";
	$leftCellCorrect = "<div class='left-cell-correct'>";
	$spaceLine = "<div class='space-line'></div>";
	$leftCellAnswer = "<div class='left-layer11'>";
	
	foreach($s as $data){
		// echo "type : <pre>";
		// 	print_r($data);
		// echo "</pre>";

		$feedbackResults .= $rowDivQuestion."<div>".$i." : ".$data->question."</div>".$spaceLine;
		$feedbackResults .= "</div> <!-- End question row -->";
					
		$feedbackResults .= $rowDivAnswer;
		foreach($data->answers as $answer){
			
			$feedbackResults .= $leftCellAnswer;
			//$feedbackResults .= print_r($answer, true);

			if($answer->type != 't') $feedbackResults .= "<strong>".$answer->answer."</strong>";
				else $feedbackResults .= "<i>".JText::_('COM_INQUISITIVE_GIVE_KEYWORDS_TITLE').$answer->keywords."</i>";
			
			$student_name = "";

			foreach($answer->results as $result){								
				if(empty($result->student_name)) $student_name = "<i>anon</i>";
					else $student_name = $result->student_name;

				if(empty($result->result)) $feedbackResults .= "<br/>".$student_name." <i>no response</i>";
					else $feedbackResults .= "<br/>".$student_name." ".$result->result;
			}
			
			$feedbackResults .= "</div><!-- end left cell -->";			
			
		}
		$feedbackResults .= "</div> <!-- End answer row -->";
		$feedbackResults .= $spaceLine;

		$i++;		
	}
	
	$feedbackResults .="</div> <!-- end results list -->";
	echo $feedbackResults;
		
?>
<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>
<!-- end of inquisitive give -->
