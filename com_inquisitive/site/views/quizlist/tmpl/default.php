<?php // no direct access
/**
 * Inquisitive View for Inquisitive Component
 * 
 * @package    Inquisitive
 * @subpackage Components
 * @link http://Inquisitive.net.au
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

	
	//TODO refactor http://www.gnu.org/licenses/gpl-2.0.html into headers
	
	defined('_JEXEC') or die('Restricted access'); 
	//Check for jquery
	require_once JPATH_COMPONENT . '/views/tmpl/jQuery.php';
	
	$user = JFactory::getUser();
	$rowCount = 0;
	$j = 1;	

	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);
	$filterQuizlist = '';

	//select article Dependencies
	 // Load the tooltip behavior.
	 JHtml::_('behavior.framework');
	 JHtml::_('behavior.tooltip');
	 JHtml::_('behavior.formvalidation');
	 JHtml::_('behavior.modal');
	 //JHtml::_('formbehavior.chosen', 'select');
	 $article = JTable::getInstance("content");

	//Display filtering if category list exists.
	$categoryList = $this->categoriesList;
	if(!empty($categoryList)){				
		$filterCategorySelect = "<select class='filterCategorySelect' name='filterCategorySelect'  default='filterClearCategory'>";

		if(!isset($this->filterCategory)) {
			$filtershowall = 'checked="checked"';
			$filterCategorySelected = 'selected=selected';		
			$filterCategorySelect .= "<option value='showall' ".$filterCategorySelected."> </option>";
		}else{ 
			$filtershowall = NULL;
		}					

		foreach($categoryList as $filteredCategory){
			if($filteredCategory['catid'] == $this->filterCategory) $filterCategorySelected = 'selected=selected';		
				else $filterCategorySelected = NULL;

			
			/*if($this->filterCategory == $filteredCategory['catid']) $filterCategorySelected = 'selected=selected';				
				else $filterCategorySelected = NULL;*/
				
			$filterCategorySelect .= "<option value='".$filteredCategory['catid']."' ".$filterCategorySelected.">".$filteredCategory['title']."</option>";
			$filterCategorySelected = NULL;				
		}
		$filterCategorySelect .= "</select>";
		
		$filterQuizlist = JText::_('COM_INQUISITIVE_QUIZLIST_CP_FILTER_TITLE').'<input class="filtershowall" type="radio" '.$filtershowall.' name="filtershowall">';
		$filterQuizlist .= JText::_('COM_INQUISITIVE_QUIZLIST_CP_CATEGORY_TITLE').$filterCategorySelect.'<br/>';
	} 

	echo '<h2>'.JText::_('COM_INQUISITIVE_QUIZLIST_TITLE').'</h2><form id="newQuestion" name="newQuestion" method="post">';


	if(empty($this->quizArray)){
		echo $filterQuizlist;
		echo "<br/>".JText::_('COM_INQUISITIVE_QUIZLIST_NOTHING_FOUND');
	}else{
		echo $filterQuizlist;
		foreach($this->quizArray as $row){
			//Parameters from quiz info table
			$quizParams = json_decode($row->params);
			
			//Check is owner
			if($user->username == $row->teacher_name) $quizOwner = TRUE;
				else $quizOwner = FALSE;
			
			//Check user has write access and is quiz owner
			if(($this->usertype == "Publisher") && ($quizOwner)) $quizOwnerWrite = TRUE;
				else $quizOwnerWrite = FALSE; //User is a guest
			
			//Super Admin true?	
			if(isset($this->superUser)) {
				$quizOwnerWrite = TRUE;
				$quizOwner = TRUE;
			}
			
			//check first row
			if($rowCount == 0){	?>
				<div class="base-layer%20list-fixed">
				<div class="table-row-top">
					<div class="left-cell-big"><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_LABEL_QUIZ_NAME'); ?></div>					
				<?php 
				if($quizOwnerWrite){?>
					<div class="left-cell-small">&nbsp;<!--Report & hits --></div>
					<!-- <div class="left-cell-small">Edit</div> -->
					<div class="left-cell-small"><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_LABEL_STATUS'); ?></div>				        						
				<?php } ?>
				</div></div><!-- end table-row-top -->
			<?php
			}//End first row
			$rowCount++;			
			
        		switch($row->hidden){	//get toggle state
					case "11":  //published (read only)
						$img = "publish_g.png";
						$alt = JText::_('COM_INQUISITIVE_QUIZLIST_PUBLISH_ALT_10');										
						$toggleStatus = '10';
						$edittag = 'Preview answers';
						$editimg = 'preview.png';
					break;
					case "10":	//Unpublished, previously published (read only)
						$img = "publish_ox.png";
						$alt = JText::_('COM_INQUISITIVE_QUIZLIST_PUBLISH_ALT_10');
						$toggleStatus = '11';
						$edittag = 'Preview answers';
						$editimg = 'preview.png';						
					break;
					case ("0" || NULL):	//Unpublished, newquiz (writeable)						
						$img = "publish_x.png";
						$alt = JText::_('COM_INQUISITIVE_QUIZLIST_PUBLISH_ALT_11');
						$toggleStatus = '11';
						$edittag = 'Edit';
						$editimg = 'edit.png';
					break;
				}
				
				//Skip displaying quiz if user is not the owner and the quiz is unpublished				
				if((!$quizOwner) && ($toggleStatus != '10')) continue;
				
			if($quizOwnerWrite){ ?>        

		<div class="table-row-list" id="row-<?php echo $row->timestamp; ?>">
			<div class="left-cell-big">
            
            <!-- Quiz Name -->       	
			<?php 
           	$quizNameLink = 'index.php?option='.JRequest::getVar('option').'&amp;controller=take&amp;task=sitquiz&amp;ID='.$row->timestamp;
			
			if(isset($row->quiz_name)){  ?>
				<a href="<?php echo JRoute::_($quizNameLink); ?>"><?php echo $row->quiz_name;?></a>
			<?php }else{ ?>
				<a href="<?php echo JRoute::_($quizNameLink); ?>">Unlabeled name</a>
			<?php } ?>
             </div>

            <div class="left-cell-small">&nbsp;
            <!-- Results / Reporting -->
                <?php 		
                //TODO Add percentage		
                if($this->usertype != "Guest"){                 
					if(($row->result != NULL) && ($row->survey != 1)){
						$reportAddress = "index.php?option=".JRequest::getVar('option')."&amp;controller=report&amp;quizID=".$row->timestamp;
						$reportIcon = '<img src="components/com_inquisitive/views/tmpl/images/cp_reports32.png" width="32" height="32" alt="Report" />';
	                    $reportLink = "<a href='".JRoute::_($reportAddress)."' title='Hits: ".$row->result."'>".$reportIcon."</a>";
						echo $reportLink;
					}elseif(($row->result != NULL) && ($row->survey == 1)){
						//PRE survey summary 09/02/15
						// $reportAddress = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."#";
						// $reportIcon = '<img src="components/com_inquisitive/views/tmpl/images/cp_reports32_grey.png" width="32" height="32" alt="Reporting unavailable for surveys." />';
	     //                $reportLink = "<a href='".JRoute::_($reportAddress)."' title='Reporting unavailable for surveys. Hits: ".$row->result."'>".$reportIcon."</a>";
						// echo $reportLink;

						$reportAddress = "index.php?option=".JRequest::getVar('option')."&amp;controller=give&amp;qid=".$row->timestamp."&amp;layout=survey_summary";
						$reportIcon = '<img src="components/com_inquisitive/views/tmpl/images/cp_reports32.png" width="32" height="32" alt="Report" />';
	                    $reportLink = "<a href='".JRoute::_($reportAddress)."' title='Hits: ".$row->result."'>".$reportIcon."</a>";
						echo $reportLink;

					}
                }else{
                    echo "" ;
                }
                ?>
            </div>
    
            <div class="left-cell-small">
            <!-- Publisher Quiz -->    
            	<?php 
                $publish_link = "index.php?option=".JRequest::getVar('option')."&amp;controller=quizlist&amp;ID=".$row->timestamp."&amp;toggle=".$toggleStatus;
                /* echo "publish link ".$publish_link."<br/><br/><br/>";
                 echo JRoute::_($publish_link);*/?>
               
                <div class="editlinktip hasTip" title="<?php echo JText::_( $alt );?>" onclick="toggleConfirm('<?php echo $img; ?>', '<?php echo $publish_link; ?>')">
                    <a href="<?php echo $_SERVER["REQUEST_URI"] ?>#">
                        <img src="components/com_inquisitive/views/tmpl/images/<?php echo $img;?>" width="32" height="32" alt="<?php echo $alt; ?>"  />
                     </a>
                </div>
            </div>
            <!-- Delete / Trash Quiz -->          

            <div class="left-cell-small">	            					
               <span class="editlinktip hasTip" title="<?php echo JText::_( "Trash" );?>">
               <a href="<?php echo $_SERVER["REQUEST_URI"] ?>#">                	
                	<div onclick="trash('<?php echo $row->timestamp; ?>'); return false">
               		    <img src="components/com_inquisitive/views/tmpl/images/trash.png" width="32" height="32" alt="Trash" />
                    </div>
               </a>
               </span>
            </div>
            <!-- Edit Quiz &controller-->
            <div class="left-cell-small">
                <span class="editlinktip hasTip" title="<?php echo JText::_( $edittag );?>">                	
                    <?php $link = "index.php?option=".JRequest::getVar('option')."&amp;controller=newquiz&amp;task=edit&amp;ID=".$row->timestamp;  ?>
                    
                    <a href="<?php echo JRoute::_($link); ?>">
                        <img src="components/com_inquisitive/views/tmpl/images/<?php echo $editimg;?>" width="32" height="32" alt="<?php echo $edittag; ?>" />
                    </a>			
                </span>
            </div>
            <!-- Control panel (more/down button)-->
            <div class="left-cell-small cp-img" id="<?php echo $row->timestamp; ?>">
                <a href="<?php echo $_SERVER["REQUEST_URI"] ?>#" class="link">
                <img src="components/<?php echo $this->comName; ?>/views/tmpl/images/cp_morearrow-down.png" alt="more"/></a>
            </div><br/>
            <div class='clearfix'>&nbsp;</div>
            
            <div id="cp-<?php echo $row->timestamp;?>" class="table-row-cp" style="display: none">
                <div class="left-cell-small"><a class="tooltip-inquisitive" href="#"><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_MODE_TITLE'); ?><span class="custom info"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/help.png" alt="Info" height="30" width="30" /><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_MODE_DESC'); ?></span></a></div><div class="left-cell-big">
                <input type="radio" value="survey" name="mode<?php echo $row->timestamp;?>" <?php if($row->survey == 1) echo 'checked="checked"'; ?> /><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_MODE_1'); ?> &nbsp;&nbsp;&nbsp;
                <input type="radio" value="quiz" name="mode<?php echo $row->timestamp;?>" <?php if($row->survey == 0) echo 'checked="checked"'; ?> /><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_MODE_2'); ?> &nbsp;&nbsp;&nbsp;
            </div><br />
            <div class='clearfix'>&nbsp;</div>
               
	       	<?php
            if(!empty($categoryList)){ ?>				                
                <!-- Categories List -->
                <div class="left-cell-small"><a class="tooltip-inquisitive" href="#"><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_CATEGORY_TITLE'); ?><span class="custom info"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/help.png" alt="Info" height="30" width="30" /><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_CATEGORY_DESC'); ?></span></a></div>
                	<div class="left-cell-big">
                	<?php
												
						$categorySelectQuiz = "<select name='category".$row->timestamp."'>";
												
                		foreach($categoryList as $category){                			
                			if($row->catid == $category['catid']) $selected = 'selected=selected';
                				else $selected = NULL;
                			$categorySelectQuiz .= "<option value='".$category['catid']."' ".$selected.">".$category['title']."</option>";	
                			$selected = NULL;
                		}
                		$categorySelectQuiz .= "</select>";
						echo $categorySelectQuiz;						
                	?>
                	</div><br />
                	<div class='clearfix'>&nbsp;</div>
            <?php } ?>
            
            <div class="left-cell-small"><a class="tooltip-inquisitive" href="#"><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_REPLICATE_TITLE'); ?><span class="custom info"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/help.png" alt="Info" height="30" width="30" /><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_REPLICATE_DESC'); ?></span></a></div>
            <div class="left-cell-big">            	
            	<a href="<?php echo $_SERVER["REQUEST_URI"] ?>#">
            		<img src="components/com_inquisitive/views/tmpl/images/cp_replicate32.png" width="32" height="32" alt="Replicate Quiz / Survey" onclick="replicatequiz('<?php echo $row->timestamp; ?>'); return false" /></a>            	
            </div><br />
            <div class='clearfix'>&nbsp;</div>

			<!-- Layout select -->
	       	<?php
	       	$layoutList = $this->layoutList;
            if(!empty($layoutList)){             	?>	            			               
                <!-- Layout List -->
                <div class="left-cell-small"><a class="tooltip-inquisitive" href="#"><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_LAYOUT_TITLE'); ?><span class="custom info"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/help.png" alt="Info" height="30" width="30" /><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_LAYOUT_DESC'); ?></span></a></div>
                	<div class="left-cell-big">
	            <?php
				$layoutSelectQuiz = "<select name='layout".$row->timestamp."'>";

	    		foreach($layoutList as $layout){                			
	    			if(@$quizParams->layout == $layout['layoutid']) $selected = 'selected=selected';
	    				else $selected = NULL;
	    			$layoutSelectQuiz .= "<option value='".$layout['layoutid']."' ".$selected.">".$layout['title']."</option>";
	    			$selected = NULL;
	    		}
	    		$layoutSelectQuiz .= "</select>";

				echo $layoutSelectQuiz;						
			?>
                	</div><br />
                	<div class='clearfix'>&nbsp;</div>
            <?php } ?>


			<!-- Select an Article (to show after user completes quiz) -->
			<script type="text/javascript">
				function jSelectArticle_jform_request_id<?php echo $row->timestamp; ?>(id, title, object) {  	
				 	document.getElementById('jform_request_id_name<?php echo $row->timestamp; ?>').value = title;
				 	document.getElementById('jform_request_id_id<?php echo $row->timestamp; ?>').value = id;
					window.parent.SqueezeBox.close();
				}

				function clearArticle(quizID){
					document.getElementById('jform_request_id_name'+quizID).value = "";
				 	document.getElementById('jform_request_id_id'+quizID).value = "";
				}
			</script>

			<?php
				$article->load($quizParams->article);

				if($quizParams->article != NULL) $articleTitle = $article->get("title");
					else $articleTitle = "Select an Article";
			?>
			<div class="left-cell-small"><a class="tooltip-inquisitive" href="#"><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_ARTICLE_TITLE'); ?><span class="custom info"><img src="components/<?php echo $this->comName; ?>/views/tmpl/images/help.png" alt="Info" height="30" width="30" /><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_ARTICLE_DESC'); ?></span></a></div>
			<div class="left-cell-big">
			 <span class="input-append">			 	
				 <input class="input-medium" id="jform_request_id_name<?php echo $row->timestamp; ?>" value="<?php echo $articleTitle; ?>" disabled="disabled" size="35" type="text">
				 <a class="modal btn" title="Select or Change article" href="index.php?option=com_content&amp;view=articles&amp;layout=modal&amp;tmpl=component&amp;function=jSelectArticle_jform_request_id<?php echo $row->timestamp; ?>&amp;<?php echo JSession::getFormToken(); ?>=1" rel="{handler: 'iframe', size: {x: 800, y: 450}}">
				 <i class="icon-file"></i> Select</a>

                 <button class="modal btn" onclick="clearArticle('<?php echo $row->timestamp; ?>')" type="button"><?php echo JText::_('COM_INQUISITIVE_BUTTON_CLEAR'); ?></button>

			 </span>
			 <input required="required" aria-required="true" id="jform_request_id_id<?php echo $row->timestamp; ?>" class="required modal-value" name="jform_request_id_id<?php echo $row->timestamp; ?>" value="" type="hidden">
			</div>
              	

			<!-- buttons -->
			<div class='clearfix'>&nbsp;</div>
            <div class="left-cell-big">
                <div class="buttons-footer text-btn">
                    <input type="button" onclick="submitbutton(this.name);" value="Save" name="<?php echo $row->timestamp;?>"> <!-- id="submitsave" -->
                </div>
                <div class="buttons-footer text-btn">
                    <button onclick="submitbutton('cancel')" type="button"><?php echo JText::_('COM_INQUISITIVE_BUTTON_CANCEL'); ?></button>
                </div>
            </div>
            <div class='clearfix'>&nbsp;</div>
            <div class="left-cell-small"><?php echo JText::_('COM_INQUISITIVE_QUIZLIST_CP_CREATED_ON'); ?></div><div class="left-cell-big"><?php echo date("d m Y",$row->timestamp); ?></div>
        </div>
<?php }//end publisher check 

if(!$quizOwnerWrite){ ?>
		<div class="table-row-list">
			<div class="left-cell-big">            
            	<!-- Quiz Name -->
            	<?php $link = "index.php?option=".JRequest::getVar('option')."&amp;controller=take&amp;task=sitquiz&amp;ID=".$row->timestamp;  ?>
				<a href="<?php echo $link; ?>"><?php echo $row->quiz_name;?></a>
             </div>
		<div class="left-cell-small">&nbsp;
        <!-- Results -->
	<?php // Show results for student or teacher
	if($this->usertype = "Guest") echo ""; //Add percentage
		elseif($row->result != NULL) echo $row->result;?> 
			</div>		
			<div class="left-cell-small"></div>
			<div class="left-cell-small"></div>        
<?php } ?>
  <div class="space-line"></div>
</div>
<?php		
		}
	 }
	echo "</form>";
 ?>
 
<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>


<script type="text/javascript" src="components/<?php echo $this->comName; ?>/views/tmpl/js/quizlist.js"></script> 