<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 

$user = JFactory::getUser();
if(!$user->guest) {
	$styleCss = "components/".$this->comName."/views/tmpl/style.css";
	$document = JFactory::getDocument();
	$document->addStyleSheet($styleCss);
?>

<form name="newQuestion" method="post" onsubmit="return validate_form(this)">  
<div class="base-container base-layer" >
 	<?php require_once (JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'questionHeader.php'); ?>

	<div class="table-row-answer">
		<div class="headingLabel">
	        <?php echo JText::_('COM_INQUISITIVE_QUESTION_DETAIL_LABEL_ANSWER'); ?>:
        	<p class="numText" ID="answerNum" style="display:none;">1</p><br/>
        </div>
		<div class="left-layer11">
        	<textarea name="answer1" id="answer1" cols="40" rows="4" <?php if($this->editcheck == "read") echo "disabled=true"; ?>><?php if(isset($this->answer_array[0])) echo $this->answer_array[0]->answer; ?></textarea>
            <br />            
        </div>             

		<div class="right-layer11 dashed">
			<div class="rowMultiChoice"><?php echo JText::_('COM_INQUISITIVE_QUESTION_DETAIL_CORRECT'); ?></div>
		   	<div class="space-line"></div>
			<div class="rowMultiChoice">&nbsp;&nbsp;&nbsp;<input type="checkbox" name="true1" id="correct" <?php if($this->editcheck == "read") echo "disabled=true"; ?> <?php if(isset($this->answer_array[0]->correct)) if($this->answer_array[0]->correct != NULL) echo 'checked=checked'; ?> />
    	    </div>    
			<div class="space-line"></div>
		</div>

		<div class="space-line"></div>
	</div>
    
<div id="content-inquisitive"></div>
           
<div class="space-line"></div>
</div>
	<input type="hidden" id="answerBankAmt" value="1" name="answerBankAmt" />
    <input type="hidden" id="count" name="skill_count" value="2" />
    <?php require_once(JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'questionFooterButtons.php'); ?>     
</form>
</div>
    <?php require_once(JPATH_COMPONENT.DS.'views'.DS.'tmpl'.DS.'poweredby.php'); ?>
<?php
	}else{		
		echo JText::_('COM_INQUISITIVE_SESSION_EXPIRED_ERROR');
	}//User Access
?>