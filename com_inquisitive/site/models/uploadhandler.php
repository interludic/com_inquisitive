<?php


/**
 * Inquisitive Model for Inquisitive Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

/**
 * Inquisitive Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
 
 //Models contain Model classes. 
 //One Model class is equal to one DB table.
 
	class InquisitiveModelUploadHandler extends JModelLegacy
	{			

	}