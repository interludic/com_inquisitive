<?php
/**
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Inquisitive helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_inquisitive
 * @since		2.0.9
 */
class InquisitiveHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	The name of the active view.
	 * @since	2.0.9
	 */
	public static function addSubmenu($vName = 'categories')
	{

		JSubMenuHelper::addEntry(JText::_('COM_INQUISITIVE_SUBMENU_ABOUT'),
			'index.php?option=com_inquisitive',
			$vName == 'about'
		);

		$return = static::getReturn();
		//JHtml::_('tsidebar.addEntry', JText::_('COM_TDOWNLOADSTORE_SETTINGS'), 'index.php?option=com_config&view=component&component=com_tdownloadstore' . $return, $vName == 'settings', '', 'icon-cog');
		JSubMenuHelper::addEntry(JText::_('COM_INQUISITIVE_SUBMENU_SETTINGS'),
			'index.php?option=com_config&view=component&component=com_inquisitive'.$return,
			$vName == 'settings', '', 'icon-cog'
		);
		JSubMenuHelper::addEntry(JText::_('COM_INQUISITIVE_SUBMENU_CATEGORIES'),
			'index.php?option=com_categories&view=categories&extension=com_inquisitive', $vName == 'categories');
		$document = JFactory::getDocument();
		if ($vName == 'categories'){ 
			$document->setTitle(JText::_('COM_INQUISITIVE_ADMINISTRATION_CATEGORIES'));
		}
			
	}

    /**
     * Method to get current URI
     * @param string|boolean $prefix
     * @return string
     * @since 1.0
     */
    public static function getReturn($prefix = '&return=')
    {
            $return = base64_encode(JURI::getInstance()->toString());
            if ($prefix)
            {
                    $return = $prefix . $return;
            }
            return $return;
    }


}
