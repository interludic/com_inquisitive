<?php
/**
 * @version       $Id$
 * @copyright     Copyright (C) 2007 - 2014 Favourites Multimedia. All rights reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @author        Harry Bosh - Inquisitive
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

# For compatibility with older versions of Joola 2.5
if (!class_exists('JViewLegacy')){
    class JViewLegacy extends JView {

    }
}

/**
 * @package    Inquisitive
 * @subpackage com_inquisitive
 */
class InquisitiveViewConfig extends JViewLegacy
{

    /**
     * Display the view
     *
     * @access    public
     */
    function display($tpl = null)
    {

        $version = new JVersion;

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            JError::raiseError(500, implode("\n", $errors));
            return false;
        }

        JHTML::stylesheet('media/components/com_inquisitive/css/inquisitive.css');

        $this->_setToolbar();

        if (version_compare($version->getShortVersion(), '3.0.0', '<')) {
            $tpl = 'legacy';
        }
        parent::display($tpl);
        
		//JRequest::setVar('hidemainmenu', true);
    }
 

    /**
     * Display the toolbar
     *
     * @access    private
     */
    function _setToolbar()
    {
        
        $xml = JFactory::getXML(JPATH_ADMINISTRATOR.'/components/com_inquisitive/install.xml');
        $version = (string)$xml->version;

        //$doc->addStyleDeclaration('.icon-48-inquisitive {background-image: url(components/com_inquisitive/images/inquisitive_48.jpg);}');
        JToolBarHelper::title(JText::_('COM_INQUISITIVE_CONFIG_TITLE')." v".$version, 'inquisitive.png');
		JToolBarHelper::preferences('com_inquisitive');
		JToolBarHelper::divider();	
		//$help_url  = 'http://www.example.com/{language}/help-server';
		$help_url  = 'http://www.inquisitive.net.au/index.php?option=com_content&view=article&id=6';
		JToolBarHelper::help('JHELP_COMPONENTS_INQUISITIVE', false, $help_url );
    }
}
