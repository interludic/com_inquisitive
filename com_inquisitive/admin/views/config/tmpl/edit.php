<?php
/**
 * @version       $Id$
 * @copyright     Copyright (C) 2007 - 2014 Favourites Multimedia. All rights reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @author        Harry Bosh - Inquisitive
 */
defined('_JEXEC') or die;


// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
if(version_compare(JVERSION,'3.0.0','ge')) {
    JHtml::_('formbehavior.chosen', 'select');
}
?>
<div class="row-fluid">
            <h2></h2>
                <div class="row-fluid">
                    <div class="span10">
                        <div class="control-group">
                             <h3><?php echo JText::_( 'Configuration:' ); ?></h3>
                            <div class="controls">
                            	 <?php echo JText::_( 'The options button allows you and your users to have the appropriate permissions while using Inquisitive. <strong>Permission MUST BE granted</strong> to the creator and user appropriatley (Button is located above right), Otherwise you will not be able to access Inquisitive. <br />Click the help button above, this links to the inquisitive.net.au support & user guide. Be sure to read it before submitting a support ticket.' ); ?>

                            </div>
                        </div>
                        <div class="control-group">
                            <h3><?php echo JText::_( 'Feature Request:' ); ?></h3>
                            <div class="controls">
                                <?php echo JText::_( 'Feel free to send feature requests to the help link. Features added to inquisitive benefit everyone.' ); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <h3><?php echo JText::_( 'Updates:' ); ?></h3>
                            <div class="controls">
                                <?php echo JText::_( 'Inquisitive is constantly being updated, this then gets sent to your site automatically.' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<div class="clr"></div>
