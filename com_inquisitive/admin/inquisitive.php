<?php
/**
 * @version       $Id$
 * @copyright     Copyright (C) 2007 - 2014 Favourites Multimedia. All rights reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @author        Harry Bosh - Inquisitive
 */

// no direct access
defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');
$document = JFactory::getDocument();
$document->addStyleDeclaration('.icon-48-inquisitive {background-image: url(../media/com_inquisitive/images/inquisitive-48.jpg);}');

# For compatibility with older versions of Joola 2.5
if (!class_exists('JControllerLegacy')){
    class JControllerLegacy extends JController {
    }
}

$controller = JControllerLegacy::getInstance('Inquisitive');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();