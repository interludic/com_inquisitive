<?php
/**
 * @version       $Id$
 * @copyright     Copyright (C) 2007 - 2014 Favourites Multimedia. All rights reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @author        Harry Bosh - Inquisitive
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Component Controller
 *
 * @package     Inquisitive
 * @subpackage  com_inquisitive
 */
class InquisitiveController extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();       
    }

    /**
     * Display the view
     */
    public function display($cachable = false, $urlparams = false)
    {
        require_once JPATH_COMPONENT.'/helpers/inquisitive.php';

        // Load the submenu.
        InquisitiveHelper::addSubmenu(JRequest::getCmd('view', 'categories'));        

        // Get the document object.
        $document = JFactory::getDocument();

        // Set the default view name and format from the Request.
        $vName = JRequest::getWord('view', 'config');
        $lName = JRequest::getWord('layout', 'edit');
		
        $vFormat = $document->getType();
		
        // Get and render the view.
        if ($view = $this->getView($vName, $vFormat)) {
            $view->setLayout($lName);
            $view->display();
        }
    }
}