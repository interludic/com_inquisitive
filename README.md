# Inquisitive

[ View website for demo, changelog and user guide ](http://inquisitive.net.au/)

![Inquisitive Logo](http://inquisitive.net.au/images/INQUISITIVE-logo.jpg)

What is this?

Inquisitive is a Joomla! component that allows you to create quiz / surveys from the front-end of your site. You can then fill in answers and submit. Reporting displays individual or entire quiz / survey responses.

    Conduct course evaluations, quizzes & tests online
    Perform training compliance self-checks
    Gather student or attendee feedback

With a view towards a Joomla! learning management system LMS. 

###TASKS
