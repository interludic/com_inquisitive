Installation
Install using the Joomla installer. Enable the plugin in the plugin manager

 
Embedding the product snapshot into a Joomla Article
Use code such as the following:

{product id=23}

This will embed the product with an id number of 23 (the id number is the one in the right hand column in the column marked id in the Virtuemart product manager). 

You can set default parameters in the plugin manager. These can be overridden with user parameters, the ones available are:

id
sku
showAddToCart 
showPrice 
showCaption
showSDescription
showDescription 
showImage 
showName
showProductLink
captionLength
showSKU
showStock

either the id parameter or the sku parameter is compulsory, the others are optional. If used they must be separated by semi-colons and have a zero or one value:

{product id=23;showImage=1;showAddToCart=0;showDescription=1} 

This will show the image and full description but not the add-to-cart button. The other parameters will take their default values as set in the plugin manager.

Example 2
{product sku=c002a;showPrice=1;showDescription=0;showStock=1}

This will show the product selected by the sku c002a, with the price and stock but not the description. The other parameters will take their default values as set in the plugin manager.

